#pragma once
#include "TcsLink.h"

#define WM_STS_REQ			WM_USER + 1
#define WM_NEW_IMAGE		WM_USER + 2
#define WM_MTSVR_SOCK		WM_USER + 3		// 면탈 서버 소켓
#define WM_MSG_FTPSOCK		WM_USER + 4
#define WM_MSG_UPSOCK		WM_USER + 5
#define WM_FTP_DATASOCK		WM_USER + 6
#define WM_SET_STATUS		WM_USER + 7

#define WM_WPARAM_ACCEPT			10		// 소켓 연결됨	
#define WM_WPARAM_DATRCV			11		// 데이터를 받음
#define WM_WPARAM_SOCKCL			12		// 소켓 끊어짐

#define MTSOCK_MAX_BUFFER			1024


typedef struct {
	int		packetNum;	// 패킷 개수
	int		currentID;	// 현재 보내는 패킷 번호
	int		dataLen;	// 전체 데이타 길이 : pData의 길이
	BYTE*	pData;		// 보낼 데이타
} SENDPACKET;

// CServerAgentSocket 명령 대상입니다.
// 연결유지용 서버소켓

class CServerAgentSocket : public CAsyncSocket
{
public:
	CServerAgentSocket();
	virtual ~CServerAgentSocket();
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

public:
	int		MAX_MT_RX_LEN;
	int		m_bufLen;
	int		m_bConnected;
	CTcsLink*	m_ptcsLnk;
	
	unsigned char	m_pBuf[1024];
	unsigned char	m_packet[256];
	unsigned char*	m_sndbuf;	//2013.5.14 SIJ
	int				m_sndsize;
	HWND	m_Hwnd;
	int		m_port;				//2003.05.30
	CString m_youAddress;		//2003.05.30 연결된 주소

	void Init(int Connected);
protected:

public:
	int SendData(unsigned char *buf, int bufsize);
	void reSend();
};

//------------------------------------------------------------
// 대기용 서버소켓
class CServerSock : public CAsyncSocket
{
public:		
	CServerSock();
	~CServerSock();

	CServerAgentSocket*	m_pAgentSock;
	void Init(HWND hwnd);
	BOOL Listen( int port, int nConnectionBacklog );
		
public:
	HWND	m_Hwnd;
	int		m_port;
	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);
};
