
// JWIS_TCSDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "JWIS_TCS.h"
#include "JWIS_TCSDlg.h"
#include "MtFtpSock.h"
#include "afxdialogex.h"
#include <MMSystem.h>
#include <sql.h>
#include <sqlext.h>
#include <msado15.h>

#include <afxinet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define MAXBUFLEN				255 
#define MIN_FILENAME_LENGTH 	35
#define MAX_FILENAME_LENGTH 	41
#define MIN_EXT_POS         	31
#define MIN_PLATE_POS       	23

#define STATUS_DISCONNECT 		0		// 접속종료종료
#define STATUS_CONNECT   		1		// 접속상태

#define STATUS_ODBC 			1		// ODBC 상태
#define STATUS_FTP  			2		// FTP 상태
//-----------------------------------------------------------------
//				INI File 내용
#define	PATH				"PATH"
#define	NETWORK				"NETWORK"
#define	LOG					"LOG"
#define	ID					"ID"


#define	CAR_IMAGE_PATH			"CAR_IMAGE_PATH"
#define	PLATE_IMAGE_PATH		"PLATE_IMAGE_PATH"
#define	TAG_PATH				"TAG_PATH"
#define	BACKUP_IMAGE_PATH		"BACKUP_IMAGE_PATH"
#define	SERVER_IMAGE_PATH		"SERVER_IMAGE_PATH"
#define SERVER_IMAGE_COPY_PATH	"SERVER_IMAGE_COPY_PATH"
#define	TEMP_IMAGE_PATH			"TEMP_IMAGE_PATH"
#define	LOG_FILE_PATH			"LOG_FILE_PATH"
#define	WORK_IMAGE_PATH			"WORK_IMAGE_PATH"


#define	SELF_ETHERNET_NAME		"SELF_ETHERNET_NAME"
#define	SELF_IP_ADDRESS			"SELF_IP_ADDRESS"
#define	SELF_SUBNET_MASK		"SELF_SUBNET_MASK"
#define	SERVER_IP_ADDRESS		"SERVER_IP_ADDRESS"
#define	FTP_USERNAME			"FTP_USERNAME"
#define	FTP_PASSWORD			"FTP_PASSWORD"
#define	MTSETVER_PORT			"MTSETVER_PORT"
#define	LCL_PORT				"LCL_PORT"
#define	FTP_DATA_PORT			"FTP_DATA_PORT"
#define	DBSERVER_ODBC_USER		"DBSERVER_ODBC_USER"
#define	DBSERVER_ODBC_PASS		"DBSERVER_ODBC_PASS"

#define	PRINT_LOG				"PRINT_LOG"
#define	AUTOSAVE_LOG			"AUTOSAVE_LOG"
#define	PRINT_TCS				"PRINT_TCS"
#define	NUM_DEVICE				"NUM_DEVICE"
#define	NUM_LANE				"NUM_LANE"
#define	NUM_OFFICE				"NUM_OFFICE"

#define	GATE_TYPE				"GATE_TYPE"
#define	HIPASS_MIXED			"HIPASS_MIXED"

#define	ALL_TICKET_DATA_SEND	"ALL_TICKET_DATA_SEND"
#define	SEND_DOUBLE_CODE		"SEND_DOUBLE_CODE"

#define	CLOSE_GATE			0	// 폐쇄식 출구
#define	OPEN_UP_GATE		1	// 개방식 상행
#define	OPEN_DN_GATE		2	// 개방식 하행
//-----------------------------------------------------------------
//
#define	TITLE_RECOG_SW		"영상촬영시스템-로컬제어-JWISTCS"
#define MT_SVR_FTP_STRING	"Run NT "
#define MT_FTP_LISTFILE		"\\ftpList.txt"

// 버전
#define MT_VERSION			"2020.04.07"
#define MT_BUILD_NUM		"Build. 210"

#define MT_LANESTATE_1		"단수부스"
#define MT_LANESTATE_2		"복수부스"

#define DBCONNECT_STRING1	"Provider=SQLOLEDB.1;Persist Security Info=False;User ID=%s;Password=%s;Initial Catalog=Tves;Data Source=%s"
#define DBCONNECT_STRING2	";Initial File Name=\"\";Server SPN=\"\""

#define MSG_MTTKDATA		"MT UNMATCHED TICKET DATA"
#define MSG_MTTOWTRUCK		"MT TOW TRUCK"					// 2013.7.12 견인차량

const UINT idMsgMtTkData = ::RegisterWindowMessage(MSG_MTTKDATA);
const UINT idMsgMtTkTow  = ::RegisterWindowMessage(MSG_MTTOWTRUCK);

int g_bImgThread;
int g_bFtpThread;
int g_bDBThread;
int g_bSearchThread;

//TCS LCL관련
HANDLE g_TCSEvent;
//ADO 관련
ADODB::_ConnectionPtr g_pConn = NULL;
int bConnectedDB = FALSE;				//DB 접속 여부
int bFirst = TRUE;
//BL및 PL 검색관련
HANDLE g_BLEvent_wait, g_BLEvent_done;
char	g_strPlate[20];

CJWIS_TCSDlg *g_pDlg = NULL;
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

/////////////////////////////////////////////////////////////////////////////
// Windows 재시작 함수
void RebootWindow()
{
	OSVERSIONINFO osinfo;
	osinfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&osinfo);
	if((osinfo.dwPlatformId == VER_PLATFORM_WIN32_NT) && (osinfo.dwMajorVersion >= 3)) {
		HANDLE hToken; 
		TOKEN_PRIVILEGES tkp;

		if (!OpenProcessToken(GetCurrentProcess(), 
					TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
			OutputDebugString("OpenProcessToken");

		// Get the LUID for the shutdown privilege. 

		LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, 
								&tkp.Privileges[0].Luid); 

		tkp.PrivilegeCount = 1; // one privilege to set 
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 

		// Get the shutdown privilege for this process. 

		AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0); 

		// Cannot test the return value of AdjustTokenPrivileges. 
		if (GetLastError() != ERROR_SUCCESS) {
			OutputDebugString("AdjustTokenPrivileges");
		}
	}

	ExitWindowsEx( EWX_REBOOT | EWX_FORCE, 0);

}

/////////////////////////////////////////////////////////////////////////////
// 날짜계산 함수

const int g_DaysPerMonth[13] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365}; 
/**
* 해당 날짜의 값을 게산한다.
*
*@param		iYear		년도 
*@param		iMonth		달
*@param		iDay			날
*/
int CalcDay(int iYear, int iMonth, int iDay) 
{ 
	int iTotalDays;
	iTotalDays = (iYear-1)*365; // 년도에 따른 일수 계산 
	iTotalDays += ((iYear-1)/4 - (iYear-1)/100 + (iYear-1)/400); // 윤년을 계산하여 일수 가감 
	iTotalDays += (g_DaysPerMonth[iMonth-1] + iDay); // 입력 월일의 일수 계산 

	// 입력된 년도가 윤년이고 2월이 지났다면 
	if (!(iYear%4) && iMonth>2) 
	{
		iTotalDays++; 
		if (!(iYear%100)) // 만약, 100의 배수인 해이면 
			iTotalDays--; 
		if (!(iYear%400)) // 만약, 400의 배수인 해이면 
			iTotalDays++; 
	}
	return iTotalDays;
} 

/////////////////////////////////////////////////////////////////////////////
// Directrory Access Function

/**
* 디렉토리가 있는지 확인한다.
*
*@param		s		
*/
/*
int isDirExists(char* s) 
{
	_finddatai64_t c_file;
	intptr_t hFile;
	int result = 0;

	hFile = _findfirsti64(s, &c_file);
	if (c_file.attrib & _A_SUBDIR ) {
		result = 1;
	}
	_findclose(hFile);

	return result;
}
*/


//  기존 디렉토리가 있을경우 안만들어지고 없으면 만든다. 부모디렉토리가 없어도 생성가능
void CreateDir(TCHAR* Path)
{
	TCHAR DirName[256]; //생성할 디렉초리 이름
	TCHAR * p = Path; //인자로 받은 디렉토리
	TCHAR * q = DirName;
	while ( * p) {
		 //루트디렉토리 혹은 Sub디렉토리
		if ((_T('\\') ==  * p) || (_T('/') ==  * p)){
			if (_T(':') !=  * (p - 1)) {
				CreateDirectory(DirName, NULL);
			}
		}
		 * q++ =  * p++;
		 * q = _T('\0');
	}
	CreateDirectory(DirName, NULL);
}

/**
*해당 하는 디렉토리에 파일이 존재해도  디렉토리가 비어있지 않아도 지울 수 있다 .
*/
void EmptyDirectory(TCHAR* folderPath)
{
	TCHAR fileFound[300];
	WIN32_FIND_DATA info;
	HANDLE hp;

	//if (isDirExists(folderPath) == FALSE)
	if(PathFileExistsA(folderPath) == false)	//2019.08.27 SIJ
	{
		return;
	}
	
	//그 이상되면 파일명을 받을 수가 없음
	if (strlen(folderPath) > 200) 
	{
		return;
	}

	memset(fileFound, 0, 300);
	wsprintf(fileFound, _T("%s\\*.*"), folderPath);
	hp = FindFirstFile(fileFound,  & info); //디렉토리에 파일이 있는지 첫번째 파일만.
	if (hp != INVALID_HANDLE_VALUE) 
	{
		do 
		{
			Sleep(0);
			if (!((strcmp(info.cFileName, ".") == 0) || (strcmp(info.cFileName, "..") == 0))) 
			{
				//Sub디렉토리가 존재하는경우
				if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY) 
				{
					CString subFolder(folderPath);
					subFolder.Append(_T("\\"));
					subFolder.Append(info.cFileName);
					EmptyDirectory(subFolder.GetBuffer(256));
					RemoveDirectory(subFolder.GetBuffer(256));
					subFolder.ReleaseBuffer();
				}
				else 
				{
					memset(fileFound, 0, 300);
					wsprintf(fileFound, _T("%s\\%s"), folderPath, info.cFileName);
					BOOL retVal = DeleteFile(fileFound);
				}
			}
		} while (FindNextFile(hp,  & info));
	}
	FindClose(hp);
}

//----------------- 쓰레드 ---------------------------------------------------------

/**
* 파일 이름으로 번호판 문자를 받는다. 
*
*@param		FileName		차량 파일명
*@param		strPlate		번호판 문자
*/
int GetPlateString(char *FileName, char *strPlate)
{
	int i;
	char *pdest;
	CString str;
	char *tmpstr;

	pdest = strchr(FileName, '.');
	i = (int)(pdest - FileName + 1);

	if(i <= MIN_FILENAME_LENGTH) return FALSE;
	if(i > MAX_FILENAME_LENGTH) return FALSE;
	if( (FileName[27] == 'X') || (FileName[27] == 'x') || (FileName[27] == ' ') ){
		return FALSE;
	}
	else{
		memcpy(strPlate, &FileName[23], i-24);
	}
	
	str.Format("%s\0", strPlate);
	str.Trim();
	i = str.GetLength();
	tmpstr = str.GetBuffer(i+1);
	memcpy(strPlate, tmpstr, i);
	strPlate[i] = '\0';
	str.ReleaseBuffer();
	
	return TRUE;
}

/**
* BSTR에서 CString으로 변환한다.
*/
CString bstrToCString(BSTR bstr)  
{
	char* pbstr;
	USES_CONVERSION;
	pbstr=OLE2A(bstr);
	return CString(pbstr);
}

int GetTickInterval(DWORD startTick)
{
	DWORD nowTick;
	int interval;
	nowTick = GetTickCount();
	if(nowTick < startTick)
	{
		interval = (ULONG_MAX - startTick) + nowTick;
	}
	else
		interval = nowTick - startTick;
	return interval;
}

/**
* DB에서 BL을 검색한다. 
*
*@param		strPlate		
*@param		strBL1		
*@param		strBL2		
*/
int SearchList(char *strPlate, CString *strBL1, CString *strBL2)
{
	int rValue = 0;		//2013.4.15 SIJ 초기화값 부여
	CString tmpstr;
	char idx;
	RETCODE retcode = SQL_SUCCESS;
	int cnt=0;
	ADODB::_RecordsetPtr rs1=NULL, rs2=NULL, rs3 = NULL;
	_variant_t var;


	char strSQL[256];
	// SQLBindCol variables
	int PLtype = 0, BLtype = 0, mtstPl = 0;

	strBL1->Empty();
	strBL2->Empty();

	////////////////////////////////////////////////////////////////////////
	// pl 검색
	memset(strSQL, 0, sizeof(strSQL));
	sprintf_s(strSQL, sizeof(strSQL), "SELECT * FROM MasterPL where CarNum = '%s'\0", strPlate);

	//2013.4.15 SIJ 위치이동
	if (bConnectedDB == FALSE)
	{		
		return -1;
	}

	try
	{
		rs1 = g_pConn->Execute(_bstr_t(strSQL), NULL, adCmdText);
	}
	catch(...)
	{
		retcode = SQL_ERROR;
	}

	if(rs1 != NULL)
	{
		if(!rs1->adoEOF)
		{
			PLtype = 1;
		}

		rs1->Close();
	}
	else
	{
		retcode = SQL_ERROR;
	}

	if(retcode == SQL_ERROR){
		return -1;
	}

	////////////////////////////////////////////////////////////////////////
	// @@cjw - 사업용 화물 검색
	memset(strSQL, 0, sizeof(strSQL));
	sprintf_s(strSQL, sizeof(strSQL), "SELECT * FROM MTSTPL  where Car_No = '%s'\0", strPlate);

	if (bConnectedDB == FALSE)
		return -1;

	try
	{
		rs3 = g_pConn->Execute(_bstr_t(strSQL), NULL, adCmdText);
	}
	catch(...)
	{
		retcode = SQL_ERROR; 
	}

	if(rs3 != NULL)
	{
		if(!rs3->adoEOF)
		{
			mtstPl = 1;		// 1~3종 사업용 화물차임
		}
		rs3->Close();
	}
	else
	{
		retcode = SQL_ERROR;
	}

	////////////////////////////////////////////////////////////////////////
	// bl 검색
	memset(strSQL, 0, sizeof(strSQL));
	sprintf_s(strSQL, sizeof(strSQL), "SELECT * FROM MasterBL  where CarNum = '%s'\0", strPlate);

	if (bConnectedDB == FALSE)
	{		
		return -1;
	}

	try
	{
		rs2 = g_pConn->Execute(_bstr_t(strSQL), NULL, adCmdText);
	}
	catch(...)
	{
		retcode = SQL_ERROR; 
	}


	if(rs2 != NULL)
	{
		if(!rs2->adoEOF)
		{
			cnt=1;
			var = rs2->Fields->GetItem(L"NonPayment")->GetValue();
			strBL2->Append(bstrToCString(var.bstrVal));
			var = rs2->Fields->GetItem(L"NonPaymentCnt")->GetValue();
			strBL1->Append(bstrToCString(var.bstrVal));
			var = rs2->Fields->GetItem(L"Code")->GetValue();
			tmpstr = bstrToCString(var.bstrVal);

			rs2->Close();
		}
	}
	else
	{
		retcode = SQL_ERROR;
	}

	if(retcode == SQL_ERROR)
	{
		return -1;
	}

	//BL테이블에서 아무것도 검색되지 않았다면
	if(cnt <= 0)
	{
		if(PLtype > 0)
			return 2;			// pl
		else if(mtstPl > 0)
			return 7;			// 1~3종 사업용 심야할인
		else
			return 0;			// 없음
	}

	// bl 데이터가 있음...
	rValue = 0;
	if(tmpstr.GetLength() >= 2)
	{
		idx = tmpstr.GetAt(1);
	}
	else
	{
		idx = '9';
	}
	
	// 0=Nothing, 1=BL, 2=PL, 3=BL/PL, 4=RL, 5=BL/RL, 6=화물할인
	// 7=1~3종 심야할인
	// 8=환불 + 1~3종 심야할인
	// 9=bl + 1~3종 심야할인

	// idx : bl테이블의 code 컬럼 데이터의 2번째 캐릭터
	switch( idx )	//일반 B/L
	{
	case '0' :
		if(PLtype > 0)
			rValue = 3;		// P/L + B/L
		else if(mtstPl > 0)
			rValue = 9;		// BL + 1~3종 심야할인
		else
			rValue = 1;		// BL 
		break;
		
	case '1' :	//R/L, 운행제한 차량
		rValue = 4;
		break;

	case '2' :	// R/L + B/L
		rValue = 5;
		break;
		
	case '3' :	//2012.11.20 SIJ - 환불차량
		if(mtstPl == 1)
			rValue = 8;		// 환불 + 1~3종 심야할인
		else
			rValue = 6;
		break;
	}
	
	return rValue;
}

/////////////////////////////////////////////////////////////////////////////
//	DB에 카메라의 펌웨어 버전을 업데이트 하는 함수

/**
* DB에 카메라 펌웨어 버전을 업데이트 한다. 
*
*@param		ICID				
*@param		TCSNum			
*@param		strLane_State		
*/
int UpdateVer(int ICID, int TCSNum, CString strLane_State)
{
	CString tmpstr;
	RETCODE retcode = SQL_SUCCESS;
	ADODB::_RecordsetPtr rs1=NULL, rs2=NULL;

	char strSQL[512];

	if (bConnectedDB == FALSE)
	{
		return -1;
	}

	//2019.08.27 - 테이블 존재여부 확인
	memset(strSQL, 0, 512);
	sprintf_s(strSQL, sizeof(strSQL), "SELECT * from sysobjects where name = Lane_Ver_State");
	try
	{
		rs1 = g_pConn->Execute(_bstr_t(strSQL), NULL, adCmdText);
	}
	catch(...)
	{
		retcode = SQL_ERROR;
		rs1 = NULL;	//2020.1.11 SIJ
	}

	if(rs1 == NULL)
	{
		retcode = SQL_ERROR;
		return retcode;
	}
	
	// Execute an SQL statement directly on the statement handle.
	// Uses a default result set because no cursor attributes are set.
	memset(strSQL, 0, 512);
	sprintf_s(strSQL, sizeof(strSQL), "SELECT Lane_Ver FROM Lane_Ver_State where IC_ID='%03d' and TCSNum='%02d'", ICID, TCSNum);

	try
	{
		rs1 = g_pConn->Execute(_bstr_t(strSQL), NULL, adCmdText);
	}
	catch(...)
	{
		retcode = SQL_ERROR;
	}

	if(rs1 != NULL)
	{
		if(!rs1->adoEOF)
		{
			sprintf_s(strSQL, sizeof(strSQL), "UPDATE Lane_Ver_State SET Lane_Ver = '%s' where IC_ID='%03d' and TCSNum='%02d'", MT_VERSION, ICID, TCSNum);
		}
		else
		{
			sprintf_s(strSQL, sizeof(strSQL), "INSERT INTO Lane_Ver_State (IC_ID, TCSNum, Lane_Ver, Lane_State) VALUES ('%03d','%02d','%s','%s')", 
						ICID, TCSNum, MT_VERSION, strLane_State);
		}
	}
	else
	{
		retcode = SQL_ERROR;
		return retcode;
	}

	rs1->Close();

	if(retcode == SQL_ERROR)
	{
		return -1;
	}

	try
	{
		rs2 = g_pConn->Execute(_bstr_t(strSQL), NULL, adExecuteNoRecords);
	}
	catch(...)
	{
		retcode = SQL_ERROR;
	}
	
	if(rs2 == NULL)
	{
		retcode = SQL_ERROR;
	}

	if(retcode == SQL_ERROR)
	{
		return -2;
	}
	else{
		return SQL_SUCCESS;
	}
}
/**
* DB에 연결한다. 
*
*@param		HostAddr		호스트 주소
*@param		User			유저ID
*@param		Pass			비밀번호
*/
int DBConnect(CString HostAddr, CString User, CString Pass)
{
	HRESULT hr;
	CString constr;
	ADODB::_RecordsetPtr rs=NULL;
	char strSQL[128];

	constr.Format(DBCONNECT_STRING1, User, Pass, HostAddr);
	constr += DBCONNECT_STRING2;

	_bstr_t strProvider(constr);
	_bstr_t strUser(User);
	_bstr_t strPass(Pass);


	try
	{
		hr = CoInitialize(NULL);
		if(FAILED(hr) && (hr != FALSE) )
		{
			return SQL_ERROR - 6;
		}
		g_pConn.CreateInstance(L"ADODB.Connection");

		g_pConn->ConnectionTimeout = 5;
		g_pConn->CommandTimeout = 5;

		hr = g_pConn->Open(strProvider, strUser, strPass, adConnectUnspecified);
		if(FAILED(hr))
		{
			return SQL_ERROR;
		}

	}
	catch(...)
	{
		return SQL_ERROR - 2;
	}

	sprintf_s(strSQL, sizeof(strSQL), "SELECT Code FROM MasterBL where CarNum = ''\0");

	try
	{
		rs = g_pConn->Execute(_bstr_t(strSQL), NULL, adCmdText);
	}
	catch(...)
	{
		return SQL_ERROR - 3;
	}

	bConnectedDB = TRUE;

	return SQL_SUCCESS;
}

/**
* DB 접속을 해제한다. 
*/
void disconnectDB()
{
	if(bConnectedDB == FALSE)
	{
		return;
	}

	bConnectedDB = FALSE;

	try
	{
		g_pConn->Close();
		g_pConn.Release();
	}
	catch(...)
	{
	}
}

/**
* DB의 데이터를 찾는 스레드이다. 
*/
UINT SearchThread(LPVOID lpParam)
{
	CJWIS_TCSDlg* pDlg  = (CJWIS_TCSDlg*)lpParam;
	int			dwResult;
	CString		tmpstr;
	DWORD		wResult;
	CString		strlog;

	g_pDlg = pDlg;

	g_bSearchThread = true;

	while(g_bSearchThread)
	{
		Sleep(1);
		
		/*************************************** Databse 연결 체크 ***************************************/
		if(bConnectedDB == FALSE)
		{
			//2013.4.25 SIJ
			if( (pDlg == NULL) || (!g_bSearchThread) )
			{	
				continue;
			}

			strlog.Format("(전송 TASK) 서버데이터베이스(%s)에 연결시도.\n", pDlg->m_ftpAddr);					
			pDlg->AddLog(strlog);
			pDlg->m_stsODBC = DBConnect(pDlg->m_ftpAddr, pDlg->m_DBUser, pDlg->m_DBPass);
			if(pDlg->m_stsODBC == SQL_SUCCESS)
			{
				strlog.Format("(전송 TASK) 서버데이터베이스(%s)에 연결됨.\n", pDlg->m_ftpAddr);					
				pDlg->AddLog(strlog);

				if(bFirst == TRUE)
				{
					bFirst = FALSE;
					strlog.Format("%s", MT_LANESTATE_1);
					dwResult = UpdateVer(pDlg->m_NumOffice, pDlg->m_NumLane, strlog);
				}
				PostMessage(pDlg->m_hWnd, WM_SET_STATUS, STATUS_ODBC, STATUS_CONNECT);
			}
			else
			{
				strlog.Format("(전송 TASK) 서버데이터베이스(%s)에 연결실패.\n", pDlg->m_ftpAddr);					
				pDlg->AddLog(strlog);
				disconnectDB();
				pDlg->m_stsODBC = SQL_ERROR;
			}
		}

		if(bConnectedDB == FALSE)
		{
			Sleep(500);
			SetEvent(g_BLEvent_done);
			continue;
		}

		wResult = WaitForSingleObject(g_BLEvent_wait, 10);
		if(wResult != WAIT_OBJECT_0)
		{
			SetEvent(g_BLEvent_done);
			continue;
		}

		ResetEvent(g_BLEvent_wait);
		try
		{
			dwResult = SearchList(g_strPlate, &pDlg->m_BLData[0], &pDlg->m_BLData[1]);
		}
		catch(...)
		{
			dwResult = -1;
		}

		pDlg->AddLog("===5===", LOG_FILE);
		if(dwResult == -1)
		{
			pDlg->m_BLtype = 0;			//2013.4.15 
			pDlg->m_BLSearchTime = 0;
			tmpstr.Format("[영상Thread] 서버데이터베이스와 연결해제시도중...\n");
			pDlg->AddLog(tmpstr);
			disconnectDB();
			tmpstr.Format("[영상Thread] 서버데이터베이스와 연결해제됨(Code1).\n");
			pDlg->AddLog(tmpstr);
			PostMessage(pDlg->m_hWnd, WM_SET_STATUS, STATUS_ODBC, STATUS_DISCONNECT);
			pDlg->m_stsODBC = SQL_ERROR;
		}
		else
		{
			// BL 유무 코드가 9가지로 변경
			if( (dwResult >= 0) && (dwResult <= 9) )
				pDlg->m_BLtype = dwResult;
			else
				pDlg->m_BLtype = 0;
		}
		SetEvent(g_BLEvent_done);
	}	// while

	return 0;
}

/**
* 이미지를 로드하는 스레드이다. 
*/
UINT LoadImgThread(LPVOID lpParam)
{
	CJWIS_TCSDlg* pDlg = (CJWIS_TCSDlg*)lpParam;
	WIN32_FIND_DATA		ffData;
	HANDLE		findhandle;
	CString		imgPath;
	CString		svrPath;
	CString		prevName;
	int			bFindResult;			// 이미지 찾았는지 여부
	CString		tmpstr;
	DWORD		TickCnt = 0;
	int			dwResult1;
	HANDLE		jpgEvent;
	int			waitResult;

	imgPath = pDlg->m_carImgPath;

	prevName = "";
	//2012.1.31 CoInitialize를 DBConnect속으로

	g_pDlg = pDlg;		// cjw

	jpgEvent = pDlg->m_jpgEvent;

	tmpstr.Format("[영상Thread] 영상파일 검색 Thread 시작\n");
	pDlg->AddLog(tmpstr);

	g_bImgThread = true;
	while (g_bImgThread) 
	{
		waitResult = WaitForSingleObject(jpgEvent, 0); //2013.5.29
		if (waitResult != WAIT_OBJECT_0) 
		{
			Sleep(100);
			continue;
		}

		bFindResult = FALSE;
		memset( & ffData, 0, sizeof(ffData));
		findhandle = FindFirstFile(imgPath + "\\*.jpg",  & ffData);		// 차량 번호판 이미지 찾음

		if (findhandle != INVALID_HANDLE_VALUE) 
		{
			pDlg->AddLog("\n");
			pDlg->AddLog("===========1===", LOG_FILE);
			bFindResult = TRUE;
			FindClose(findhandle);
		} 
		else 
		{
			findhandle = FindFirstFile(imgPath + "\\*.wmv",  & ffData);
			if (findhandle != INVALID_HANDLE_VALUE) 
			{
				pDlg->AddLog("\n");
				pDlg->AddLog("=========== 1-1 ===", LOG_FILE);
				// @@cjw - 현재는 가능성 없으니...주석...bmt에서 다시 설정 필요
				//pDlg->m_extWMV = TRUE;
				bFindResult = TRUE;
				FindClose(findhandle);
			}
		}

		// 이미지를 찾으면 처리
		if (bFindResult) 
		{
			pDlg->m_strCimg.Format("%s\0", ffData.cFileName);
			if (prevName.Compare(pDlg->m_strCimg) == 0) 
			{
				ResetEvent(jpgEvent);
				PostMessage(pDlg->m_hWnd, WM_NEW_IMAGE, 0, 0);		// OnNewImage 호출
			}
			else 
			{
				pDlg->m_BLtype = -1;
				pDlg->m_BLSearchTime = 0;

				//2013.4.25 - 위치이동 GetPlateString 다음으로
				pDlg->AddLog("===2===", LOG_FILE);
				ResetEvent(jpgEvent);
				Sleep(1);
				TickCnt = GetTickCount();
				PostMessage(pDlg->m_hWnd, WM_NEW_IMAGE, 0, 0);		// OnNewImage 호출

				pDlg->AddLog("===3===", LOG_FILE);

				waitResult = WaitForSingleObject(g_BLEvent_done, 0);
				if (waitResult == WAIT_OBJECT_0) 
				{
					ResetEvent(g_BLEvent_done);

					pDlg->AddLog("===2===", LOG_FILE);
					if (pDlg->m_extWMV == FALSE) 
					{
						try 
						{
							memset(g_strPlate, 0, sizeof(g_strPlate)); //2015.1.26 SIJ
							dwResult1 = GetPlateString(ffData.cFileName, g_strPlate);
						} 
						catch (...) 
						{
							dwResult1 = FALSE;
						}
					}
					if (dwResult1 == TRUE)
						SetEvent(g_BLEvent_wait);
				} 
				else{
					pDlg->m_BLtype = 0;
				}

				pDlg->AddLog("===6===\n", LOG_FILE);
				pDlg->m_BLSearchTime = GetTickInterval(TickCnt);

				prevName = pDlg->m_strCimg;
				Sleep(500);
			}
		}
		else 
		{
			if (prevName.GetLength() > 0)
			{
				prevName.Empty();
			}
		}

		Sleep(10);
	}
	disconnectDB();
	CloseHandle(pDlg->m_jpgEvent);

	//AfxEndThread(0);
	return 0;
}



//2013.12.01 - ftpThread 내의 ping테스트하는 로직을 삭제함
char ftpThreadBuffer[2 * 1024 * 1024];	// 3200 * 2880 고려 (면탈 프로그램 normal image max size)

/**
* FTP 스레드를 처리한다. 
*/
UINT ftpThread(LPVOID lpParam)
{
	CJWIS_TCSDlg* pDlg = (CJWIS_TCSDlg*)lpParam;
	CMtFtpSock*	ftp;
	int		bufsize;
	char	filename[256];
	char	imgfilename[256];
	CStdioFile listfile;
	CFile	imgfile;
	int		sum, i;
	BOOL	bsuccess;
	int		retFtp;
	//test
	fd_set	rfds;
	struct	timeval selTimeout;
	int		set_ret;
	int		sel_ret;
	char	buftmp[100];
	int		rcvlen;
	char	tmpstr[100];
	char	logstr[512];
	int		dwResult = 0;
	int		FtpConnCnt = 0;
	CString strlog;
	ULONGLONG tickcnt1, tickcnt2, tickcntSVR1, tickcntSVR2;
	WIN32_FIND_DATA	ffData;
	HANDLE			findhandle;

	g_bFtpThread = TRUE;
	ftp = &pDlg->m_ftp;
	ftp->SetListenPort(pDlg->m_ftpPort);
	tickcnt1    = 0;
	tickcntSVR1 = 0;

	CString		addr, svrPath;
	HWND		hWnd;	

	while(g_bFtpThread)
	{
		Sleep(10);
		addr = pDlg->m_ftpAddr;
		svrPath = pDlg->m_svrImgPath;
		hWnd = pDlg->m_hWnd;

		/******************************** PING(ICMP)을 이용한 서버이상유무 체크 ********************************/
		if(pDlg->m_mtSvrSock.m_connected == FALSE)
		{
			tickcntSVR2 = GetTickCount();
			if(tickcntSVR2 - tickcntSVR1 > (60 * 1000))
			{
				strlog.Format("면탈서버에 접속시도중...\n");
				pDlg->AddLog(strlog);
				dwResult = pDlg->m_mtSvrSock.Connect(pDlg->m_ftpAddr, pDlg->m_SvrPort);
				tickcntSVR1 = tickcntSVR2;
				/*
				if(pDlg->m_mtSvrSock.m_connected == FALSE)
				{
					strlog.Format("면탈서버에 접속실패.\n");
					pDlg->AddLog(strlog);
				}
				*/
				if (dwResult == TRUE)
				{
					pDlg->m_mtSvrSock.m_connected = TRUE;
					strlog.Format("면탈서버에 접속성공. 대기중...\n");
					pDlg->AddLog(strlog);
				}
				else
				{
					strlog.Format("면탈서버에 접속실패. \n");
					pDlg->AddLog(strlog);
				}
			}
			Sleep(50);
		}
		/************************************* 업로드 파일 존재여부 체크 *************************************/
		if( (pDlg == NULL) || (!g_bFtpThread) )
		{
			continue;
		}

		bufsize = 0;
		bsuccess = FALSE;
		findhandle = FindFirstFile(svrPath + "\\*.jpg", &ffData);	// D:\SvrImg 폴더에 FTP 전송할 그림 찾기
		if( findhandle != INVALID_HANDLE_VALUE ) 
		{
			memset(imgfilename, 0, sizeof(imgfilename));
			sprintf_s(imgfilename, 256, "%s\0", ffData.cFileName);
			memset(filename, 0, sizeof(filename));
			sprintf_s(filename, sizeof(filename), "%s\\%s", svrPath, imgfilename);
			bsuccess = TRUE;
		}
		FindClose(findhandle);
		Sleep(10);

		/************************************** 영상파일을 메모리에 로드 **************************************/
		//파일정보의 저장
		if( bsuccess == FALSE ){
			Sleep(100);
			continue;
		}
		else{
			Sleep(10);
			//파일 오픈에서 에러가 발생하면
			if( imgfile.Open(filename, CFile::modeRead ) == 0){
				bufsize = 0;
				continue;
			}
			else{
				bufsize = (int)imgfile.GetLength();
				if(bufsize < sizeof(ftpThreadBuffer)){
					sum = 0;
					while(sum < bufsize)
					{
						i = imgfile.Read(&ftpThreadBuffer[sum], bufsize);
						sum += i;
					}
					imgfile.Close();
				}
				// 2MB이상의 파일은 업로드할 수 없음 - 2012.11.06
				else{
					imgfile.Close();
					DeleteFile(filename);
					continue;
				}
			}
		}

		/************************************** FTP 접속시도 **************************************/
		//2013.4.25 SIJ
		if( (pDlg == NULL) || (!g_bFtpThread) )
		{
			continue;
		}

		if(ftp->m_connected == FALSE)
		{
			tickcnt2 = GetTickCount();
			//2012.4.30 SIJ 120 --> 2
			if(tickcnt2 - tickcnt1 > 2000)
			{
				if(ftp->FtpConnect(pDlg->m_ftpAddr, pDlg->m_ftpUser, pDlg->m_ftpPass, pDlg)){
					PostMessage(pDlg->m_hWnd, WM_SET_STATUS, STATUS_FTP, STATUS_CONNECT);
				}
				else
				{
					PostMessage(pDlg->m_hWnd, WM_SET_STATUS, STATUS_FTP, STATUS_DISCONNECT);
				}
			}
			//2012.1.30 SIJ - 일정한 시간후에 재접속 시도하며, 이후의 전송시도 루틴은 실행하지 않는다.
			else	
			{
				continue;
			}
		}

		/************************************** FTP 전송시도 **************************************/
		//전송시도
		bsuccess   = FALSE;
		retFtp = 1;
		if( (bufsize > 0) && (sum >= bufsize) )
		{
			//FTP 접속 여부 체크
			FD_ZERO(&rfds);
			FD_SET(ftp->m_sock, &rfds);
			selTimeout.tv_sec  = 0;
			selTimeout.tv_usec = 0;
			sel_ret = select(0, &rfds, NULL, NULL, &selTimeout);
			if(sel_ret == 1)
			{
				set_ret = FD_ISSET(ftp->m_sock, &rfds);
				if(set_ret == 1)
				{
					memset(buftmp, 0, sizeof(buftmp));
					rcvlen = recv(ftp->m_sock, buftmp, 100, 0);
					if( rcvlen <= 0)
					{
						ftp->FtpDisconnect();
						pDlg->AddLog("(전송 TASK) FTP 소켓연결 종료 by select()\n");
						PostMessage(pDlg->m_hWnd, WM_SET_STATUS, STATUS_FTP, STATUS_DISCONNECT);
						tickcnt1 = GetTickCount();
					}
					else
					{
						i = ftp->GetRcvCode(buftmp);

						memset(tmpstr, 0, sizeof(tmpstr));
						if(rcvlen > 80)
						{
							strncpy_s(tmpstr, sizeof(tmpstr), buftmp, 80);
						}
						else
						{
							strncpy_s(tmpstr, sizeof(tmpstr), buftmp, rcvlen);
						}

						memset(logstr, 0, sizeof(logstr));
						sprintf_s(logstr, sizeof(logstr), "'%d' - %s\n", i, buftmp);
						pDlg->AddLog(logstr);
						//2013.1.9 연속으로 재접속시도하는 것을 방지
						if (i == 421)
						{
							ftp->FtpDisconnect();
							PostMessage(pDlg->m_hWnd, WM_SET_STATUS, STATUS_FTP, STATUS_DISCONNECT);
							tickcnt1 = GetTickCount();
						}
					}
				}
			}

			if(ftp->m_connected == TRUE)
			{
				FtpConnCnt = 0;		//2014.12.19 SIJ
				if(ftp->m_TransListenSock == INVALID_SOCKET)
				{
					ftp->CreateListenSocket(pDlg->m_hWnd, WM_MSG_FTPSOCK);
				}
				
				if(ftp->m_TransListenSock != INVALID_SOCKET)
				{
					pDlg->AddLog("(전송 TASK) FTP 전송시작 (하나씩)\n");
					try
					{
						retFtp = ftp->UploadBuffer(imgfilename, ftpThreadBuffer, bufsize, pDlg->m_gateType);
					}
					catch(...)
					{
						memset(logstr, 0, sizeof(logstr));
						sprintf_s(logstr, sizeof(logstr), "(전송 TASK) FTP업로드 실패(catch문)\n", imgfilename);
						pDlg->AddLog(logstr);
					}
				}
				shutdown(ftp->m_TransListenSock, SD_BOTH);
				closesocket(ftp->m_TransListenSock);
				ftp->m_TransListenSock = INVALID_SOCKET;
				Sleep(50);
			}
			else	
			{
				FtpConnCnt++;
				if(FtpConnCnt > 10)
				{
					FtpConnCnt = 0;
				}
			}
		}

		/************************************** FTP 전송결과 체크 **************************************/
		if( (pDlg == NULL) || (!g_bFtpThread) ){
			continue;
		}

		memset(logstr, 0, sizeof(logstr));
		
		//해당파일의 삭제
		if(retFtp == 0)
		{				
			memset(logstr, 0, sizeof(logstr));
			sprintf_s(logstr, sizeof(logstr), "(전송 TASK) [FTP] 파일전송 성공 : %s\n", imgfilename);
			pDlg->AddLog(logstr);
			DeleteFile(filename);
			Sleep(10);
		}
		//업로드에 실패하면? 재전송? 파일 삭제? - 1회만 재시도?
		else
		{
			memset(logstr, 0, sizeof(logstr));
			sprintf_s(logstr, sizeof(logstr), "(전송 TASK) FTP업로드 실패, 에러코드 = %04X\n", retFtp);
			pDlg->AddLog(logstr);
			if(ftp->m_connected == TRUE)
			{
				ftp->FtpDisconnect();
				PostMessage(pDlg->m_hWnd, WM_SET_STATUS, STATUS_FTP, STATUS_DISCONNECT);
			}
			tickcnt1 = GetTickCount();
		}
	}	// while ------

	if(pDlg != NULL)
	{
		try
		{
			if(pDlg->m_ftpEvent != NULL)
			{
				CloseHandle(pDlg->m_ftpEvent);
			}
			if(pDlg->m_svrConnEvent != NULL)
			{
				CloseHandle(pDlg->m_svrConnEvent);
			}
		}
		catch (...)
		{
		}
	}

	return 0;
}

/**
* 백업 파일을 삭제하는 스레드이다. 
*/
UINT EraseBackupFileThread(LPVOID lpParam)
{
	CJWIS_TCSDlg* pDlg  = (CJWIS_TCSDlg*)lpParam;

	WIN32_FIND_DATA	ffData;
	HANDLE			findhandle;
	CString			imgPath;
	int		FindResult;
	int		i;
	int		IsStrNum = FALSE;
	char	strDelPath[512];
	int		iYear, iMon, iDay;
	int		iDayTotal1, iDayTotal2;
	int		timeInterval;
	SYSTEMTIME	st;
	FILETIME	ftLocal;

	GetLocalTime(&st);
	imgPath = pDlg->m_bakImgPath;		// D:/TcsBak

	findhandle = FindFirstFile(imgPath + "\\*.*", &ffData);
	if(findhandle != INVALID_HANDLE_VALUE)
	{
		FindResult = 1;
	}
	else
	{
		FindResult = 0;
	}

	while(FindResult > 0)
	{
		Sleep(100);

		if( (ffData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)==FILE_ATTRIBUTE_DIRECTORY)
		{
		// 1. 숫자로만 이루어진 폴더인지 확인하고
			if(strlen(ffData.cFileName) == 6)
			{
				IsStrNum = TRUE;
				for(i=0; i<6; i++)
				{
					if( (ffData.cFileName[i] < '0') || (ffData.cFileName[i] > '9') )
					{
						IsStrNum = FALSE;
					}
				}
				// 2. 6자리 숫자로 변환
				if(IsStrNum)
				{
					// 3. 현재의 날짜를 가져와서 숫자로 변환
					iYear = (ffData.cFileName[0]-'0') * 10  + (ffData.cFileName[1]-'0');
					iMon  = (ffData.cFileName[2]-'0') * 10  + (ffData.cFileName[3]-'0');
					iDay  = (ffData.cFileName[4]-'0') * 10  + (ffData.cFileName[5]-'0');

					iDayTotal1 = CalcDay(iYear, iMon, iDay);
					iDayTotal2 = CalcDay(st.wYear%100, st.wMonth, st.wDay);

					// 4. 7일 이상 차가 나면 삭제 - 2014.5.13 SIJ, 7일에서 14일로 변경
					timeInterval = iDayTotal2 - iDayTotal1;
					if(timeInterval > 15)
					{
						memset(strDelPath, 0, sizeof(strDelPath));
						sprintf_s(strDelPath, sizeof(strDelPath), "%s\\%s", pDlg->m_bakImgPath, ffData.cFileName);
						EmptyDirectory(strDelPath);
						RemoveDirectory(strDelPath);
					}
				}
			}
		}
		FindResult = FindNextFile(findhandle, &ffData);
	}

	FindClose(findhandle);



	///////////////////////////////////////////////////////////////////////////////////////////////////
	//   2019.08.27 SIJ TCSWorkBak 폴더파일 삭제
	///////////////////////////////////////////////////////////////////////////////////////////////////
	
	imgPath = pDlg->m_passBakImgPath;		//면탈촬영통과대수 백업 경로

	findhandle = FindFirstFile(imgPath + "\\*.*", &ffData);
	if(findhandle != INVALID_HANDLE_VALUE)
	{
		FindResult = 1;
	}
	else
	{
		FindResult = 0;
	}

	while(FindResult > 0)
	{
		Sleep(100);

		if( (ffData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)==FILE_ATTRIBUTE_DIRECTORY)
		{
		// 1. 숫자로만 이루어진 폴더인지 확인하고
			if(strlen(ffData.cFileName) == 6)
			{
				IsStrNum = TRUE;
				for(i=0; i<6; i++)
				{
					if( (ffData.cFileName[i] < '0') || (ffData.cFileName[i] > '9') )
					{
						IsStrNum = FALSE;
					}
				}
				// 2. 숫자로만 되어 있는 폴더이면 삭제
				if(IsStrNum)
				{
					memset(strDelPath, 0, sizeof(strDelPath));
					sprintf_s(strDelPath, sizeof(strDelPath), "%s\\%s", pDlg->m_passBakImgPath, ffData.cFileName);
					try	//2020,1.11 SIJ 삭제 에러를 대비
					{
						EmptyDirectory(strDelPath);
						RemoveDirectory(strDelPath);
					}
					catch(...)
					{
					}
				}
			}
		}
		FindResult = FindNextFile(findhandle, &ffData);
	}

	FindClose(findhandle);



	///////////////////////////////////////////////////////////////////////////////////////////////////
	//   PLT,TAG 폴더파일 삭제 시간의 기준 설정
	///////////////////////////////////////////////////////////////////////////////////////////////////
	CTime t = CTime::GetCurrentTime();
	t -= CTimeSpan(0, 1, 0, 0);	//1 hour exactly
	t.GetAsSystemTime(st);

	FILETIME ft;
	SystemTimeToFileTime(&st, &ft);
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//   PLT 폴더의 파일 - 전날의 것
	///////////////////////////////////////////////////////////////////////////////////////////////////


	imgPath = pDlg->m_pltImgPath;
	findhandle = FindFirstFile(imgPath + "\\*.jpg", &ffData);
	if(findhandle != INVALID_HANDLE_VALUE)
	{
		FindResult = 1;
	}
	else
	{
		FindResult = 0;
	}

	while(FindResult > 0)
	{
		Sleep(100);
		FileTimeToLocalFileTime( &ffData.ftCreationTime, &ftLocal );
		 //파일생성시간이 1시간 전
		if(CompareFileTime(&ft, &ftLocal) == 1)
		{
			DeleteFile(imgPath + "\\" + ffData.cFileName);
		}
		FindResult = FindNextFile(findhandle, &ffData);
	}

	FindClose(findhandle);
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//   TAG 파일 - 전날의 것
	///////////////////////////////////////////////////////////////////////////////////////////////////
	imgPath = pDlg->m_tagPath;
	findhandle = FindFirstFile(imgPath + "\\*.tag", &ffData);
	if(findhandle != INVALID_HANDLE_VALUE)
	{
		FindResult = 1;
	}
	else
	{
		FindResult = 0;
	}

	while(FindResult > 0)
	{
		Sleep(100);
		FileTimeToLocalFileTime( &ffData.ftCreationTime, &ftLocal );
		//파일생성시간이 1시간 전
		if(CompareFileTime(&ft, &ftLocal) == 1)
		{ 
			DeleteFile(imgPath + "\\" + ffData.cFileName);
		}
		FindResult = FindNextFile(findhandle, &ffData);
	}

	FindClose(findhandle);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//   LOG 파일 - 1주일 전의 것
	///////////////////////////////////////////////////////////////////////////////////////////////////
	t -= CTimeSpan(30, 0, 0, 0);		//10 days exactly 2014.5.13  30일로 변경
	t.GetAsSystemTime(st);

	SystemTimeToFileTime(&st, &ft);

	imgPath = pDlg->m_LogPath;
	findhandle = FindFirstFile(imgPath + "\\*.txt", &ffData);
	if(findhandle != INVALID_HANDLE_VALUE)
	{
		FindResult = 1;
	}
	else
	{
		FindResult = 0;
	}

	while(FindResult > 0)
	{
		Sleep(100);
		FileTimeToLocalFileTime( &ffData.ftCreationTime, &ftLocal );
		if(CompareFileTime(&ft, &ftLocal) == 1) //파일생성시간이 10일 전
			DeleteFile(imgPath + "\\" + ffData.cFileName);
		FindResult = FindNextFile(findhandle, &ffData);
	}

	FindClose(findhandle);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//   종료할 수 있을 때까지 대기
	///////////////////////////////////////////////////////////////////////////////////////////////////
	GetLocalTime(&st);
	while(st.wHour == 2)
	{
		Sleep(10000);
		GetLocalTime(&st);
	}

	pDlg->m_BackupFileErasing = FALSE;

	pDlg->AddLog("백업파일 삭제 Thread 종료\n");
	return 0;
}


// CJWIS_TCSDlg 대화 상자


char g_ftpBuffer[1024 * 1024];

CJWIS_TCSDlg::CJWIS_TCSDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CJWIS_TCSDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CJWIS_TCSDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_TCS_CONN, m_edit_tcsConn);
	DDX_Control(pDX, IDC_EDIT_SVR_CONN, m_edit_svrConn);
	DDX_Control(pDX, IDC_EDIT_FTP_CONN, m_edit_ftpStatus);
	DDX_Control(pDX, IDC_EDIT_ODBC_CONN, m_edit_odbcConn);
	DDX_Control(pDX, IDC_STATIC_VERSION, m_static_Version);
	DDX_Control(pDX, IDC_CHK_PRINT_LOG, m_chk_prtLog);
	DDX_Control(pDX, IDC_CHK_PRINT_TCS, m_chk_prtTCS);
	DDX_Control(pDX, IDC_CHK_SAVE_LOG, m_chk_saveLog);
	DDX_Control(pDX, IDC_STATIC_ENVIRONMENT1, m_static_env1);
	DDX_Control(pDX, IDC_STATIC_ENVIRONMENT2, m_static_env2);
	DDX_Control(pDX, IDC_RICHEDIT, m_redit_Log);
}

BEGIN_MESSAGE_MAP(CJWIS_TCSDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_NEW_IMAGE, &CJWIS_TCSDlg::OnNewImage)
	ON_MESSAGE(WM_MT_MSG, &CJWIS_TCSDlg::OnMtMsg)
	ON_MESSAGE(WM_STS_REQ, &CJWIS_TCSDlg::OnSockAccept)
	ON_MESSAGE(WM_MTSVR_SOCK, &CJWIS_TCSDlg::OnMtSvrSock)
	ON_MESSAGE(WM_MSG_FTPSOCK, &CJWIS_TCSDlg::OnFtpAccept)
	ON_MESSAGE(WM_FTP_DATASOCK, &CJWIS_TCSDlg::OnFTPDataSock)
	ON_MESSAGE(WM_SET_STATUS, &CJWIS_TCSDlg::OnSetStatus)
	ON_MESSAGE(WM_LOG_TKDATA, &CJWIS_TCSDlg::OnLogTkData)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_WRITE_INI, &CJWIS_TCSDlg::OnBtnWriteIni)
	ON_BN_CLICKED(IDC_BTN_INITQ, &CJWIS_TCSDlg::OnBnClickedBtnInitq)
	ON_BN_CLICKED(IDC_BTN_SAVE_LOG, &CJWIS_TCSDlg::OnBnClickedBtnSaveLog)
	ON_BN_CLICKED(IDC_BTN_ERASE_BACKUP, &CJWIS_TCSDlg::OnBnClickedBtnEraseBackup)
	ON_BN_CLICKED(IDC_BTN_TOW_TEST, &CJWIS_TCSDlg::OnBnClickedBtnTowTest)
	ON_BN_CLICKED(IDC_BTN_BACKTRIGGER, &CJWIS_TCSDlg::OnBnClickedBtnBacktrigger)
	ON_BN_CLICKED(IDC_BTN_EXIT, &CJWIS_TCSDlg::OnBnClickedBtnExit)
END_MESSAGE_MAP()


// CJWIS_TCSDlg 메시지 처리기

BOOL CJWIS_TCSDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

		
	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	timeBeginPeriod(1);
	CoInitialize(NULL);

	// TODO: 여기에 추가 초기화 작업을 추가합니다. ------------------------------------------

	m_TakeBackImg = FALSE;
	m_extWMV      = FALSE;  // Result폴더에 동영상이 검색되었음

	// Priority 변경
	SetPriorityClass(GetCurrentProcess(), BELOW_NORMAL_PRIORITY_CLASS);

	m_bTowCar		= FALSE;		// 2013.9.1
	m_svrStsCnt      = 0;			// 서버접속여부 이상 카운터
	m_LightErrorCnt  = 21;			// 10회 이상이면 조명이상으로...
	m_bShowLogTime   = 0;
	m_LoopRstTickCnt = UINT_MAX;

	//TCS 소켓전송 관련
	g_TCSEvent = CreateEvent(NULL, TRUE, TRUE, NULL);

	m_logEvent = CreateEvent(NULL, TRUE, TRUE, NULL);

	m_BackupFileErasing = FALSE;	//파일삭제 Thread가 기동중인지의 플래그

	//2013.5.10 SIJ, LCL에 전송할 영상의 크기의 디폴트값
	m_cImgW = 484;
	m_cImgH = 364;
	m_pImgW = 144;
	m_pImgH = 84;	// 차후 가변조절
		
	// Window Title
	memset(m_TitleRecogSW, 0, sizeof(m_TitleRecogSW));
	sprintf_s(m_TitleRecogSW, sizeof(m_TitleRecogSW), "%s", TITLE_RECOG_SW);
		
	// Read ini File
	Read_Ini();

	// 기타 초기화
	m_AckMode = 0;
	m_ImgThread = NULL;
	m_mtSvrSock.SetWnd(m_hWnd);		// m_MtSvrCSock
	m_tcsLink.Init(m_hWnd);
	m_tcsLink.m_NumLane = m_NumLane;

	//LCL 접속 Accept용 소켓을 포인터로 변환 -> 후에 통신에러시 delete하려고
	//InitLCLPort();
	m_SvrSock = new CServerSock;
	m_SvrSock->Init(m_hWnd);
	m_SvrSock->m_pAgentSock->m_ptcsLnk = &m_tcsLink;
	m_SvrSock->Listen(m_lclPort, 5);

	// B/L문구 초기화
	m_cxCimg.InitTextInfo(&m_BLtext);
	m_BLtext.bcolor = 0xFFFFFFFF;
	m_BLtext.fcolor = 0x00000000;
	m_BLtext.lfont.lfHeight = 150;
	m_BLtext.lfont.lfCharSet = HANGUL_CHARSET;
	m_BLtext.align = DT_RIGHT;
	m_BLtext.b_round = false;
	memset(m_BLtext.lfont.lfFaceName, 0, sizeof(m_BLtext.lfont.lfFaceName));
	memset(m_BLtext.text, 0, sizeof(m_BLtext.text));
	sprintf_s(m_BLtext.lfont.lfFaceName, sizeof(m_BLtext.lfont.lfFaceName) , "나눔고딕 ExtraBold");

	sprintf_s(m_BLtext.text, sizeof(m_BLtext.text), "B/L차량");

	// R/L문구 초기화
	m_cxCimg.InitTextInfo(&m_RLtext);
	m_RLtext.bcolor = 0xFFFFFFFF;
	m_RLtext.fcolor = 0x00000000;
	m_RLtext.lfont.lfHeight = 90;
	m_RLtext.lfont.lfCharSet = HANGUL_CHARSET;
	m_RLtext.align = DT_RIGHT;	//2012.10.04 SIJ
	m_RLtext.b_round = false;
	memset(m_RLtext.lfont.lfFaceName, 0, sizeof(m_RLtext.lfont.lfFaceName));
	memset(m_RLtext.text, 0, sizeof(m_RLtext.text));
	sprintf_s(m_RLtext.lfont.lfFaceName, sizeof(m_RLtext.lfont.lfFaceName) , "나눔고딕 ExtraBold");
	sprintf_s(m_RLtext.text, sizeof(m_RLtext.text), "제한차량");

	// P/L문구 초기화
	m_cxCimg.InitTextInfo(&m_PLtext);
	m_PLtext.bcolor = 0xFFFFFFFF;
	m_PLtext.fcolor = 0x00000000;
	m_PLtext.lfont.lfHeight = 90;
	m_PLtext.lfont.lfCharSet = HANGUL_CHARSET;
	m_PLtext.align = DT_RIGHT;	//2012.10.04 SIJ
	m_PLtext.b_round = false;
	memset(m_PLtext.lfont.lfFaceName, 0, sizeof(m_PLtext.lfont.lfFaceName));
	memset(m_PLtext.text, 0, sizeof(m_PLtext.text));
	sprintf_s(m_PLtext.lfont.lfFaceName, sizeof(m_PLtext.lfont.lfFaceName) , "나눔고딕 ExtraBold");
	sprintf_s(m_PLtext.text, sizeof(m_PLtext.text), "경차주의");

	// 환불문구 초기화 - 2012.11.20
	m_cxCimg.InitTextInfo(&m_RFtext);
	m_RFtext.bcolor = 0xFFFFFFFF;
	m_RFtext.fcolor = 0x00000000;
	m_RFtext.lfont.lfHeight = 150;
	m_RFtext.lfont.lfCharSet = HANGUL_CHARSET;
	m_RFtext.align = DT_RIGHT;
	m_RFtext.b_round = false;
	memset(m_RFtext.lfont.lfFaceName, 0, sizeof(m_RFtext.lfont.lfFaceName));
	memset(m_RFtext.text, 0, sizeof(m_RFtext.text));
	sprintf_s(m_RFtext.lfont.lfFaceName, sizeof(m_RFtext.lfont.lfFaceName) , "나눔고딕 ExtraBold");
	sprintf_s(m_RFtext.text, sizeof(m_RFtext.text), "환불차량");

	// 심야할인 문구 초기화
	m_cxCimg.InitTextInfo(&m_NightDCtext);
	m_NightDCtext.bcolor = 0xFFFFFFFF;
	m_NightDCtext.fcolor = 0x00000000;
	m_NightDCtext.lfont.lfHeight = 90;
	m_NightDCtext.lfont.lfCharSet = HANGUL_CHARSET;
	m_NightDCtext.align = DT_RIGHT;
	m_NightDCtext.b_round = false;
	memset(m_NightDCtext.lfont.lfFaceName, 0, sizeof(m_NightDCtext.lfont.lfFaceName));
	memset(m_NightDCtext.text, 0, sizeof(m_NightDCtext.text));
	sprintf_s(m_NightDCtext.lfont.lfFaceName, sizeof(m_NightDCtext.lfont.lfFaceName) , "나눔고딕 ExtraBold");
	sprintf_s(m_NightDCtext.text, sizeof(m_NightDCtext.text), "심야");

	m_cxPimg.SetJpegQuality(50);	//면탈서버 전송용
	m_cxCimg.SetJpegQuality(35);

	CString logstr;
	AddLog("\n");
	logstr.Format("영상촬영장치(면탈카메라(ID:%03d)) 운영시작\n", m_NumCamera);
	AddLog(logstr);

	m_ftpFile = NULL;

	SetTimer(WM_TIMER_INIT, 1000, NULL);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}


// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CJWIS_TCSDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CJWIS_TCSDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//---------------------------------------------------------------------------------
//
//									Start
//
//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------
//			Read Ini File
//
void CJWIS_TCSDlg::Read_Ini()
{
	CString		keyString;
	CString		iniName;
	CString		strenv;
	char*		tmpstr;
	TCHAR		pszPathName[MAX_PATH];

	// Window Version
	strenv.Format("버전 : %s \n%s", MT_VERSION, MT_BUILD_NUM);
	m_static_Version.SetWindowText(strenv);

	GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, MAX_PATH);
	iniName.Format("%s", pszPathName);

	int brk = 0;
	brk = iniName.ReverseFind('\\');
	if (brk)
	{
		iniName = iniName.Left(brk);
	}

	m_emptyImageName = iniName + "\\" + "00000000000000000000000XXXXXXXXXXXX.jpg";
	iniName = iniName + "\\jwis_tcs.ini";

	// PATH
	tmpstr = m_carImgPath.GetBuffer(256);
	GetPrivateProfileString(PATH, CAR_IMAGE_PATH, "D:\\JWIS\\Result\\jpeg", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_carImgPath.ReleaseBuffer();

	tmpstr = m_pltImgPath.GetBuffer(256);
	GetPrivateProfileString(PATH, PLATE_IMAGE_PATH, "D:\\JWIS\\Result\\plate", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_pltImgPath.ReleaseBuffer();

	tmpstr = m_tagPath.GetBuffer(256);
	GetPrivateProfileString(PATH, TAG_PATH, "D:\\JWIS\\Result\\tag", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_tagPath.ReleaseBuffer();

	tmpstr = m_LogPath.GetBuffer(256);
	GetPrivateProfileString(PATH, LOG_FILE_PATH, "D:\\JWIS\\Log", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_LogPath.ReleaseBuffer();

	tmpstr = m_svrImgPath.GetBuffer(256);
	GetPrivateProfileString(PATH, SERVER_IMAGE_PATH, "D:\\SvrImg", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_svrImgPath.ReleaseBuffer();

	//----- 통합서버 -----
	tmpstr = m_svrImgPathCopy.GetBuffer(256);
	GetPrivateProfileString(PATH, SERVER_IMAGE_COPY_PATH, "D:\\SvrImgCopy", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_svrImgPathCopy.ReleaseBuffer();
	//--------------------

	tmpstr = m_tmpImgPath.GetBuffer(256);
	GetPrivateProfileString(PATH, TEMP_IMAGE_PATH, "D:\\TmpImg", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_tmpImgPath.ReleaseBuffer();

	tmpstr = m_bakImgPath.GetBuffer(256);
	GetPrivateProfileString(PATH, BACKUP_IMAGE_PATH, "D:\\TcsBak", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_bakImgPath.ReleaseBuffer();

	tmpstr = m_passImgPath.GetBuffer(256);
	GetPrivateProfileString(PATH, WORK_IMAGE_PATH, "D:\\TcsWork", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_passImgPath.ReleaseBuffer();

	tmpstr = m_passBakImgPath.GetBuffer(256);
	GetPrivateProfileString(PATH, WORK_IMAGE_PATH, "D:\\TcsWorkBak", tmpstr, 256, iniName);
	CreateDir(tmpstr);
	m_passBakImgPath.ReleaseBuffer();

	// NETWORK
	tmpstr = m_MyEtherName.GetBuffer(256);
	GetPrivateProfileString(NETWORK, SELF_ETHERNET_NAME, "도공망",tmpstr, 256, iniName);
	m_MyEtherName.ReleaseBuffer();

	tmpstr = m_MyIPAddr.GetBuffer(256);
	GetPrivateProfileString(NETWORK, SELF_IP_ADDRESS, "",tmpstr, 256, iniName);
	m_MyIPAddr.ReleaseBuffer();

	tmpstr = m_MySubnet.GetBuffer(256);
	GetPrivateProfileString(NETWORK, SELF_SUBNET_MASK, "",tmpstr, 256, iniName);
	m_MySubnet.ReleaseBuffer();

	SetTCPIPAddress(m_MyEtherName, m_MyIPAddr, m_MySubnet);

	// FTP
	tmpstr = m_ftpAddr.GetBuffer(256);
	GetPrivateProfileString(NETWORK, SERVER_IP_ADDRESS, "192.168.10.4",tmpstr, 256, iniName);
	m_ftpAddr.ReleaseBuffer();

	tmpstr = m_ftpUser.GetBuffer(256); 
	GetPrivateProfileString(NETWORK, FTP_USERNAME, "tes", tmpstr, 256, iniName);
	m_ftpUser.ReleaseBuffer();

	tmpstr = m_ftpPass.GetBuffer(256); 
	GetPrivateProfileString(NETWORK, FTP_PASSWORD, "tes", tmpstr, 256, iniName);
	m_ftpPass.ReleaseBuffer();

	//FTP 통신 포트 번호
	m_ftpPort = GetPrivateProfileInt(NETWORK, FTP_DATA_PORT, 21, iniName);
	//서버연결 포트 번호
	m_SvrPort = GetPrivateProfileInt(NETWORK, MTSETVER_PORT, 1944, iniName);
	//LCL 통신 포트 번호
	m_lclPort = GetPrivateProfileInt(NETWORK, LCL_PORT, 8010, iniName);
	
	//서버와 통신시에 사용되는 장치번호
	m_NumCamera = GetPrivateProfileInt(ID, NUM_DEVICE, 201, iniName);
	//차로번호 2자리
	m_NumLane = GetPrivateProfileInt(ID, NUM_LANE, 1, iniName);
	//서버와 통신시에 사용되는 영업소번호
	m_NumOffice = GetPrivateProfileInt(ID, NUM_OFFICE, 101, iniName);

	//ODBC User Name
	tmpstr = m_DBUser.GetBuffer(256);
	GetPrivateProfileString(NETWORK, DBSERVER_ODBC_USER, "sa", tmpstr, 256, iniName);
	m_DBUser.ReleaseBuffer();
	//ODBC Password
	tmpstr = m_DBPass.GetBuffer(256);
	GetPrivateProfileString(NETWORK, DBSERVER_ODBC_PASS, "testes", tmpstr, 256, iniName);
	m_DBPass.ReleaseBuffer();

	m_gateType    = GetPrivateProfileInt(ID, GATE_TYPE, 0, iniName);
	m_MixedHipass = GetPrivateProfileInt(ID, HIPASS_MIXED, 0, iniName);
	// 개방식에서도 일반 통행권확인데이터(0x57)을 전송
	m_AllTkData   = GetPrivateProfileInt(ID, ALL_TICKET_DATA_SEND, 0, iniName); 
	m_SendDoubleCode = GetPrivateProfileInt(ID, SEND_DOUBLE_CODE, 0, iniName);
	m_tcsLink.m_SendDoubleCode = m_SendDoubleCode;		// 2013.5.30

	strenv.Format("영업소ID : %d, \t 서 버 I P : %s", m_NumOffice, m_ftpAddr);
	m_static_env1.SetWindowText(strenv);

	switch(m_gateType)
	{
	case CLOSE_GATE : 
		strenv.Format("차선번호 : %d, \t 카메라ID : %d, \t 폐쇄식출구", m_NumLane, m_NumCamera);
		break;
	case OPEN_UP_GATE : 
		strenv.Format("차선번호 : %d, \t 카메라ID : %d, \t 개방식상행", m_NumLane, m_NumCamera);
		break;
	case OPEN_DN_GATE : 
		strenv.Format("차선번호 : %d, \t 카메라ID : %d, \t 개방식하행", m_NumLane, m_NumCamera);
		break;
	}
	
	if(m_MixedHipass == TRUE){
		strenv += ",  혼용차로 모드";
	}
	else{
		strenv += ",  전용차로 모드";
	}
	
	m_static_env2.SetWindowText(strenv);

	//로그를 출력할 것인지에 대한 설정
	int tmp_i;
	tmp_i = GetPrivateProfileInt(LOG, PRINT_LOG, 0, iniName);
	if (tmp_i > 0)
	{
		m_chk_prtLog.SetCheck(1);
	}
	//로그를 자동 저장할 것인지에 대한 설정
	tmp_i = GetPrivateProfileInt(LOG, AUTOSAVE_LOG, 0, iniName);
	if (tmp_i > 0)
	{
		m_chk_saveLog.SetCheck(1);
	}	
	//TCS처리정보를 출력할 것인진에 대한 설정
	tmp_i = GetPrivateProfileInt(LOG, PRINT_TCS, 0, iniName);
	if (tmp_i > 0)
	{
		m_chk_prtTCS.SetCheck(1);
	}
}
/*
void CJWIS_TCSDlg::CreateDir(TCHAR* Path)
{
	TCHAR	DirName[256];			// 생성할 디렉토리 이름
	TCHAR*	p = Path;				// 인자로 받은 디렉토리
	TCHAR*	q = DirName;

	while (*p)
	{
		// 디렉토리 혹은 서브디렉토리
		if ((('\\') == *p) || (('/') == *p))
		{
			if ((':') != *(p - 1))
			{
				CreateDirectory(DirName, NULL);
			}
		}
		*q++ = *p++;
		*q = '\0';
	}
	CreateDirectory(DirName, NULL);
}
*/
void CJWIS_TCSDlg::SetTCPIPAddress(CString EtherName, CString IPAddr, CString SubnetMask)
{
	if(m_MyEtherName.IsEmpty()){
		return;
	}
	
	if(IPAddr.IsEmpty()){
		return;
	}
	
	if(SubnetMask.IsEmpty()){
		return;
	}

	//netsh interface ip add address name="도공망" addr=192.168.1.178 mask=255.255.255.0
	CString cmdline;
	cmdline.Format("netsh interface ip set address name=\"%s\" source=static addr=%s mask=%s",EtherName, IPAddr, SubnetMask);
	WinExec(cmdline, SW_HIDE);
	Sleep(100);

}

/**
* 로그를 파일에 표시한다. 
*
* @param		str			표시 문자
* @param		showtime	표시 옵션
*/
void CJWIS_TCSDlg::AddLog(LPCTSTR str, int showtime)
{
	CString strtm;
	SYSTEMTIME st;
	char logfilename[256];
	CStdioFile logfile;
	long nNewLines;
	long nScroll;
	char gateType;		// 출구 형태

	WaitForSingleObject(m_logEvent, 1000);
	ResetEvent(m_logEvent);

	GetLocalTime(&st);

	switch(m_gateType)
	{					
	case OPEN_UP_GATE : 
		gateType = 'U';		
		break;
	case OPEN_DN_GATE : 
		gateType = 'D';		
		break;
	case CLOSE_GATE:	
	default : 
		gateType = 'O';	//해당사항이 없으면 기본형은 TCS출구로 설정		
		break;
	}

	if(showtime == LOG_DAY)
	{
		if(m_chk_prtLog.GetCheck() == FALSE) 
		{
			return;
		}

		if(m_bShowLogTime > 0)
		{
			strtm.Format("\n[%02d-%02d %02d:%02d:%02d:%03d] ", st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		else
		{
			strtm.Format("[%02d-%02d %02d:%02d:%02d:%03d] ", st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		m_bShowLogTime = 0;
	}
	else if(showtime == LOG_TIME)
	{
		if(m_chk_prtLog.GetCheck() == FALSE)
		{
			SetEvent(m_logEvent);
			return;
		}

		m_bShowLogTime++;
		if(m_bShowLogTime > 100)
		{
			m_bShowLogTime = 0;
			strtm.Format("\n[%02d:%02d:%02d.%03d]  ", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
		else
		{
			strtm.Format("[%02d:%02d:%02d.%03d]  ", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		}
	}

	// 파일에 로그 출력
	if(showtime == LOG_FILE){
		memset(logfilename, 0, sizeof(logfilename));
		sprintf_s(logfilename, sizeof(logfilename), "%s\\mtLog%03d_%c%02d_%04d%02d%02d.txt\0", 
				  m_LogPath, m_NumOffice, gateType, m_NumLane, st.wYear, st.wMonth, st.wDay);
		if(logfile.Open(logfilename, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeReadWrite ))
		{
			logfile.SeekToEnd();
			logfile.WriteString(strtm + str);
			logfile.Close();
		}
	}
	// 화면에 로그 표시
	else{
		long nOldLines = m_redit_Log.GetLineCount();
		if(nOldLines > 1000)
		{
			m_redit_Log.Clear();
			m_redit_Log.SetWindowText("");
		}
		m_redit_Log.ReplaceSel(strtm + str);

		nNewLines = m_redit_Log.GetLineCount();
		// Scroll bt the number of lines just inserted
		nScroll = nNewLines - nOldLines;
		if(nScroll > 0)
		{
			m_redit_Log.LineScroll(nScroll);
		}

		if(m_chk_saveLog.GetCheck() > 0)
		{
			memset(logfilename, 0, sizeof(logfilename));
			sprintf_s(logfilename, sizeof(logfilename), "%s\\mtLog%03d_%c%02d_%04d%02d%02d.txt\0", 
					  m_LogPath, m_NumOffice, gateType, m_NumLane, st.wYear, st.wMonth, st.wDay);
			if(logfile.Open(logfilename, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeReadWrite ))
			{
				logfile.SeekToEnd();
				logfile.WriteString(strtm + str);
				logfile.Close();
			}
		}
	}
	SetEvent(m_logEvent);
}


void CJWIS_TCSDlg::OnTimer(UINT nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	MtPk*	mtpk = NULL;
	MtImg*	pimg = NULL;
	CString str, logstr;

	//서버 접속여부 체크
	fd_set	rfds;
	struct	timeval selTimeout;
	int		set_ret;
	int		sel_ret;
	char	buftmp[100];
	int		rcvlen;
	int		sndsize;

	KillTimer(nIDEvent);

	switch(nIDEvent)
	{
	case WM_TIMER_INIT:
		//FTP 업로드를 위한 초기화
		m_ftpEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
		m_svrConnEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		m_ftpThread = AfxBeginThread(ftpThread, this, THREAD_PRIORITY_BELOW_NORMAL);

		//2013.8.26 SIJ, BL검색 Thread
		g_BLEvent_wait = CreateEvent(NULL, TRUE, FALSE, NULL);
		g_BLEvent_done = CreateEvent(NULL, TRUE, TRUE, NULL);
		m_DBThread = AfxBeginThread(SearchThread, this, THREAD_PRIORITY_BELOW_NORMAL);

		//영상검색 Thread
		m_jpgEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
		m_ImgThread = AfxBeginThread(LoadImgThread, this, THREAD_PRIORITY_ABOVE_NORMAL);

		break;
	case WM_TIMER_MTSVR_CONNECT:
		str.Format("I%03d1.1", m_NumCamera);
		m_mtSvrSock.sendString(str);
		logstr.Format("면탈서버 %s(%04d)에 접속됨(%s)\n", m_ftpAddr, m_SvrPort, str);
		m_svrStsCnt = 0;
		SetSVRGUIStatus(STATUS_CONNECT);
		AddLog(logstr);

		SetTimer(WM_SVR_SOCK_CHECK, 5000, NULL);
		break;
	case WM_TIMER_STS:
		++m_cnt_sts;

		if (m_cnt_sts > 10) {
			if (m_SvrSock->m_pAgentSock->m_bConnected) {
				//2012.1.14 build.51 추가
				//2012.1.31 build.53 Close할 때 소켓의 유효성 여부 검사
				if (m_SvrSock->m_pAgentSock->m_hSocket != INVALID_SOCKET){
					m_SvrSock->m_pAgentSock->Close();
				}
				m_SvrSock->m_pAgentSock->m_bConnected = FALSE;
				AddLog("상태요청정보가 10회 이상 없어 LCL 연결을 끊습니다.\n");
				SetTCSCGUIStatus(STATUS_DISCONNECT);
			}
		}
		else if (m_cnt_sts > 0) {
			if (m_SvrSock->m_pAgentSock->m_bConnected) {
				mtpk = m_tcsLink.mkFrame(MT_CMD_ACK);
				if (mtpk != NULL) {
					Sleep(50);
					m_SvrSock->m_pAgentSock->SendData(mtpk->buffer, mtpk->size);
					logstr.Format("LCL연결유지를 위한 ACK전송\n");
					AddLog(logstr);
					m_cnt_sts = 0;
				}
			}
		}

		if (m_SvrSock->m_pAgentSock->m_bConnected == TRUE) //2012.1.14 build.51 추가
			SetTimer(WM_TIMER_STS, MT_STATUS_INTERVAL, NULL);
		break;
	case WM_TIMER_SYNCIMG:
		pimg = m_tcsLink.selectImg(FALSE, m_MixedHipass);
		if (pimg != NULL) {
			mtpk = m_tcsLink.mkFrame(MT_CMD_SYNC, pimg->QNum);
			if (mtpk != NULL) 
			{
				sndsize = m_SvrSock->m_pAgentSock->SendData(mtpk->buffer, mtpk->size);
				if (mtpk->size > sndsize)
				{
					logstr.Format("영상맞춤명령 %dbyte중 %dbyte만 전송했습니다.\n", mtpk->size, sndsize);
				}
				else
				{
					logstr.Format("한건처리영상번호: %d *lTCSImageCnt : %d (영상맞춤 명령전송)\n", pimg->QNum, pimg->QNum);
				}
				AddLog(logstr);
			}
		}
		break;
	case WM_SVR_SOCK_CHECK: 
		FD_ZERO( & rfds);
		FD_SET(m_mtSvrSock.m_sock,  & rfds);
		selTimeout.tv_sec = 0;
		selTimeout.tv_usec = 0;
		sel_ret = select(0,  & rfds, NULL, NULL,  & selTimeout);
		if (sel_ret == 1) 
		{
			set_ret = FD_ISSET(m_mtSvrSock.m_sock,  & rfds);
			if (set_ret == 1) 
			{
				rcvlen = recv(m_mtSvrSock.m_sock, buftmp, 100, 0);
				if (rcvlen <= 0) 
				{
					if (++m_svrStsCnt > 5) 
					{
						m_mtSvrSock.Disconnect();
						AddLog("서버 소켓연결 종료 by select()\n");
						SetSVRGUIStatus(STATUS_DISCONNECT);
						return;
					}
				} 
				else
				{
					m_svrStsCnt = 0;
				}
			}
		}

		SetTimer(WM_SVR_SOCK_CHECK, 5000, NULL);
		break;
	case WM_TIMER_INIT_LCL:
		mtpk = m_tcsLink.mkFrame(MT_CMD_INITACK);
		try 
		{
			sndsize = m_SvrSock->m_pAgentSock->SendData(mtpk->buffer, mtpk->size);

			if (sndsize < mtpk->size)
			{
				logstr.Format("LCL초기화 데이터 %dbyte중 %dbyte만 전송되었습니다.\n", mtpk->size, sndsize);
			}
			else
			{
				logstr = "LCL 초기화데이터 전송완료 - " + m_SvrSock->m_pAgentSock->m_youAddress + "\n";
			}
			AddLog(logstr);
		} 
		catch (...) 
		{
			AddLog("LCL 초기화데이터 전송실패 - " + m_SvrSock->m_pAgentSock->m_youAddress + "\n");
		}

		SetTimer(WM_TIMER_STS, MT_STATUS_INTERVAL, NULL); //2012.1.14 build.51 break다음에 있었던 것을 위치 이동
		break;
	case WM_TIMER_SEND_STATUS:
		SendStatus();
		break;
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CJWIS_TCSDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//if(m_hWDT != INVALID_HANDLE_VALUE)
	//	dis_wdt();

	if(m_SvrSock->m_pAgentSock->m_bConnected)
		m_SvrSock->m_pAgentSock->Close();

	if(m_mtSvrSock.m_connected)
		m_mtSvrSock.Disconnect();

	g_bSearchThread = false;
	g_bImgThread	= false;
	g_bFtpThread	= false;
	g_bSearchThread = false;
	SetEvent(m_ftpEvent);

	g_bDBThread = false;

	//WriteINI();
	CDialog::OnClose();

	timeEndPeriod(1);
	CoUninitialize();

	CDialogEx::OnClose();
}

/**
* @@cjw - result 폴더에 새로운 이미지가 생기면 처리
*
*@param		wParam		사용안함
*@param		lParam		사용안함
*/
afx_msg LRESULT CJWIS_TCSDlg::OnNewImage(WPARAM wParam, LPARAM lParam)
{
	MtPk* mtpk = NULL;
	MtImg* Img;
	int		brk = 0;
	CString str;
	CString FileName;
	bool	bload = FALSE;		// 이미지 파일 로드 성공 여부
	int		ImgNum;
	unsigned char* buf = NULL;
	int		ListType = 0;			// 검색된 BL, RL, PL의 종류
	time_t	tmpTime;
	struct tm nt;
	DWORD	TickCnt;
	int x;
	int i;
	double	meanValue = 100;
	char	MtSvrBuf[10] = {	'\0'};
	int		sndsize;

	CString srcIntFile; 
	CString	tgtIntFile;
	CString tgDir;
	tgDir = "D:\\SvrImgTot";	// SMJ

	str.Format("\n[WM_NEW_IMAGE 메세지 수신]\n"); //2013.4.25 
	AddLog(str);

	if (m_mtSvrSock.m_connected) 
	{
		//카메라(그래버) 정상으로
		if (m_tcsLink.m_tcsSockCnt >= 10)
		{
			MtSvrBuf[0] = 'F';
			try 
			{
				i = send(m_mtSvrSock.m_sock, MtSvrBuf, 1, 0);
			}
			catch (...) 
			{
				i = SOCKET_ERROR;
			}
			AddLog("카메라정상보고\n");
			if (i > 0)
			{
				m_tcsLink.m_tcsSockCnt = 0; //카메라 통신오류 해제
			}
		} 
		else
		{
			m_tcsLink.m_tcsSockCnt = 0;
		}
	}
	else
	{
		m_tcsLink.m_tcsSockCnt = 11;
	}

	ResetEvent(m_jpgEvent); //2013.5.29 

	TickCnt = GetTickCount();
	time( & tmpTime);
	localtime_s( & nt,  & tmpTime);

	AddLog("===========================================================================\n", LOG_STRING);
	FileName.Format("%s\\%s", m_carImgPath, m_strCimg);
	str.Format("인식시작 - %s\n", FileName); //2013.4.25 SIJ, 파일이름 첨가
	AddLog(str);
	Img = m_tcsLink.NewImgQ();
	memset(&Img->ImgInfo, 0, SIZE_IMGINFO);

	//차량 번호판 영상(트리거 진입)
	if (m_extWMV == FALSE) 
	{
		try 
		{
			bload = m_cxCimg.Load(FileName, CXIMAGE_FORMAT_JPG);
		}
		catch (...) 
		{
			bload = FALSE;
		}

		//파일 로드 성공
		if (bload) 
		{
			//SIJING - 후에 옵션으로 지정할 수 있도록 (표시부이미지향상?)
			//2011.11.24 - 영상이 하얗게 변하는 현상의 주범?
			meanValue = m_cxCimg.Mean();

			memset(Img->ImgInfo.srcFileName, 0, sizeof(Img->ImgInfo.srcFileName));
			sprintf_s(Img->ImgInfo.srcFileName, sizeof(Img->ImgInfo.srcFileName), "%s\0", m_strCimg);

			//파일 이름으로부터 인식정보 및 B/L정보 입력
			m_tcsLink.ReadInfoFromFileName( & Img->ImgInfo);
		} 
		// 파일 로드 실패
		else 
		{
			str.Format("JPG파일 로드실패\n[WM_NEW_IMAGE] 처리 종료\n");
			AddLog(str);
			m_tcsLink.FreeImgQ(Img);

			try 
			{
				DeleteFile(FileName);
			} 
			catch (...) 
			{
				str.Format("읽기실패된 '%s'파일 삭제 실패\n");
				AddLog(str);
			}

			SetEvent(m_jpgEvent);
			return 0L;
		}
		//번호판 파일명 30자 이상이면 정상처리
		if (m_strCimg.GetLength() >= 30) 
		{
			AddLog("\n", LOG_STRING);
			str.Format("%s\n", Img->ImgInfo.strPlate);
			AddLog(str);
			if (Img->ImgInfo.bRecog == RECOG_OK) 
			{
				m_bTowCar = FALSE;

				//2013.8.23 북남원에서의 현상으로 200-->400으로 증가시킴
				while ((GetTickInterval(TickCnt) < 400) && (m_BLtype == -1)) 
				{
					if ((m_BLtype >= 0) && (m_BLtype <= 9))
					{
						break;
					}
					else
					{
						Sleep(1);
					}
				}

				if (m_BLtype != -1) 
				{
					Img->ImgInfo.BLtype = m_BLtype;
					m_BLSearchTime = m_BLSearchTime / 2;
				} 
				else 
				{
					Img->ImgInfo.BLtype = NOT_BL;
					m_BLSearchTime = 101;
				}

				if ((Img->ImgInfo.BLtype == NOT_BL) || (Img->ImgInfo.BLtype == PL) || (Img->ImgInfo.BLtype == BL_PL)) 
				{
					str.Format("[B/L] Database 없음 :%s (형태:%d)  Query 속도: %d[ms]\n", Img->ImgInfo.strPlate, Img->ImgInfo.BLtype, m_BLSearchTime);
					AddLog(str);
				}
				else 
				{
					str.Format("[B/L] Database 발견 :%s (형태:%d)  Query 속도: %d[ms]\n", Img->ImgInfo.strPlate, Img->ImgInfo.BLtype, m_BLSearchTime);
					AddLog(str);
				}

				if ((Img->ImgInfo.BLtype == PL) || (Img->ImgInfo.BLtype == BL_PL)) 
				{
					str.Format("[P/L] Database 발견 :%s (형태:%d)  Query 속도: %d[ms]\n", Img->ImgInfo.strPlate, Img->ImgInfo.BLtype, m_BLSearchTime);
					AddLog(str);
				}
				else 
				{
					str.Format("[P/L] Database 없음 :%s (형태:%d)  Query 속도: %d[ms]\n", Img->ImgInfo.strPlate, Img->ImgInfo.BLtype, m_BLSearchTime);
					AddLog(str);
				}

				// B/L, R/L, P/L에 따른 처리
				// 0=Nothing, 1=BL, 2=PL, 3=BL/PL, 4=RL, 5=BL/RL, 6=화물할인
				// 7=1~3종 심야할인
				// 8=환불 + 1~3종 심야할인
				// 9=bl + 1~3종 심야할인
				x = m_cxCimg.GetWidth() - 16;
				memset(Img->ImgInfo.strBL, 0, sizeof(Img->ImgInfo.strBL));

				switch (Img->ImgInfo.BLtype) 
				{
				case BL: 
					// 환불차량이라는 문구가 더 잘 보이도록 위치 수정
					// 2012.11.20 SIJ - 적용되기 전까지는 B/L대신 환불문구
					m_cxCimg.DrawStringEx(NULL, x, 280,  & m_BLtext);	// "B/L차량"
					//sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s건 %s원", m_BLData[0], m_BLData[1]);
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s원 %s건", m_BLData[1], m_BLData[0]);

					// 파일 저장
					sprintf_s(Img->ImgInfo.BLFileName, sizeof(Img->ImgInfo.BLFileName),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.jpg",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[1], m_BLData[0]);
					m_cxCimg.Save(m_svrImgPath + "\\" + Img->ImgInfo.BLFileName, CXIMAGE_FORMAT_JPG);
					
					//str.Format("파일저장 (1).\n");	// TEST SMJ
					//AddLog(str);

					// 19.10.28 YJW 추가
					sprintf_s(Img->ImgInfo.BLFileName2, sizeof(Img->ImgInfo.BLFileName2),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.JPG",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[1], m_BLData[0]);
					m_cxCimg.Save(m_svrImgPathCopy + "\\" + Img->ImgInfo.BLFileName2, CXIMAGE_FORMAT_JPG);
					/////////////////////////////////////////////////////////////////////////////////
					srcIntFile	= m_svrImgPathCopy+"\\"+Img->ImgInfo.BLFileName2;
					tgtIntFile	= tgDir + "\\"+Img->ImgInfo.BLFileName2;
					//복사
					::CopyFile(srcIntFile, tgtIntFile, FALSE);
					str.Format("FILE BL - srcIntFile : %s,  tgtIntFile : %s\n", srcIntFile, tgtIntFile);	// TEST SMJ
					AddLog(str);
					Sleep(50);
					// 삭제
					::DeleteFile(srcIntFile);
					//m_BackImgName.Format("B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.wmv",
					//	nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
					//	nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
					//	m_NumOffice, m_NumLane, m_BLData[1], m_BLData[0]);
					//m_TakeBackImg = TRUE; //후면촬영 개시(BMT용)
					break;
				case PL:		// ?
					m_cxCimg.DrawStringEx(NULL, x, 110,  & m_PLtext);	// "경차주의"
					// 2012.2.14 BL문구입력난에 차량번호 입력
					// 20160607 도공 문서 기준으로 변경
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s", "경차");
					break;
				case BL_PL:		// ?
					m_cxCimg.DrawStringEx(NULL, x, 280,  & m_BLtext);	// "B/L차량"
					//sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s건 %s원", m_BLData[0], m_BLData[1]);
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s원%s건,경차", m_BLData[1], m_BLData[0]);

					// 파일 저장
					sprintf_s(Img->ImgInfo.BLFileName, sizeof(Img->ImgInfo.BLFileName),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.jpg",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[0], m_BLData[1]);
					m_cxCimg.Save(m_svrImgPath + "\\" + Img->ImgInfo.BLFileName, CXIMAGE_FORMAT_JPG);

					//str.Format("파일저장 (2).\n");	// TEST SMJ
					//AddLog(str);

					// 19.10.28 YJW 추가
					sprintf_s(Img->ImgInfo.BLFileName2, sizeof(Img->ImgInfo.BLFileName2),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.JPG",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[0], m_BLData[1]);
					m_cxCimg.Save(m_svrImgPathCopy + "\\" + Img->ImgInfo.BLFileName2, CXIMAGE_FORMAT_JPG);
					/////////////////////////////////////////////////////////////////////////////////
					//---------- SMJ 추가 ----------------
					srcIntFile	= m_svrImgPathCopy+"\\"+Img->ImgInfo.BLFileName2;
					tgtIntFile	= tgDir + "\\"+Img->ImgInfo.BLFileName2;
					//복사
					::CopyFile(srcIntFile, tgtIntFile, FALSE);
					str.Format("FILE BL_PL - srcIntFile : %s,  tgtIntFile : %s\n", srcIntFile, tgtIntFile);	// TEST SMJ
					AddLog(str);
					Sleep(50);
					// 삭제
					::DeleteFile(srcIntFile);

					m_cxCimg.DrawStringEx(NULL, x, 110,  & m_PLtext);	// "경차주의"
					//m_TakeBackImg = TRUE; //후면촬영 개시(BMT용)
					break;
				case RL: 
					m_cxCimg.DrawStringEx(NULL, 10, 110,  & m_RLtext);	// "제한차량"
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s", "제한B/L");
					sprintf_s(Img->ImgInfo.BLFileName, sizeof(Img->ImgInfo.BLFileName),
						"R%04d%02d%02d%02d%02d%02d_%s_%03d_%02d.jpg",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane);
					m_cxCimg.Save(m_svrImgPath + "\\" + Img->ImgInfo.BLFileName, CXIMAGE_FORMAT_JPG);

					str.Format("파일저장 (3).\n");	// TEST SMJ
					AddLog(str);

					// 19.10.28 YJW 추가
					sprintf_s(Img->ImgInfo.BLFileName2, sizeof(Img->ImgInfo.BLFileName2),
						"R%04d%02d%02d%02d%02d%02d_%s_%03d_%02d.JPG",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane);
					m_cxCimg.Save(m_svrImgPathCopy + "\\" + Img->ImgInfo.BLFileName2, CXIMAGE_FORMAT_JPG);
					/////////////////////////////////////////////////////////////////////////////////
					srcIntFile	= m_svrImgPathCopy+"\\"+Img->ImgInfo.BLFileName2;
					tgtIntFile	= tgDir + "\\"+Img->ImgInfo.BLFileName2;
					//복사
					::CopyFile(srcIntFile, tgtIntFile, FALSE);
					str.Format("FILE RL - srcIntFile : %s,  tgtIntFile : %s\n", srcIntFile, tgtIntFile);	// TEST SMJ
					AddLog(str);
					Sleep(50);
					// 삭제
					::DeleteFile(srcIntFile);
					break;
				case BL_RL: 
					m_cxCimg.DrawStringEx(NULL, x, 110,  & m_RLtext);	// "제한차량"
					m_cxCimg.DrawStringEx(NULL, x, 280,  & m_BLtext);	// "B/L차량"

					//sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s건 %s원", m_BLData[0], m_BLData[1]);
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s원%s건,제한B/L", m_BLData[1], m_BLData[0]);

					// 파일 저장
					sprintf_s(Img->ImgInfo.BLFileName, sizeof(Img->ImgInfo.BLFileName),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.jpg",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[0], m_BLData[1]);
					m_cxCimg.Save(m_svrImgPath + "\\" + Img->ImgInfo.BLFileName, CXIMAGE_FORMAT_JPG);

					//str.Format("파일저장 (4).\n");	// TEST SMJ
					//AddLog(str);

					// 19.10.28 YJW 추가
					printf_s(Img->ImgInfo.BLFileName2, sizeof(Img->ImgInfo.BLFileName2),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.JPG",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[0], m_BLData[1]);
					m_cxCimg.Save(m_svrImgPathCopy + "\\" + Img->ImgInfo.BLFileName2, CXIMAGE_FORMAT_JPG);
					/////////////////////////////////////////////////////////////////////////////////
					sprintf_s(Img->ImgInfo.BLFileName, sizeof(Img->ImgInfo.BLFileName),
						"R%04d%02d%02d%02d%02d%02d_%s_%03d_%02d.jpg",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane);
					m_cxCimg.Save(m_svrImgPath + "\\" + Img->ImgInfo.BLFileName, CXIMAGE_FORMAT_JPG);

					srcIntFile	= m_svrImgPathCopy+"\\"+Img->ImgInfo.BLFileName2;
					tgtIntFile	= tgDir + "\\"+Img->ImgInfo.BLFileName2;
					//복사
					::CopyFile(srcIntFile, tgtIntFile, FALSE);
					str.Format("FILE BL_RL - srcIntFile : %s,  tgtIntFile : %s\n", srcIntFile, tgtIntFile);	// TEST SMJ
					AddLog(str);
					Sleep(50);

					str.Format("파일저장 (5).\n");	// TEST SMJ
					AddLog(str);

					// 19.10.28 YJW 추가
					sprintf_s(Img->ImgInfo.BLFileName2, sizeof(Img->ImgInfo.BLFileName2),
						"R%04d%02d%02d%02d%02d%02d_%s_%03d_%02d.JPG",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane);
					m_cxCimg.Save(m_svrImgPathCopy + "\\" + Img->ImgInfo.BLFileName2, CXIMAGE_FORMAT_JPG);
					/////////////////////////////////////////////////////////////////////////////////
					//---------- SMJ 추가 ----------------
					srcIntFile	= m_svrImgPathCopy+"\\"+Img->ImgInfo.BLFileName2;
					tgtIntFile	= tgDir + "\\"+Img->ImgInfo.BLFileName2;
					//복사
					::CopyFile(srcIntFile, tgtIntFile, FALSE);
					str.Format("FILE RL - srcIntFile : %s,  tgtIntFile : %s\n", srcIntFile, tgtIntFile);	// TEST SMJ
					AddLog(str);
					Sleep(50);
					// 삭제
					::DeleteFile(srcIntFile);
					break;
				case REFUND:
					m_cxCimg.DrawStringEx(NULL, x, 280,  & m_RFtext);	// "환불차량"

					//sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s건 %s원", m_BLData[0], m_BLData[1]);
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "환불,%s원", m_BLData[1]);
					break;
				/////////////////////////////////////////////////////////////////////////////////
				// @@cjw - 1~3종 심야할인 기능 추가 (20160515)
				case 7:		// 1~3종 심야할인
					m_cxCimg.DrawStringEx(NULL, x, 110,  &m_NightDCtext);		// "심야"
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s", "심야");
					break;
				case 8:		// 8=환불 + 1~3종 심야할인
					m_cxCimg.DrawStringEx(NULL, x, 110,  &m_NightDCtext);	// "심야"
					m_cxCimg.DrawStringEx(NULL, x, 280,  &m_RFtext);		// "환불차량"
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "환불,%s원,심야", m_BLData[1]);
					break;
				case 9:		// 9=bl + 1~3종 심야할인
					m_cxCimg.DrawStringEx(NULL, x, 280,  & m_BLtext);	// "B/L차량"

					//sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s건 %s원", m_BLData[0], m_BLData[1]);
					sprintf_s(Img->ImgInfo.strBL, sizeof(Img->ImgInfo.strBL), "%s원%s건,심야", m_BLData[1], m_BLData[0]);

					// 파일저장
					sprintf_s(Img->ImgInfo.BLFileName, sizeof(Img->ImgInfo.BLFileName),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.jpg",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[1], m_BLData[0]);
					m_cxCimg.Save(m_svrImgPath + "\\" + Img->ImgInfo.BLFileName, CXIMAGE_FORMAT_JPG);

					//str.Format("파일저장 (6).\n");	// TEST SMJ
					//AddLog(str);

					// 19.10.28 YJW 추가
					printf_s(Img->ImgInfo.BLFileName2, sizeof(Img->ImgInfo.BLFileName2),
						"B%04d%02d%02d%02d%02d%02d_%s_%03d_%02d_%s_%s.JPG",
						nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
						nt.tm_hour, nt.tm_min, nt.tm_sec, Img->ImgInfo.strPlate,
						m_NumOffice, m_NumLane, m_BLData[1], m_BLData[0]);
					m_cxCimg.Save(m_svrImgPathCopy + "\\" + Img->ImgInfo.BLFileName2, CXIMAGE_FORMAT_JPG);
					/////////////////////////////////////////////////////////////////////////////////
					srcIntFile	= m_svrImgPathCopy+"\\"+Img->ImgInfo.BLFileName2;
					tgtIntFile	= tgDir + "\\"+Img->ImgInfo.BLFileName2;
					//복사
					::CopyFile(srcIntFile, tgtIntFile, FALSE);
					str.Format("FILE 9(1~3종 심야할인) - srcIntFile : %s,  tgtIntFile : %s\n", srcIntFile, tgtIntFile);	// TEST SMJ
					AddLog(str);
					Sleep(50);
					// 삭제
					::DeleteFile(srcIntFile);

					m_cxCimg.DrawStringEx(NULL, x, 110,  &m_NightDCtext);		// "심야"
					break;
				/////////////////////////////////////////////////////////////////////////////////
				default:
					Img->ImgInfo.BLtype = NOT_BL;
					break;
				}

				brk = m_strCimg.Find(".jpg");
				if (brk < MIN_EXT_POS)
				{
					brk = m_strCimg.Find(".JPG");
				}
				if (brk >= MIN_EXT_POS)
				{
					m_strPimg = m_strCimg.Left(brk) + "_p.jpg";
				}
				if (m_cxPimg.Load(m_pltImgPath + "\\" + m_strPimg, CXIMAGE_FORMAT_JPG)) 
				{
					if (meanValue < 50)
					{
						m_cxPimg.Light(20, 0);
					}
					m_cxPimg.Resample(m_pImgW, m_pImgH);
					if (m_cxPimg.IsGrayScale() == false)
					{
						m_cxPimg.GrayScale();
					}

					Img->ImgInfo.pImgW = m_cxPimg.GetWidth();
					Img->ImgInfo.pImgH = m_cxPimg.GetHeight();

					memset(Img->ImgInfo.pltFileName, 0, sizeof(Img->ImgInfo.pltFileName));
					sprintf_s(Img->ImgInfo.pltFileName, sizeof(Img->ImgInfo.pltFileName), "%s\0", m_strPimg);

					buf = NULL;
					m_cxPimg.Encode(buf, Img->ImgInfo.pImgSize, CXIMAGE_FORMAT_JPG);
					memcpy(Img->pBuf, buf, Img->ImgInfo.pImgSize);
					m_cxPimg.FreeMemory(buf);

					str.Format("Resize Suceess(N) :%d \n", Img->ImgInfo.pImgSize);
					AddLog(str);
				} 
				else 
				{
					str.Format("번호판 영상파일 %s 로드 실패\n", m_strPimg);
					AddLog(str);
				}
			}
			m_BLtype = -1; //2012.4.1 SIJ
		}
		//번호판 파일명 30자 미만이면 처리 안함
		else 
		{
			m_BLtype = -1;
			str.Format("%s파일의 파일명이 30자 미만임\n[WM_NEW_IMAGE] 처리 종료\n", m_strCimg);
			AddLog(str);
			try 
			{
				DeleteFile(FileName);
			} 
			catch (...) 
			{
				str.Format("읽기실패된 '%s'파일 삭제 실패\n");
				AddLog(str);
			}
			SetEvent(m_jpgEvent);
			return 0L;
		}
	}
	// 동영상 파일이면 초기화
	else 
	{
		memset(Img->ImgInfo.srcFileName, 0, sizeof(Img->ImgInfo.srcFileName));
		sprintf_s(Img->ImgInfo.srcFileName, sizeof(Img->ImgInfo.srcFileName), "%s\0", m_strCimg);

		//파일 이름으로부터 인식정보 및 B/L정보 입력
		m_tcsLink.ReadInfoFromFileName( & Img->ImgInfo);
	}

	//2013.9.1 SIJ 후면사진(특정한 케이스의 진출영상)이나 동영상이면 처리
	// @@cjw
	if (m_bTowCar == TRUE || m_extWMV == TRUE) 
	{
		m_extWMV = FALSE;
		m_bTowCar = FALSE;
		MoveFile(m_tmpImgPath + "\\" + m_TowCarName + "_a.jpg", m_svrImgPath + "\\" + m_TowCarName + "_a.jpg");
		// 19.10.28 JYW 추가
		MoveFile(m_tmpImgPath + "\\" + m_TowCarName + "_a.jpg", m_svrImgPathCopy + "\\" + m_TowCarName + "_a.jpg");
		// 동영상 파일이면 백업경로에 이동
		if (m_extWMV == TRUE)
		{
			MoveFile(m_carImgPath + "\\" + Img->ImgInfo.srcFileName, m_svrImgPath + "\\" + m_BackImgName + ".wmv");
			//CString strlog;
			//strlog.Format("파일저장 (7).\n");	// TEST SMJ
			//AddLog(strlog);
			// 19.10.28 YJW 추가
			MoveFile(m_carImgPath + "\\" + Img->ImgInfo.srcFileName, m_svrImgPathCopy + "\\" + m_BackImgName + ".wmv");
		}
		else
		{
			//MoveFile(m_carImgPath + "\\" + Img->ImgInfo.srcFileName, m_svrImgPath + "\\" + m_TowCarName + ".wmv");
			MoveFile(m_carImgPath + "\\" + Img->ImgInfo.srcFileName, m_svrImgPath + "\\" + m_TowCarName + "_z.jpg");
			//CString strlog;
			//strlog.Format("파일저장 (8).\n");	// TEST SMJ
			//AddLog(strlog);
			// 19.10.28 YJW 추가
			MoveFile(m_carImgPath + "\\" + Img->ImgInfo.srcFileName, m_svrImgPathCopy + "\\" + m_BackImgName + ".wmv");
			//MoveFile(m_carImgPath + "\\" + Img->ImgInfo.srcFileName, m_svrImgPath + "\\" + m_TowCarName + "_z.jpg");
		}
		m_tcsLink.FreeImgQ(Img);
		m_cxCimg.FreeMemory(buf);
	}
	//진입 영상만 처리
	else 
	{
		Img->ImgInfo.ImgW = m_cxCimg.GetWidth();
		Img->ImgInfo.ImgH = m_cxCimg.GetHeight();

		//차량영상을 전송할 영상으로 크기 변환 - 2011.08.18
		m_cxCimg.Resample(m_cImgW, m_cImgH);

		buf = NULL;
		m_cxCimg.Encode(buf, Img->ImgInfo.ImgSize, CXIMAGE_FORMAT_JPG);
		memcpy(Img->cBuf, buf, Img->ImgInfo.ImgSize);

		str.Format("Resize Suceess(M) :%d \n", Img->ImgInfo.ImgSize);
		AddLog(str);

		// @@cjw - tcs로 영상 전송
		mtpk = m_tcsLink.mkImgFrame(Img);
		if (mtpk != NULL) 
		{
			sndsize = m_SvrSock->m_pAgentSock->SendData(mtpk->buffer, mtpk->size);

			if (sndsize >= mtpk->size) 
			{
				m_AckMode = MT_CMD_SYNC;
				memcpy( & ImgNum,  & mtpk->buffer[5], 4);
				str.Format("[면탈->LCL]전송 Size : %d 이미지번호 : %d, BL(PL)코드 : %d\n", mtpk->size, ImgNum, Img->ImgInfo.BLtype);
				AddLog(str);
			}
		}

		m_cxCimg.FreeMemory(buf);

		//영상의 전체 밝기 점검
		str.Format("Gray Value : %d \n", (int)meanValue);
		AddLog(str);

		if (meanValue < 10.0) 
		{
			//조명이상으로 보고
			if (++m_LightErrorCnt == 20)
			{
				m_LightErrorCnt = 21;
				MtSvrBuf[0] = 'E';
				if (m_mtSvrSock.m_connected)
				{
					send(m_mtSvrSock.m_sock, MtSvrBuf, 1, 0);
				}
				AddLog("조명이상보고\n");
			}
		} 
		//정상으로 돌아온 것 같음
		else
		{
			//조명이상으로 보고
			if (m_LightErrorCnt >= 20) 
			{
				MtSvrBuf[0] = 'W';
				i = SOCKET_ERROR;
				if (m_mtSvrSock.m_connected)
				{
					i = send(m_mtSvrSock.m_sock, MtSvrBuf, 1, 0);
				}
				
				if (i > 0) 
				{
					AddLog("조명정상보고\n");
					m_LightErrorCnt = 0;
				} 
				else
				{
					m_LightErrorCnt = 21;
				}
			} 
			else
			{
				m_LightErrorCnt = 0;
			}
		}

		Sleep(0);

		//차량영상파일을 TEMP(16개 버퍼) 디렉토리로 복사를 성공하면 영상처리기 차량 파일 삭제
		if (CopyFile(m_carImgPath + "\\" + m_strCimg, m_tmpImgPath + "\\" + m_strCimg, FALSE) == TRUE)
		{
			CopyFile(m_carImgPath + "\\" + m_strCimg, m_passImgPath + "\\" + m_strCimg, FALSE);				//면탈촬영통과대수		
			DeleteFile(m_carImgPath + "\\" + m_strCimg);
		}

		//번호판영상파일은 FTP전송용 파일이 만들어지고 난 다음에...
		if (++m_tcsLink.m_tcsSockErrCnt == 10) 
		{
			m_tcsLink.m_tcsSockErrCnt = 11;
			AddLog("[ProcImg] TCS통신이상 발생\n");

			InitLCLPort();

			MtSvrBuf[0] = 'B';
			if (m_mtSvrSock.m_connected)
			{
				send(m_mtSvrSock.m_sock, MtSvrBuf, 1, 0);
			}

			if (m_SvrSock->m_pAgentSock->m_bConnected) 
			{
				if (m_SvrSock->m_pAgentSock->m_hSocket != INVALID_SOCKET)
				{
					m_SvrSock->m_pAgentSock->Close();
				}
				AddLog("LCL 소켓통신 연결을 끊었습니다.\n");
			}
			SetTCSCGUIStatus(STATUS_DISCONNECT);
			m_SvrSock->m_pAgentSock->m_bConnected = FALSE;
		}

		//폐쇄차로 데이터가 있는지 확인
		if (m_tcsLink.m_tkClosed == TRUE) 
		{
			AddLog("LCL의 폐쇄차로 차검데이터가 있습니다. 폐쇄차로 영상처리중...\n");
			m_tcsLink.m_tkClosed = FALSE;
			m_tcsLink.ProcImg(Img,  & m_tcsLink.m_tkSnd);
		}

		// 2013.9.1 SIJ, 견인차량이면 후면촬영 명령을 전송
		// 2017.01.20 요청으로...폐쇄차선에서는 견인차량 영상 전송 안함
		if ((Img->ImgInfo.bTow == 1) && (m_tcsLink.m_tkClosed == FALSE)) 
		{	// || (m_TakeBackImg == TRUE)) {
			if (Img->ImgInfo.bTow == 1)
			{
				AddLog("[I/O보드] 특수차량번호판 감지됨\n");
			}
			else 
			{
				AddLog("[I/O보드] 테스트용 후면촬영 개시\n");
				m_TakeBackImg = FALSE;
			}
			SendToTowTrigger();
			m_bTowCar = TRUE;
			m_TowCarName.Format("t%04d%02d%02d%02d%02d%02d_01_%03d_%s",
				nt.tm_year + 1900, nt.tm_mon + 1, nt.tm_mday,
				nt.tm_hour, nt.tm_min, nt.tm_sec, m_NumLane, Img->ImgInfo.strPlate);
			CopyFile(m_tmpImgPath + "\\" + Img->ImgInfo.srcFileName, m_tmpImgPath + "\\" + m_TowCarName + "_a.jpg", TRUE);
		}

	}

	AddLog("[WM_NEW_IMAGE] 처리 종료\n");

	SetEvent(m_jpgEvent);

	return 0;
}

/**
* @@cjw - 면탈 이벤트 메세지를 처리한다. 
*
*@param		wParam		
*@param		lParam		
*/
afx_msg LRESULT CJWIS_TCSDlg::OnMtMsg(WPARAM wParam, LPARAM lParam)
{
	int		i;
	CString srcstr, dststr;
	char	strdate[7], strDir[256];
	char*	buf;
	char	MtSvrBuf[10];
	int		bSend;				// 주에어기 전송 플래그
	SYSTEMTIME st;
	MtImg*	mtimg;

	switch(wParam)
	{
	case MT_WPARAM_FTPSEND :
		if( (lParam > 15) || (lParam < 0) )
		{
			return 0;
		}
		
		mtimg = &g_ImgQ.Img[lParam];

		//전에 TCS처리데이터가 영상없이 처리된 플래그를 다시 세팅
		if(m_tcsLink.m_FlagEmptyImg)
		{	
			m_tcsLink.m_FlagEmptyImg = FALSE;
		}

		bSend = TRUE;
	
		if( (m_gateType == OPEN_UP_GATE) || (m_gateType == OPEN_DN_GATE) )
		{
			//2013.3.21 SIJ - m_AllTkData의 조건 추가
			if( (m_tcsLink.m_tkSnd.cmd == TICKET_CONFIRM_DATA) && (m_AllTkData == 0) )
			{
				bSend = FALSE;
			}
		}
		
		if(bSend == TRUE)
		{
			SetFtpBuffer(mtimg, &m_tcsLink.m_tkSnd); //결과는 파일로 저장됨
		}

		m_tcsLink.FreeTkSndData();

		//영상맞춤 command 전송
		SetTimer(WM_TIMER_SYNCIMG, 50, NULL);

	case MT_WPARAM_STSRES :
		// 1. 시간을 체크
		GetLocalTime(&st);
		// 2. 매일 12시 넘어선 시간에 파일 삭제 실행
		if( (st.wHour == 2) && (st.wMinute <= 59) )
		{
			if(m_BackupFileErasing == FALSE)
			{
				m_BackupFileErasing = TRUE;
				AfxBeginThread(EraseBackupFileThread, this, THREAD_PRIORITY_LOWEST);
				AddLog("백업파일 삭제 Thread 기동\n");
			}
		}

		break;
	
	// @@cjw - 큐 영상 삭제
	case MT_WPARAM_DELFILE :
		srcstr = m_tcsLink.m_delFilename;

		//차량영상파일 백업
		if(lParam == FALSE)
		{
			buf = srcstr.GetBuffer(256);
			memcpy(strdate, &buf[7], 6);
			srcstr.ReleaseBuffer();
			strdate[6] = '\0';

			// TcsBak/날짜 폴더 생성
			memset(strDir, 0, sizeof(strDir));
			sprintf_s(strDir, 256, "%s\\%s", m_bakImgPath, strdate);
			CreateDir(strDir);
			memset(strDir, 0, sizeof(strDir));

			// TmpImg/~.jpg 를 TcsBak/~.jpg 로 백업
			sprintf_s(strDir, 256, "%s\\%s\\%s", m_bakImgPath, strdate, srcstr);
			CopyFile(m_tmpImgPath+"\\"+srcstr, strDir, FALSE);

			// cjw - TCS 미처리 데이터
			// 혼용차로 일때는 서버로 이미지를 전송하지 않음 (20160516)
			// 환경설정에서 혼용차로가 체크되어 있어야함
			if(m_MixedHipass != 1)
			{
				SYSTEMTIME st;
				GetLocalTime(&st);
				int imgId = g_ImgQ.pbgn;
				char *imgDateTime = g_ImgQ.Img[imgId].imgDateTime;

				CString destFileName;
				WORD workNum = m_tcsLink.GetTcsWorkNum();
				destFileName.Format("%s\\c%03d%04d%s.JPG", m_svrImgPath, m_NumOffice, workNum, imgDateTime);
				
				CString strlog;
				strlog.Format("파일저장 (9).\n");	// TEST SMJ
				AddLog(strlog);
				// 19.10.28 YJW 추가
				destFileName.Format("%s\\c%03d%04d%s.JPG", m_svrImgPathCopy, m_NumOffice, workNum, imgDateTime);
				CopyFile(strDir, destFileName, FALSE);

				CString logMsg;
				logMsg.Format("TCS 미처리 - %s\n", destFileName);
				AddLog(logMsg);
			}
		}

		DeleteFile(m_tmpImgPath + "\\" + srcstr);
		srcstr = m_tcsLink.m_delpltFilename;
		DeleteFile(m_pltImgPath + "\\" + srcstr);
		break;
		
	case MT_WPARAM_INIT :
		m_cImgW = m_tcsLink.m_ImgW;
		m_cImgH = m_tcsLink.m_ImgH;
		m_pImgW = m_tcsLink.m_plW;
		m_pImgH = m_tcsLink.m_plH;
		srcstr.Format("LCL전송영상이미지 %dx%d(%dx%d)로 조정됨\n", m_cImgW, m_cImgH, m_pImgW, m_pImgH);
		AddLog(srcstr);
		break;
		
	case MT_WPARAM_UNMATCHED_TK :
		srcstr.Format("[ProcImg] TCS데이터와 일치하는 영상이 없음 (%04X)\n", lParam);
		AddLog(srcstr);
		SendToRecogSW();	// 인식S/W에 전달하여 루프를 리셋
		break;
		
	case MT_WPARAM_EMPTY_TK :
		srcstr.Format("영상파일이 없거나 영상을 읽어들이는데 실패했습니다 (%04X)\n", lParam);
		AddLog(srcstr);
		if(m_tcsLink.m_wrecker == FALSE)
		{
			m_tcsLink.m_FlagEmptyImg = TRUE;
		}
		m_tcsLink.FreeTkSndData();
		break;
	case MT_WPARAM_FAIL_SELIMG :
		m_tcsLink.FreeTkSndData();
		if(lParam == TRUE){
			AddLog("[ProcImg] TCS처리에 해당되는 이미지 선택실패 (알수없는 오류) \n");
		}
		break;
		
	case MT_WPARAM_CAMERA_ERR :
		AddLog("[ProcImg] 카메라 오류 발생 \n");
		MtSvrBuf[0] = 'X';
		if(m_mtSvrSock.m_connected == TRUE){
			send(m_mtSvrSock.m_sock, MtSvrBuf, 1, 0);
		}
		break;
		
	case MT_WPARAM_TCSSOCK :
		MtSvrBuf[0] = 'O';
		i = SOCKET_ERROR;
		if(m_mtSvrSock.m_connected == TRUE){
			i = send(m_mtSvrSock.m_sock, MtSvrBuf, 1, 0);
		}
		if(i > 0){
			m_tcsLink.m_tcsSockErrCnt = 0;
			AddLog("TCS통신정상보고\n");
		}
		else{
			m_tcsLink.m_tcsSockErrCnt = 11;
		}
		break;
	}

	return 0;
}

/**
* 차선제어기로부터 소켓 이벤트가 발생시 처리한다. 
*
*@param		wParam		
*@param		lParam		
*/
afx_msg LRESULT CJWIS_TCSDlg::OnSockAccept(WPARAM wParam, LPARAM lParam)
{
	CString	str;
	ULONG	len;				// Command Data의 길이
	BYTE	code;				// 제어코드
	int		serial;				// 차량영상일련번호
	MtImg*	pimg = NULL;
	MtPk*	mtpk = NULL;
	char	tmpstr[256];
	int		sndsize;
	int		id;

	switch(wParam)
	{
	case WM_WPARAM_ACCEPT:
		AddLog("[TCS]LCL TCP소켓 연결됨 - " + m_SvrSock->m_pAgentSock->m_youAddress + "\n");
		SetTCSCGUIStatus(STATUS_CONNECT);

		m_cnt_sts = 0;
		m_AckMode = 0;

		ResetEvent(m_jpgEvent);

		try
		{
			id = g_ImgQ.pend;
			if( (g_ImgQ.Img[id].bUsed == TRUE) && (g_ImgQ.Img[id].bTrfd == FALSE) )
			{
				MoveFile(m_tmpImgPath + "\\" + g_ImgQ.Img[id].ImgInfo.srcFileName,
							m_carImgPath + "\\" + g_ImgQ.Img[id].ImgInfo.srcFileName);
			}

			memset(tmpstr, 0, sizeof(tmpstr));
			sprintf_s(tmpstr, sizeof(tmpstr), "%s", m_tmpImgPath);
			EmptyDirectory(tmpstr);

			//2011.11.28 SIJ
			memset(tmpstr, 0, sizeof(tmpstr));
			sprintf_s(tmpstr, sizeof(tmpstr), "%s", m_carImgPath);
			BackupFiles(m_carImgPath, m_bakImgPath, 20);	//2012.1.14 build.51 10-->20로 복귀

			m_tcsLink.InitImgQ();

			SetEvent(m_jpgEvent);
			AddLog("백업파일 처리 및 Image Buffer 초기화 완료\n");
		}
		catch(...)
		{
			AddLog("백업파일 처리 및 Image Buffer 초기화 과정에서 오류가 발생했습니다.\n");
		}
		break;
		
	case WM_WPARAM_DATRCV:
		KillTimer(WM_TIMER_STS);
		m_cnt_sts = 0;
		len    =  lParam & 0x000FFFFF;
		serial = (lParam & 0x00F00000) >> 20;
		code   = (lParam & 0xFF000000) >> 24;

		//2014.3.31 SIJ
		if( code != STATUS_REQUEST)
		{
			str.Format("[LCL->면탈] paket length = %d, code = %d, serial = %d \n", len, code, serial);
			AddLog(str);
		}

		//2013.5.30 SIJ 필터추가
		if( (code == 0xFF) && (serial == 0xFF) )
		{
			str.Format("[LCL->면탈] %d Byte 수신된 데이터에 오류가 있습니다.\n", len);
			AddLog(str);
			return 0;
		}

		//2012.1.14 build.51 추가
		if(len == 0x000FFFFF) 
		{
			str.Format("LCL로 데이터(opcode=0x%x) 전송을 완료하지 못했습니다\n", code);
			AddLog(str);
			
			//재전송 - 2013.5.30 SIJ
			Sleep(50);
			m_SvrSock->m_pAgentSock->reSend();
		}

		switch(code)
		{
		case ACK :
			switch(m_AckMode)
			{
			case MT_CMD_SYNC:
				pimg = m_tcsLink.selectImg(TRUE, m_MixedHipass);
				if(pimg != NULL){
					mtpk = m_tcsLink.mkFrame(MT_CMD_SYNC, pimg->QNum);
					if(mtpk != NULL)
					{
						sndsize = m_SvrSock->m_pAgentSock->SendData(mtpk->buffer, mtpk->size);
						if(sndsize < mtpk->size)
						{
							str.Format("영상맞춤명령 %dbyte중 %dbyte만 전송했습니다.\n", mtpk->size,sndsize);
						}
						else
						{
							str.Format("[한건처리영상번호]: %d, *lTCSImageCnt : %d (영상맞춤 명령전송)\n", pimg->QNum, pimg->QNum);
						}
						AddLog(str);
					}
					else
					{
						str.Format("한건처리영상번호: NULL, *lTCSImageCnt : NULL [오류번호:1]\n");
						AddLog(str);
					}
				}
				else
				{
					str.Format("한건처리영상번호: NULL, *lTCSImageCnt : NULL [오류번호:2]\n");
					AddLog(str);
				}
				break;
			}
			m_AckMode = 0;
			break;
			
		case NAK:
			str.Format("[TCS]NAK 수신됨 (패킷전체크기=%d바이트)\n", len);
			AddLog(str);
			m_AckMode = 0;
			//2013.5.10 SIJ
			m_SvrSock->m_pAgentSock->reSend();
			break;
			
		case WORK_START:
			AddLog("[TCS]근무개시 명령 수신\n");
			//정상차로 전송
			tmpstr[0] = 'K';
			send(m_mtSvrSock.m_sock, tmpstr, 1, 0);
			Sleep(50);
			//기기 상태 전송
			SetTimer(WM_TIMER_SEND_STATUS, 500, NULL);
			break;
			
		case WORK_FINISH:
			AddLog("[TCS]근무종료 명령 수신\n");
			//차로 상태 전송 - 아마도 폐쇄차로?
			tmpstr[0] = 'C';
			send(m_mtSvrSock.m_sock, tmpstr, 1, 0);
			break;
			
		case STATUS_REQUEST:
			break;
			
		case PREVIOUS_DATA:
			str.Format("[TCS]이전영상 명령 수신 - 응답 영상번호 : %d \n",serial);
			AddLog(str);
			break;
			
		case CAR_NUM_REQUEST:
			str.Format("[TCS]촬영대수 요청 명령 수신\n");
			AddLog(str);
			break;
			
		default:
			break;
		}
		SetTimer(WM_TIMER_STS, MT_STATUS_INTERVAL, NULL);
		break;
	case WM_WPARAM_SOCKCL:
		AddLog("[TCS]LCL 소켓통신 끊어짐\n");
		SetTCSCGUIStatus(STATUS_DISCONNECT);
		break;
	}

	return 0;
}


afx_msg LRESULT CJWIS_TCSDlg::OnMtSvrSock(WPARAM wParam, LPARAM lParam)
{
	CString	str, logstr;
	char	rcvbuf[256], sndbuf[256];
	int		sndsize;
	int		rcvlen;
	SYSTEMTIME st;
	int		bResult;
	char	strYear[5]={0}, strMon[3]={0}, strDay[3]={0}, strHour[3]={0}, strMin[3]={0}, strSec[3]={0};
	int i;

	switch(WSAGETSELECTEVENT(lParam))
	{
	case FD_CLOSE :
		if(m_mtSvrSock.m_connected){
			str.Format("면탈 서버통신 소켓 끊어짐\n");
			AddLog(str);
		}
		m_mtSvrSock.m_connected = FALSE;
		m_svrStsCnt = 0;
		SetSVRGUIStatus(STATUS_DISCONNECT);
		break;
		
	case FD_READ :
		rcvlen = recv(m_mtSvrSock.m_sock, rcvbuf, sizeof(rcvbuf), 0);
		switch(rcvbuf[0])
		{
		case '#' : 
			m_mtSvrSock.m_connected = TRUE;
			str.Format("I%03d1.1", m_NumCamera);
			m_mtSvrSock.sendString(str);
			logstr.Format("면탈서버 %s(%04d)에 접속됨(%s)\n", m_ftpAddr, m_SvrPort, str);
			m_svrStsCnt = 0;
			SetSVRGUIStatus(STATUS_CONNECT);
			AddLog(logstr);
				
			SetTimer(WM_SVR_SOCK_CHECK, 5000, NULL);
			break;
		case 'R' :	//시스템 재시작
			AddLog("시스템 리셋 명령 수신됨. 리셋 시도중...\n\n\n");
			RebootWindow();
			break;
		case 'V' : //차로 버전 요구
			memset(sndbuf, 0, sizeof(sndbuf));
			sprintf_s(sndbuf,sizeof(sndbuf), "V%s", MT_VERSION);
			sndsize = strlen(sndbuf);
			send(m_mtSvrSock.m_sock, sndbuf, sndsize, 0);
			break;
		case 'T' : //날짜 변경
			bResult = TRUE;
			str.Format("시간동기화 명령 수신 실패\n");
			if(rcvlen >= 15){
				try{
					i = 1;
					memcpy(strYear ,&rcvbuf[i], 4); i+=4;
					memcpy(strMon  ,&rcvbuf[i], 2); i+=2;
					memcpy(strDay  ,&rcvbuf[i], 2); i+=2;
					memcpy(strHour ,&rcvbuf[i], 2); i+=2;
					memcpy(strMin  ,&rcvbuf[i], 2); i+=2;
					memcpy(strSec  ,&rcvbuf[i], 2); i+=2;
					st.wYear   = atoi(strYear);
					st.wMonth  = atoi(strMon);
					st.wDay    = atoi(strDay);
					st.wHour   = atoi(strHour);
					st.wMinute = atoi(strMin);
					st.wSecond = atoi(strSec);
					st.wMilliseconds=0;
					bResult = TRUE;
				}
				catch(...){
					bResult = FALSE;
				}
				
				if(bResult == TRUE){	
					if(SetLocalTime(&st)){
						str.Format("시간동기화 명령 수신 성공 - %04d-%02d-%02d %02d:%02d:%02d\n",st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond);
					}
				}
				AddLog(str);
			}
			break;
		case 'G' : //날짜 요구
			GetLocalTime(&st);
			memset(sndbuf, 0, sizeof(sndbuf));
			sprintf_s(sndbuf, sizeof(sndbuf), "G%04d%02d%02d%02d%02d%02d",
					st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond);
			sndsize = strlen(sndbuf);
			send(m_mtSvrSock.m_sock, sndbuf, sndsize, 0);
			break;
		}
		break;
	}

	return 0;
}

/**
* FTP 접속을 받는다. 
*/
afx_msg LRESULT CJWIS_TCSDlg::OnFtpAccept(WPARAM wParam, LPARAM lParam)
{
	int		ievent;
	SOCKET	sock;
	CString str;
	int		client_socklen;
	SOCKADDR_IN	client_saddr;
	DWORD	ret_wait;
	int		ErrCode;

	ievent = WSAGETSELECTEVENT(lParam);
	sock = wParam;
	ULONG on;

	switch(ievent)
	{
	case FD_ACCEPT :
		if(m_ftp.m_TransmitSock != INVALID_SOCKET) 
		{
			return 0L;
		}
		ret_wait = WaitForSingleObject(m_ftp.m_hListenEvent, 0);
		if(ret_wait != WAIT_OBJECT_0) return 0L;
		//FTP서버의 데이터 전송 소켓 연결 대기
		client_socklen = sizeof(client_saddr);
		m_ftp.m_TransmitSock = accept(sock, (LPSOCKADDR)&client_saddr, &client_socklen);
		if(m_ftp.m_TransmitSock == INVALID_SOCKET)
		{
			ErrCode = WSAGetLastError();
			str.Format("FTP 오류 발생(Accept, %d)\n", ErrCode);
			AddLog(str);
		}

		int  lpOptionLen, lpOptionValue;
		lpOptionLen = sizeof(int*);
		
		lpOptionValue = 40960;
		setsockopt( m_ftp.m_TransmitSock, SOL_SOCKET, SO_RCVBUF, (char *)&lpOptionValue, sizeof(lpOptionValue) );

		on = 1;
		ioctlsocket(m_ftp.m_TransmitSock, FIONBIO, &on);

		WSAAsyncSelect(m_ftp.m_TransmitSock, m_hWnd, WM_FTP_DATASOCK, FD_READ | FD_CLOSE);
		m_total = 0;

		SetEvent(m_ftp.m_hAcceptEvent);
		break;
		
	case FD_CLOSE :
		closesocket(sock);
		break;
	}

	return 0;
}


afx_msg LRESULT CJWIS_TCSDlg::OnFTPDataSock(WPARAM wParam, LPARAM lParam)
{
	int		ievent;
	SOCKET	sock;
	CString str;
	int		nrcv;

	ievent = WSAGETSELECTEVENT(lParam);
	sock = wParam;
	
	switch(ievent)
	{
	case FD_READ :
		nrcv = recv(sock, m_rcvbuf, min(m_ftpfileSize - m_total, sizeof(m_rcvbuf)), 0);
		if(nrcv > 0)
		{
			if(m_ftpFile)
				fwrite(m_rcvbuf, 1, nrcv, m_ftpFile);
			m_total += nrcv;
		}
		else
		{
			if(m_ftpFile)
			{
				closesocket(sock);
				fclose(m_ftpFile);
				m_ftpFile = NULL;
				SetEvent(m_ftp.m_hRcvDoneEvent);
			}
		}
		str.Format("%d 수신 - 총 %d bytes\n", nrcv, m_total);
		AddLog(str);
		break;
		
	case FD_CLOSE :
		closesocket(sock);
		if(m_ftpFile)
		{
			fclose(m_ftpFile);
			m_ftpFile = NULL;
			SetEvent(m_ftp.m_hRcvDoneEvent);
		}
		break;
	}

	return 0;
}


afx_msg LRESULT CJWIS_TCSDlg::OnSetStatus(WPARAM wParam, LPARAM lParam)
{
	switch( wParam )
	{
	case STATUS_ODBC :
		SetODBCGUIStatus( lParam );
		break;
	case STATUS_FTP :
		SetFTPGUIStatus( lParam );
		break;
	}

	return 0;
}

/**
* TCS 데이터를 로그로 출력한다.
*/
afx_msg LRESULT CJWIS_TCSDlg::OnLogTkData(WPARAM wParam, LPARAM lParam)
{
	int type, code, serial, icnum, tType, fType;

	if(m_chk_prtTCS.GetCheck() == FALSE) return 0L;

	serial = HIWORD(wParam);
	type   = HIBYTE(LOWORD(wParam));
	code   = LOBYTE(LOWORD(wParam));
	icnum  = HIWORD(lParam);
	tType  = HIBYTE(LOWORD(lParam));
	fType  = LOBYTE(LOWORD(lParam));

	CString logstr;
	logstr.Format("[TCS DATA] 제어코드=%02X, 위반코드=%02X, 일련번호=%04d, 입구=%03d, 차종(권)=%01d, 차종(확정)=%01d\n",type, code, serial, icnum, tType, fType);
	AddLog(logstr);

	return 0;
}


HBRUSH CJWIS_TCSDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(m_edit_odbcConn.GetSafeHwnd() == pWnd->GetSafeHwnd())
	{
		if(m_odbc_conn == 1)
			pDC->SetTextColor(RGB(0, 0, 0));
		else
			pDC->SetTextColor(RGB(255, 0, 0));
	}
	else if(m_edit_tcsConn.GetSafeHwnd() == pWnd->GetSafeHwnd())
	{
		if(m_tcsconn == 1)
			pDC->SetTextColor(RGB(0, 0, 0));
		else
			pDC->SetTextColor(RGB(255, 0, 0));
	}
	else if(m_edit_svrConn.GetSafeHwnd() == pWnd->GetSafeHwnd())
	{
		if(m_svrconn == 1)
			pDC->SetTextColor(RGB(0, 0, 0));
		else
			pDC->SetTextColor(RGB(255, 0, 0));
	}
	else if(m_edit_ftpStatus.GetSafeHwnd() == pWnd->GetSafeHwnd())
	{
		if(m_FTP_Status == 1)
			pDC->SetTextColor(RGB(0, 0, 0));
		else
			pDC->SetTextColor(RGB(255, 0, 0));
	}
	
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CJWIS_TCSDlg::SetSVRGUIStatus(int status)
{
	m_svrconn = status;;
	if(status == 1){
		m_edit_svrConn.SetWindowText("연결");
	}
	else{
		m_edit_svrConn.SetWindowText("단절");
	}
}

void CJWIS_TCSDlg::SetODBCGUIStatus(int status)
{
	m_odbc_conn = status;;
	if(status == 1){
		m_stsODBC = SQL_SUCCESS;
		m_edit_odbcConn.SetWindowText("연결");
	}
	else{
		m_stsODBC = SQL_ERROR;
		m_edit_odbcConn.SetWindowText("단절");
	}
}

void CJWIS_TCSDlg::SetFTPGUIStatus(int status)
{
	m_FTP_Status = status;
	if(status == 1)
		m_edit_ftpStatus.SetWindowText("연결");
	else
		m_edit_ftpStatus.SetWindowText("단절");
}

void  CJWIS_TCSDlg::SetTCSCGUIStatus(int status)
{
	m_tcsconn = status;
	if(status == 1){
		m_edit_tcsConn.SetWindowText("연결");
	}
	else{
		m_edit_tcsConn.SetWindowText("단절");
	}
}

/**
* 서버로 상태정보를 보낸다. 
*/
void CJWIS_TCSDlg::SendStatus()
{
	char buf[32];
	int  len;

	len = 0;
	buf[len++] = 'S';
	//이하 데이터 구성

	if(!m_mtSvrSock.m_connected) {
		return;
	}

	send(m_mtSvrSock.m_sock, buf, len, 0);
}

/**
* 백업 파일을 생성한다.
*
*@param		srcPath		
*@param		dstPath		
*@param		EraseSec		
*/
void CJWIS_TCSDlg::BackupFiles(CString srcPath, CString dstPath, int EraseSec)
{
	WIN32_FIND_DATA	ffData;
	HANDLE	findhandle;
	int		FindResult;
	SYSTEMTIME st;
	FILETIME ft, ftLocal;
	char*	buf;
	char	strdate[7], strDir[256];

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//   삭제 시간의 기준 설정
	///////////////////////////////////////////////////////////////////////////////////////////////////
	CTime t = CTime::GetCurrentTime();
	t -= CTimeSpan(0, 0, 0, EraseSec);	//1 hour exactly
	t.GetAsSystemTime(st);

	SystemTimeToFileTime(&st, &ft);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//  삭제작업 실행
	///////////////////////////////////////////////////////////////////////////////////////////////////
	findhandle = FindFirstFile(srcPath + "\\*.jpg", &ffData);
	if(findhandle != INVALID_HANDLE_VALUE)
	{
		FindResult = 1;
	}
	else
	{
		FindResult = 0;
	}

	while(FindResult > 0)
	{
		FileTimeToLocalFileTime( &ffData.ftLastWriteTime, &ftLocal ); 
		//기준시간보다 전에 생성되었음
		if(CompareFileTime(&ft, &ftLocal) == 1)
		{
			buf = ffData.cFileName;
			memcpy(strdate, &buf[7], 6);
			strdate[6] = '\0';

			memset(strDir, 0, sizeof(strDir));
			sprintf_s(strDir, 256, "%s\\%s", dstPath, strdate);
			CreateDir(strDir);
			sprintf_s(strDir, 256, "%s\\%s\\%s", dstPath, strdate, ffData.cFileName);
			MoveFile(srcPath+"\\"+ffData.cFileName, strDir);
			DeleteFile(srcPath+"\\"+ffData.cFileName);
		}
		else
		{
			DeleteFile(srcPath+"\\"+ffData.cFileName);
		}
		FindResult = FindNextFile(findhandle, &ffData);
	}

	FindClose(findhandle);
}

/**
* 차선제어기 포트를 초기화한다. 
*/
void CJWIS_TCSDlg::InitLCLPort()
{
	
	if(m_SvrSock != NULL){
		delete m_SvrSock;
	}
	
	m_SvrSock = new CServerSock;
	m_SvrSock->Init(m_hWnd);
	m_SvrSock->m_pAgentSock->m_ptcsLnk = &m_tcsLink;
	m_SvrSock->Listen(m_lclPort, 5);
	
}

/**
* 견인 메세지를 보낸다.
*/
void CJWIS_TCSDlg::SendToTowTrigger()
{
	HWND  RecoghWnd = NULL;
	DWORD dwResult = 0;

	RecoghWnd = ::FindWindow(NULL, m_TitleRecogSW); 

	if(IsWindow(RecoghWnd)){
		SendMessageTimeout(RecoghWnd, idMsgMtTkTow, 0, 0, SMTO_NORMAL, 0, &dwResult);
		AddLog("[영상촬영] LOOP 진출시 트리거 명령 전송완료... \n");
	}
	else{
		AddLog("[영상촬영] LOOP보드 통신 S/W를 찾을 수 없음 \n");
	}
}

/**
* FTP 버퍼를 설정하고 처리코드에 대해 예외처리를 하고TCS 정보를 생성하여 주제어기에 이미지 데이터를 전송할 준비를 한다. 
* 백업경로에 저장한다. 
*
*@param		mtimg		
*@param		tk		
*/
// @@cjw - 실제 ftp로 전송하는 함수
void CJWIS_TCSDlg::SetFtpBuffer(MtImg *mtimg, TkData *tk)
{
	CString filename, tmpName, pltFileName, tagFileName;
	CString renameCopyfile;
	CString copyTargetDir;
	int CarImglen = 0, pltImglen = 0, i, p, sum, sndsize;
	CFile fjpg1, fjpg2, fjpg3, tagFile, fjpg4;
	char headerstr[256], *strTCSData;
	int num[4];
	char *buf = g_ftpBuffer;
	char tmpFileName[256], ftpFilename[256];
	CString logstr;
	char strDir[256];
	char strdate[7];	//백업용 Directory 생성용 스트링
	int brk;
	char bufx[5]={'\0','\0','\0','\0','\0'}, bufy[5]={'\0','\0','\0','\0','\0'};
	DWORD tmpSize;
	char dc_card[17], free_card[17], dc_card_old[11], free_card_old[11];

	memset(dc_card, 0, 17);
	memset(free_card, 0, 17);
	memset(dc_card_old, 0, 11);
	memset(free_card_old, 0, 11);
	
	//2011.04.30 전송하려는 영상이 NULL일 경우 빈영상을 전송하지 않고 리턴한다
	if(mtimg == NULL)
	{
		return;
	}

	i = strlen(mtimg->ImgInfo.dstFileName);
	if(i < 5)
	{
		return;
	}
	
	memcpy(tmpFileName, mtimg->ImgInfo.dstFileName, i-1);	//Write완료후에 Rename하려고
	tmpFileName[i-1] = '\0';
	memcpy(ftpFilename, mtimg->ImgInfo.dstFileName, i);
	ftpFilename[i] = '\0';

	//차량영상 파일 오픈
	filename = m_tmpImgPath + "\\" + mtimg->ImgInfo.srcFileName;
	CxImage cximg;
	cximg.Load(filename, CXIMAGE_FORMAT_JPG);

	if(cximg.Mean() < 140){
		cximg.Light(25, -5);
		cximg.Save(filename, CXIMAGE_FORMAT_JPG);
	}
	cximg.Destroy();
	Sleep(10);

	//차량영상을 여는데 실패하면 리턴
	if( fjpg1.Open(filename, CFile::modeRead, NULL) == 0 )
	{	
		return;
	}
	CarImglen = (int)fjpg1.GetLength();

	if(CarImglen <= 0)
	{
		return;
	}

	//번호판영상 파일 열기
	if(mtimg->ImgInfo.bRecog > RECOG_AREA)
	{
		pltFileName = m_pltImgPath + "\\" + mtimg->ImgInfo.pltFileName;
		if(fjpg2.Open(pltFileName, CFile::modeRead, NULL) == 0)
		{
			pltImglen = 0;
		}
		else
		{
			pltImglen = (int)fjpg2.GetLength();
		}
	}
	else
	{
		pltImglen = 0;
	}

	//버퍼의 할당
	sndsize = CarImglen + pltImglen + 513;
	memset(buf, 0, sndsize);

	p = 0;
	//차량영상 읽기
	sum = 0; 
	while(sum < CarImglen)
	{
		i = fjpg1.Read(&buf[p], CarImglen-sum);
		p += i;		
		sum += i;
	}
	
	//번호판영상 읽기
	sum = 0;
	mtimg->ImgInfo.pLeft = 0;
	mtimg->ImgInfo.pTop  = 0;
	if(pltImglen > 0)
	{
		while(sum < pltImglen)
		{
			i = fjpg2.Read(&buf[p], pltImglen-sum);
			p += i;
			sum += i;
		}
		
		tmpName = m_tagPath + "\\" + mtimg->ImgInfo.srcFileName;

		brk = tmpName.Find(".jpg");
		if(brk < MIN_EXT_POS)
		{
			brk = tmpName.Find(".JPG");
		}
		
		if(brk >= MIN_EXT_POS)
		{
			tagFileName = tmpName.Left(brk) + ".tag";
		}
		
		if( tagFile.Open(tagFileName, CFile::modeRead, NULL) != 0 )
		{
			tagFile.Seek(36, CFile::begin);
			tagFile.Read(bufx, 4);
			tagFile.Read(bufy, 4);
			try{
				mtimg->ImgInfo.pLeft = atoi(bufx);
				mtimg->ImgInfo.pTop  = atoi(bufy);
			}
			catch(...)
			{
				mtimg->ImgInfo.pLeft = 0;
				mtimg->ImgInfo.pTop  = 0;
			}
			tagFile.Close();
		}

	}

	//"Run NT " - 7Byte
	sprintf_s(headerstr, sizeof(headerstr), "%s", MT_SVR_FTP_STRING);
	memcpy(&buf[p], headerstr, 7);
	p += 7;
	//Unkown Header - 25Byte
	num[0] = 1;			//아마도... 0=TCS/과적, 1=HIPASS

	//번호판 인식여부 - 2012.12.04 SIJ, I/F사양서 변경으로 확인이 필요함
	if(mtimg->ImgInfo.bRecog == RECOG_OK)
	{	
		num[1] = 2;
	}
	else
	{
		num[1] = 1;
	}

	//2013.7.12 SIJ, 영업소번호는 통행권데이터로부터 획득
	num[2] = m_NumCamera;					//면탈제어기의 번호
	num[3] = tk->extICNum;					//영업소 ID

	//2012.12.04 배열의 크기 조정, 5-->4
	for(i=0; i<4; i++)
	{
		memcpy(&buf[p], &num[i], 4);
		p += 4;
	}
	
	switch(m_gateType)	// 2012.12.04 SIJ 차선의 운영형태 (0=폐쇄식출구, 1=개방식상행, 2=개방식하행)
	{					// O-출구, I-입구, U-상행, D-하행
	case CLOSE_GATE : 
		buf[p++] = 'O';		
		break;
	case OPEN_UP_GATE : 
		buf[p++] = 'U';		
		break;
	case OPEN_DN_GATE : 
		buf[p++] = 'D';		
		break;
	default :
		buf[p++] = 'O';	//해당사항이 없으면 기본형은 TCS출구로 설정		
		break;
	}
	
	p+=4;				//2012.12.04 SIJ, DUMMY[4]
	buf[p++] = 'D';		//2012.12.04 SIJ, 카드번호가 16자리로 확장된 버전임을 뜻함
	buf[p++] = 'I';
	buf[p++] = 'S';
	buf[p++] = 'C';
	//TCS처리데이터+입구데이터 - 22
	memset(headerstr, 0, 100);
	sprintf_s(headerstr, sizeof(headerstr), "%02X%02d%02d%02d%02d%04d%03d%01d%02d%01d%01d",
			tk->cmd,tk->entMon,tk->entDay,tk->entHour,tk->entMin,tk->entSerial,tk->entICNum,
			tk->entProper,tk->entLane,tk->entType,tk->entCntAxle);
	memcpy(&buf[p], headerstr, 22);
	strTCSData = &buf[p];
	p+=22;
			
	//TCS출구데이터	- 63Byte (0이 하나 포함된 것에 주의)
	memcpy(dc_card_old,  tk->extDisc, 10);
	memcpy(free_card_old,tk->extExemp,10);
	memcpy(dc_card,      tk->extDisc, 16);
	memcpy(free_card,    tk->extExemp,16);
	memset(headerstr, 0, 100);
	sprintf_s(headerstr, sizeof(headerstr), "%02d%02d%02d%02d%02d%04d%02X%04d%06d%06d%03d%01d%01d%01d%s%s",
			tk->extYear,tk->extMon,tk->extDay,tk->extHour,tk->extMin,tk->extWorkNum,tk->extCode,
			tk->extSerial,tk->extFare,tk->extCash,tk->extICNum,tk->extCntAxle,tk->extTtype,
			tk->extFtype, dc_card_old, free_card_old);
	memcpy(&buf[p], headerstr, 58);
	p+=58;	//2013.4.18 SIJ, 63-->58
	//2013.4.18, SIJ - DUMMY를 0으로 채움
	buf[p++] = '0';
	buf[p++] = '0';
	buf[p++] = '0';
	buf[p++] = '0';
	buf[p++] = '0';

	//제어코드스트링 - 15Byte
	//도주, 면제, 긴급, 사무실, 차종변경, 견인, 후처리
	memset(headerstr, 0, 16);
	if(tk->cmd == TICKET_CONFIRM_DATA){
		sprintf_s(headerstr, sizeof(headerstr), "통행권확인     ");		//2012.12.04 SIJ,  통행권확인-->권확인
		logstr.Format("[TCS] 통행권확인데이터 응답\n");
		AddLog(logstr);
		logstr.Format("TCS에서 전송받은 이미지 번호 : %d\n", mtimg->QNum);
		AddLog(logstr);
		logstr.Format("종별: 57 시리얼번호: %04d 이미지 번호:%d 처리코드: %x 차종 : %d 통행권확인데이터: %85s\n",
					  tk->extSerial, mtimg->QNum, tk->extCode, tk->extFtype, strTCSData);
		AddLog(logstr);
	}
	else if(tk->cmd == EXIT_DATA){
		switch(tk->extCode)
		{
		case CD_FREE :	
			sprintf_s(headerstr, sizeof(headerstr), "면제           ");
			break;
		case CD_URGENT :	
			sprintf_s(headerstr, sizeof(headerstr), "긴급           ");
			break;
		case CD_TOW :	
			sprintf_s(headerstr, sizeof(headerstr), "견인           ");
			break;
		case CD_CAR_CHANGE :	
			sprintf_s(headerstr, sizeof(headerstr), "차종변경       ");
			break;
		case CD_NEXT_PROC :	
			sprintf_s(headerstr, sizeof(headerstr), "후처리         ");
			break;
		case CD_COMPACT_TRUCK :	
			sprintf_s(headerstr, sizeof(headerstr), "소형화물       ");
			break;
		case CD_BUSINESS_TRUCK :	
			sprintf_s(headerstr, sizeof(headerstr), "사업용화물     ");
			break;
		case CD_EMPLOYEE_FREE_CARD : 
			sprintf_s(headerstr, sizeof(headerstr), "직원면제카드   ");
			break;
		case OVERLAB_DATA_USE :
			sprintf_s(headerstr, sizeof(headerstr), "중복영상사용   ");
			logstr.Format("[TCS] 중복영상 응답\n");		AddLog(logstr);
			break;
		}
		
		logstr.Format("[TCS] 출구특별처리데이터 응답\n");
		AddLog(logstr);
		logstr.Format("TCS에서 전송받은 이미지 번호 : %d\n", mtimg->QNum);
		AddLog(logstr);
		logstr.Format("종별: 37 시리얼번호: %04d 이미지 번호:%d 처리코드: %x 차종 : %d 출구특별처리데이터: %85s\n",
					  tk->extSerial, mtimg->QNum, tk->extCode, tk->extFtype, strTCSData);
		AddLog(logstr);
	}
	else if(tk->cmd == OFFICE_DATA)
	{
		switch(tk->extCode)
		{
		case 0x01 :	
			sprintf_s(headerstr, sizeof(headerstr), "도주           ");
			break;
		case 0x06 : 
			sprintf_s(headerstr, sizeof(headerstr), "불법 U-TURN    ");	//2013.4.15 SIJ
			break;
		case 0x99 :	
			sprintf_s(headerstr, sizeof(headerstr), "사무실         ");
			break;
		}
		logstr.Format("[TCS] 사무실처리데이터 응답\n");
		AddLog(logstr);
		logstr.Format("TCS에서 전송받은 이미지 번호 : %d\n", mtimg->QNum);
		AddLog(logstr);
		logstr.Format("종별: 36 시리얼번호: %04d 이미지 번호:%d 처리코드: %x 차종 : %d 사무실처리데이터: %85s\n",
					  tk->extSerial, mtimg->QNum, tk->extCode, tk->extFtype, strTCSData);
		AddLog(logstr);
	}
	
	logstr.Format("할인카드정보:%s, 면제카드정보:%s\n", dc_card, free_card);
	AddLog(logstr);
	
	logstr.Format("처리버퍼 : %d\n", mtimg->QNum);
	AddLog(logstr);

	memcpy(&buf[p], headerstr, 15);
	p+=15;

	//차량 영상사이즈 - 4Byte
	memcpy(&buf[p], &CarImglen, 4);
	p+=4;
	logstr.Format("Full Jpeg Suceess : %d\n", CarImglen);
	AddLog(logstr);
	//번호판영상사이즈 - 4Byte
	memcpy(&buf[p], &pltImglen, 4);
	p+=4;
	logstr.Format("Num Jpeg Suceess : %d\n", pltImglen);
	AddLog(logstr);

	//Unkown - 10Byte - 2012.12.04 SIJ, DUMMY[10]
	p+=10;
	
	//인식된 차량번호
	memset(headerstr, 0, sizeof(headerstr));
	i = strlen(mtimg->ImgInfo.strPlate);
	if(mtimg != NULL)
	{
		if(mtimg->ImgInfo.bRecog == RECOG_OK)
		{
			memcpy(headerstr, mtimg->ImgInfo.strPlate, i);
		}
		else
		{
			sprintf_s(headerstr, sizeof(headerstr), "인식실패");
		}
	}
	memcpy(&buf[p], headerstr, 12);
	p+=12;

	//인식좌표 - left, top, right, bottom
	memcpy(&buf[p], &mtimg->ImgInfo.pLeft, 4);
	p+=4;
	memcpy(&buf[p], &mtimg->ImgInfo.pTop, 4);
	p+=4;
	tmpSize = mtimg->ImgInfo.pLeft + mtimg->ImgInfo.pImgW;
	memcpy(&buf[p], &tmpSize, 4);
	p+=4;
	tmpSize = mtimg->ImgInfo.pTop  + mtimg->ImgInfo.pImgH;
	memcpy(&buf[p], &tmpSize, 4);
	p+=4;

	//원영상 - width, height, size
	memcpy(&buf[p], &mtimg->ImgInfo.ImgW, 4);
	p+=4;
	memcpy(&buf[p], &mtimg->ImgInfo.ImgH, 4);
	p+=4;
	tmpSize = mtimg->ImgInfo.ImgW*mtimg->ImgInfo.ImgH;
	memcpy(&buf[p], &tmpSize, 4);
	p+=4;

	//카메라 타입
	sprintf_s(headerstr, sizeof(headerstr), "13S2");
	memcpy(&buf[p], headerstr, 4);
	p+=4;

	//2012.12.04 새로바뀐 할인카드와 면제카드
	memcpy(&buf[p], dc_card, 16);
	p+=16;
	memcpy(&buf[p], free_card, 16);
	p+=16;

	//Dummy - 316Byte, 2012.12.04 SIJ, 316-->284
	p+=284;

	//EOF String
	sprintf_s(headerstr, sizeof(headerstr), "EOF");
	memcpy(&buf[p], headerstr, 3);

	fjpg3.Open(m_svrImgPath+"\\"+tmpFileName, CFile::modeCreate|CFile::modeWrite, NULL);
	fjpg3.Write(buf, sndsize);
	fjpg3.Close();

	//// 19.10.31 YJW 추가 ///////
	CString strlog;
	strlog.Format("파일저장 (1).\n");	// TEST SMJ
	AddLog(strlog);
	fjpg4.Open(m_svrImgPathCopy+"\\"+tmpFileName, CFile::modeCreate|CFile::modeWrite, NULL);
	fjpg4.Write(buf, sndsize);
	fjpg4.Close();
	////////////////////////////////

	logstr.Format("File Save Suceess : %d\n", sndsize);
	AddLog(logstr);
	AddLog("===========================================================================\n");
	AddLog("\n");

	Sleep(10);	//2011.04.30 최소시간만 쉰다. 파일을 닫는 시간을 벌어주기 위해...

	//차량영상파일 백업
	memcpy(strdate, &mtimg->ImgInfo.dstFileName[7], 6);
	strdate[6] = '\0';

	memset(strDir, 0, sizeof(strDir));
	sprintf_s(strDir, 256, "%s\\%s", m_bakImgPath, strdate);
	CreateDir(strDir);
	memset(strDir, 0, sizeof(strDir));
	sprintf_s(strDir, 256, "%s\\%s\\%s", m_bakImgPath, strdate, ftpFilename);
	CopyFile(m_svrImgPath+"\\"+tmpFileName, strDir, FALSE);
	// 19.10.28 YJW 추가

	strlog.Format("파일저장 (10) %s .\n", strDir);	// TEST SMJ
	AddLog(logstr);

	//CopyFile(m_svrImgPathCopy+"\\"+tmpFileName, strDir, FALSE);
	mtimg->bSndFTP = TRUE;
	
	//전송할 수 있도록 확장자를 ".JPG"로 바꿈
	rename(m_svrImgPath+"\\"+tmpFileName, m_svrImgPath+"\\"+ftpFilename);
	// 19.12.07 YJW SvrImgCopy 저장시 앞문자 붙여서 저장
	CString tgDir;
	tgDir = "D:\\SvrImgTot";

	switch(m_gateType)
	{
		case 0 : rename(m_svrImgPathCopy+"\\"+tmpFileName, m_svrImgPathCopy+"\\o"+ftpFilename);
				renameCopyfile	= m_svrImgPathCopy+"\\o"+ftpFilename;
				copyTargetDir	= tgDir + "\\o"+ftpFilename;
			break;
		case 1 : rename(m_svrImgPathCopy+"\\"+tmpFileName, m_svrImgPathCopy+"\\u"+ftpFilename);
				renameCopyfile = m_svrImgPathCopy+"\\u"+ftpFilename;
				copyTargetDir	= tgDir + "\\u"+ftpFilename;
			break;
		case 2 : rename(m_svrImgPathCopy+"\\"+tmpFileName, m_svrImgPathCopy+"\\d"+ftpFilename);
				renameCopyfile = m_svrImgPathCopy+"\\d"+ftpFilename;
				copyTargetDir	= tgDir + "\\d"+ftpFilename;
			break;
		default : rename(m_svrImgPathCopy+"\\"+tmpFileName, m_svrImgPathCopy+"\\o"+ftpFilename);	//2012.12.04 SIJ - 기본형을 'o'(폐쇄식출구)로 지정
				renameCopyfile = m_svrImgPathCopy+"\\o"+ftpFilename;
				copyTargetDir	= tgDir + "\\o"+ftpFilename;
			break;
	}
	strlog.Format("파일저장 (11) \n");	// TEST SMJ
	AddLog(strlog);
	//rename(m_svrImgPathCopy+"\\"+tmpFileName, m_svrImgPathCopy+"\\"+ftpFilename);
	Sleep(50);
	//복사
	::CopyFile(renameCopyfile, copyTargetDir, FALSE);
	Sleep(50);
	// 삭제
	::DeleteFile(renameCopyfile);

	fjpg1.Close();
	if(pltImglen > 0)
	{
		fjpg2.Close();
	}

	DeleteFile(tagFileName);
}

/**
* 영상처리기에 리셋 명령 전송한다. 
*/
void CJWIS_TCSDlg::SendToRecogSW()
{
	HWND  RecoghWnd = NULL;
	DWORD dwResult = 0;

	if(m_ImgThread != NULL)
	{
		if(m_ImgThread->m_hThread != NULL)
		{
			GetExitCodeThread(m_ImgThread->m_hThread, &dwResult);
			if(dwResult != STILL_ACTIVE)
			{
				TerminateThread(m_ImgThread->m_hThread, 0);
				delete m_ImgThread;
				m_ImgThread = NULL;
			}
		}
		else
		{
			delete m_ImgThread;
			m_ImgThread = NULL;
		}
	}

	if(dwResult != STILL_ACTIVE)
	{
		AddLog(" LoadImgThread 종료됨. Thread 재실행 시도중...\n");
		m_ImgThread = AfxBeginThread(LoadImgThread, this, THREAD_PRIORITY_ABOVE_NORMAL);
		return;
	}

	AddLog("[영상오류] 영상이 생성되지 않음 \n");
	dwResult = GetTickCount();
	if(dwResult > m_LoopRstTickCnt)
	{
		//2011.12.18 - 1시간에서 10분으로 수정
		if( (dwResult - m_LoopRstTickCnt) < (600 * 1000) )	
		{
			AddLog("[영상오류] 마지막리셋시간부터 10분이 경과되지 않음 \n");
			return;
		}
	}
	m_LoopRstTickCnt = dwResult;

	RecoghWnd = ::FindWindow(NULL, m_TitleRecogSW); 

	if(IsWindow(RecoghWnd))
	{
		SendMessageTimeout(RecoghWnd, idMsgMtTkData, 0, 0, SMTO_NORMAL, 0, &dwResult);
		AddLog("[영상오류] I/O보드 리셋명령 전송완료... \n");
	}
	else
	{
		AddLog("[영상오류] I/O제어 S/W를 찾을 수 없음 \n");
	}
}

void CJWIS_TCSDlg::OnBtnWriteIni()
{	
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	char *tmpstr;
	CString strenv;

	m_dlgSetup.m_carImgPath		= m_carImgPath;
	m_dlgSetup.m_pltImgPath		= m_pltImgPath;
	m_dlgSetup.m_tagPath		= m_tagPath;
	m_dlgSetup.m_svrImgPath		= m_svrImgPath;
	m_dlgSetup.m_svrImgPathCopy = m_svrImgPathCopy;
	m_dlgSetup.m_tmpImgPath		= m_tmpImgPath;
	m_dlgSetup.m_bakImgPath		= m_bakImgPath;
	m_dlgSetup.m_LogPath		= m_LogPath;
	m_dlgSetup.m_MyIPAddr		= m_MyIPAddr;
	m_dlgSetup.m_MySubnet		= m_MySubnet;
	m_dlgSetup.m_ftpAddr		= m_ftpAddr;
	m_dlgSetup.m_ftpUser		= m_ftpUser;
	m_dlgSetup.m_ftpPass		= m_ftpPass;
	m_dlgSetup.m_ftpPort		= m_ftpPort;
	m_dlgSetup.m_SvrPort		= m_SvrPort;
	m_dlgSetup.m_lclPort		= m_lclPort;
	m_dlgSetup.m_NumCamera		= m_NumCamera;
	m_dlgSetup.m_NumLane		= m_NumLane;
	m_dlgSetup.m_NumOffice		= m_NumOffice;
	m_dlgSetup.m_DBUser			= m_DBUser;
	m_dlgSetup.m_DBPass			= m_DBPass;
	m_dlgSetup.m_gateType		= m_gateType;
	m_dlgSetup.m_MixedHipass	= m_MixedHipass;
	m_dlgSetup.m_AllTkData		= m_AllTkData;
	m_dlgSetup.m_SendDoubleCode = m_SendDoubleCode;;

	if(m_dlgSetup.DoModal() != IDOK)
	{
		return;
	}

	m_carImgPath = m_dlgSetup.m_carImgPath;
	tmpstr = m_carImgPath.GetBuffer(256);
	CreateDir(tmpstr);
	m_carImgPath.ReleaseBuffer();

	m_pltImgPath = m_dlgSetup.m_pltImgPath;
	tmpstr = m_pltImgPath.GetBuffer(256);
	CreateDir(tmpstr);
	m_pltImgPath.ReleaseBuffer();

	m_tagPath = m_dlgSetup.m_tagPath;
	tmpstr = m_tagPath.GetBuffer(256);
	CreateDir(tmpstr);
	m_tagPath.ReleaseBuffer();

	m_svrImgPath = m_dlgSetup.m_svrImgPath;
	tmpstr = m_svrImgPath.GetBuffer(256);
	CreateDir(tmpstr);
	m_svrImgPath.ReleaseBuffer();

	// 19.10.28 YJW 추가
	CString strlog;
	strlog.Format("FILE (12) \n");
	AddLog(strlog);
	m_svrImgPathCopy = m_dlgSetup.m_svrImgPathCopy;
	tmpstr = m_svrImgPathCopy.GetBuffer(256);
	CreateDir(tmpstr);
	m_svrImgPathCopy.ReleaseBuffer();
	///////////////////////

	m_tmpImgPath = m_dlgSetup.m_tmpImgPath ;
	tmpstr = m_tmpImgPath.GetBuffer(256);
	CreateDir(tmpstr);
	m_tmpImgPath.ReleaseBuffer();

	m_bakImgPath = m_dlgSetup.m_bakImgPath;
	tmpstr = m_bakImgPath.GetBuffer(256);
	CreateDir(tmpstr);
	m_bakImgPath.ReleaseBuffer();

	m_LogPath = m_dlgSetup.m_LogPath;
	tmpstr = m_LogPath.GetBuffer(256);
	CreateDir(tmpstr);
	m_LogPath.ReleaseBuffer();

	//2012.7.12 SIJ --------------------
	m_MyIPAddr = m_dlgSetup.m_MyIPAddr;
	m_MySubnet = m_dlgSetup.m_MySubnet;
	//----------------------------------
	m_ftpAddr   = m_dlgSetup.m_ftpAddr;
	m_ftpUser   = m_dlgSetup.m_ftpUser;
	m_ftpPass   = m_dlgSetup.m_ftpPass;
	m_ftpPort   = m_dlgSetup.m_ftpPort;
	m_SvrPort   = m_dlgSetup.m_SvrPort;
	m_lclPort   = m_dlgSetup.m_lclPort;
	m_NumCamera = m_dlgSetup.m_NumCamera;
	m_NumLane   = m_dlgSetup.m_NumLane;
	m_NumOffice = m_dlgSetup.m_NumOffice;
	m_DBUser    = m_dlgSetup.m_DBUser;
	m_DBPass    = m_dlgSetup.m_DBPass;
	m_gateType  = m_dlgSetup.m_gateType;
	m_MixedHipass = m_dlgSetup.m_MixedHipass;
	m_AllTkData = m_dlgSetup.m_AllTkData;	//2013.3.21 SIJ 개방식차로에서의 일반통행권데이터(0x57)의 전송
	m_SendDoubleCode = m_dlgSetup.m_SendDoubleCode;
	m_tcsLink.m_SendDoubleCode = m_SendDoubleCode;	//2013.5.30 SIJ

	strenv.Format("영업소ID : %d, \t 서 버 I P : %s", m_NumOffice, m_ftpAddr);
	m_static_env1.SetWindowText(strenv);
		
	switch(m_gateType)
	{
	case CLOSE_GATE : 
		strenv.Format("차선번호 : %d, \t 카메라ID : %d, \t 폐쇄식출구", m_NumLane, m_NumCamera);
		break;
	case OPEN_UP_GATE : 
		strenv.Format("차선번호 : %d, \t 카메라ID : %d, \t 개방식상행", m_NumLane, m_NumCamera);
		break;
	case OPEN_DN_GATE :
		strenv.Format("차선번호 : %d, \t 카메라ID : %d, \t 개방식하행", m_NumLane, m_NumCamera);
		break;
	}
	if(m_MixedHipass == TRUE){
		strenv += ",  혼용차로 모드";
	}
	else{
		strenv += ",  전용차로 모드";
	}
	
	m_static_env2.SetWindowText(strenv);
		
	m_tcsLink.m_NumLane = m_NumLane;

	WriteINI();

}

/**
* INI를 저장한다. 
*/
void CJWIS_TCSDlg::WriteINI()
{
	CString str, keyString, iniName;
	TCHAR pszPathName[_MAX_PATH]; 
	GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH); 
	iniName = pszPathName;

	int brk = 0;
	brk = iniName.ReverseFind('\\');
	if(brk){
		iniName = iniName.Left(brk);
	}
	iniName = iniName + "\\jwis_tcs.ini";

	WritePrivateProfileString(PATH, CAR_IMAGE_PATH, m_carImgPath, iniName);
	WritePrivateProfileString(PATH, PLATE_IMAGE_PATH, m_pltImgPath, iniName);
	WritePrivateProfileString(PATH, TAG_PATH, m_tagPath, iniName);
	WritePrivateProfileString(PATH, SERVER_IMAGE_PATH, m_svrImgPath, iniName);
	WritePrivateProfileString(PATH, SERVER_IMAGE_COPY_PATH, m_svrImgPathCopy, iniName);
	WritePrivateProfileString(PATH, TEMP_IMAGE_PATH, m_tmpImgPath, iniName);
	WritePrivateProfileString(PATH, BACKUP_IMAGE_PATH, m_bakImgPath, iniName);
	WritePrivateProfileString(PATH, LOG_FILE_PATH, m_LogPath, iniName);

	WritePrivateProfileString(NETWORK, SELF_ETHERNET_NAME, m_MyEtherName, iniName);
	WritePrivateProfileString(NETWORK, SELF_IP_ADDRESS,    m_MyIPAddr,    iniName);
	WritePrivateProfileString(NETWORK, SELF_SUBNET_MASK,   m_MySubnet,    iniName);
	WritePrivateProfileString(NETWORK, SERVER_IP_ADDRESS, m_ftpAddr, iniName);
	WritePrivateProfileString(NETWORK, FTP_USERNAME, m_ftpUser, iniName);
	WritePrivateProfileString(NETWORK, FTP_PASSWORD, m_ftpPass, iniName);
	
	// 2019.11.12 YJW 추가 ////////////////////////////////////////
	//WritePrivateProfileString(NETWORK, SERVER2_IP_ADDRESS, m_ftp2Addr, iniName);
	//WritePrivateProfileString(NETWORK, FTP2_USERNAME, m_ftp2User, iniName);
	//WritePrivateProfileString(NETWORK, FTP2_PASSWORD, m_ftp2Pass, iniName);
	//////////////////////////////////////////////////////////////////

	str.Format("%d", m_SvrPort);
	WritePrivateProfileString(NETWORK, MTSETVER_PORT, str, iniName);
	//str.Format("%d", m_Svr2Port);
	//WritePrivateProfileString(NETWORK, MTSETVER2_PORT, str, iniName);

	str.Format("%d", m_lclPort);
	WritePrivateProfileString(NETWORK, LCL_PORT, str, iniName);
	m_lclPort = atoi(str);

	str.Format("%d", m_ftpPort);
	WritePrivateProfileString(NETWORK, FTP_DATA_PORT, str, iniName);
	//str.Format("%d", m_ftp2Port);
	//WritePrivateProfileString(NETWORK, FTP2_DATA_PORT, str, iniName);

	//Database ODBC User Name
	WritePrivateProfileString(NETWORK, DBSERVER_ODBC_USER, m_DBUser, iniName);

	//Database ODBC Password
	WritePrivateProfileString(NETWORK, DBSERVER_ODBC_PASS, m_DBPass, iniName);

	//로그를 출력할 것인지에 대한 설정
	if(m_chk_prtLog.GetCheck() > 0 ){
		WritePrivateProfileString(LOG, PRINT_LOG, "1", iniName);
	}
	else{
		WritePrivateProfileString(LOG, PRINT_LOG, "0", iniName);
	}

	//로그를 자동 저장할 것인지에 대한 설정
	if(m_chk_saveLog.GetCheck() > 0 ){
		WritePrivateProfileString(LOG, AUTOSAVE_LOG, "1", iniName);
	}
	else{
		WritePrivateProfileString(LOG, AUTOSAVE_LOG, "0", iniName);
	}

	//로그를 자동 저장할 것인지에 대한 설정
	if(m_chk_prtTCS.GetCheck() > 0 ){
		WritePrivateProfileString(LOG, PRINT_TCS, "1", iniName);
	}
	else{
		WritePrivateProfileString(LOG, PRINT_TCS, "0", iniName);
	}

	//서버와 통신시에 사용되는 장치번호
	str.Format("%d", m_NumCamera);
	WritePrivateProfileString(ID, NUM_DEVICE, str, iniName);

	//차로번호 2자리
	str.Format("%d", m_NumLane);
	WritePrivateProfileString(ID, NUM_LANE, str, iniName);

	//서버와 통신시에 사용되는 영업소번호
	str.Format("%d", m_NumOffice);
	WritePrivateProfileString(ID, NUM_OFFICE, str, iniName);

	// 차선의 운영형태 (0=폐쇄식출구, 1=개방식상행, 2=개방식하행)
	str.Format("%d", m_gateType);
	WritePrivateProfileString(ID, GATE_TYPE, str, iniName);

	//혼용차로 여부
	str.Format("%d", m_MixedHipass);
	WritePrivateProfileString(ID, HIPASS_MIXED, str, iniName);

	//2013.3.21 SIJ 개방식차로에서 일반통행권확인데이터(0x57)의 전송여부
	str.Format("%d", m_AllTkData);
	WritePrivateProfileString(ID, ALL_TICKET_DATA_SEND, str, iniName);

	//2013.3.21 SIJ 개방식차로에서 일반통행권확인데이터(0x57)의 전송여부
	str.Format("%d", m_SendDoubleCode);
	WritePrivateProfileString(ID, SEND_DOUBLE_CODE, str, iniName);
}



void CJWIS_TCSDlg::OnBnClickedBtnInitq()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	disconnectDB();
}


void CJWIS_TCSDlg::OnBnClickedBtnSaveLog()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString FileName, tmpstr;
	int i,cnt;

	FileName = m_LogPath + "\\tcsLog.txt"; 
	CStdioFile logfile(FileName, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeReadWrite );
	logfile.SeekToEnd();

	cnt = m_redit_Log.GetLineCount();
	for(i=0; i<cnt; i++){
		m_redit_Log.GetLine(i, tmpstr.GetBuffer(512), 512);
		logfile.WriteString(tmpstr);
		logfile.WriteString("\n");
	}

	logfile.Close();
}


void CJWIS_TCSDlg::OnBnClickedBtnEraseBackup()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_BackupFileErasing == FALSE)
	{
		m_BackupFileErasing = TRUE;
		AfxBeginThread(EraseBackupFileThread, this, THREAD_PRIORITY_LOWEST);
		AddLog("백업파일 삭제 Thread 기동\n");
	}
}


void CJWIS_TCSDlg::OnBnClickedBtnTowTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SendToTowTrigger();
}


void CJWIS_TCSDlg::OnBnClickedBtnBacktrigger()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_TakeBackImg = TRUE;
}


void CJWIS_TCSDlg::OnBnClickedBtnExit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SendMessage(WM_CLOSE, 0, 0);
}
