﻿#include "StdAfx.h"
#include "JWIS_TCS.h"
#include "TcsLink.h"
#include "JWIS_TCSDlg.h"

#include <io.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define LOOP_RESET_COUNT			5
#define MAX_MATCHING_ERR_CNT		2

MtImgQ	g_ImgQ;
MtPk	g_mtpk;

CTcsLink::CTcsLink(void)
{
	memset(&m_tkSnd, 0, SIZE_TKDATA);
	memset(&g_ImgQ, 0, sizeof(MtImgQ));

	m_FlagEmptyImg  = FALSE;
	m_ImgErrCnt     = 0;
	m_tcsSockCnt    = 11;
	m_tcsSockErrCnt = 11;
	m_loopcnt		= 0;
	m_tkClosed		= FALSE;

	m_SendDoubleCode = FALSE;		// 2013.5.30
	m_workNum = 0;

	//전송되지 못한 영상을 폐쇄차로 데이터로 전송
	m_closedEvent = CreateEvent(NULL, TRUE, TRUE,  NULL);
		
}


CTcsLink::~CTcsLink(void)
{
	CloseHandle(m_closedEvent);
}

/**
* 이미지 큐를 초기화한다. 
*/
void CTcsLink::InitImgQ()
{
	//int i;
	for(int i=0; i < MT_MAX_IMGQ; i++)		// MT_MAX_IMGQ = 16
	{
		g_ImgQ.Img[i].Next = i + 1;
		g_ImgQ.Img[i].Prev = i - 1;
		
		memset(&g_ImgQ.Img[i].ImgInfo, 0, SIZE_IMGINFO); 
		g_ImgQ.Img[i].QNum  = i;
		g_ImgQ.Img[i].bUsed = FALSE;
		g_ImgQ.Img[i].bTrfd = FALSE;
		g_ImgQ.Img[i].bSndFTP = FALSE;
		g_ImgQ.Img[i].DoubleCode = 0;
	}
	g_ImgQ.Img[0].Prev = MT_MAX_IMGQ - 1;
	g_ImgQ.Img[MT_MAX_IMGQ - 1].Next = 0;
	g_ImgQ.pbgn = 0;
	g_ImgQ.pend = 0;
	g_ImgQ.pscr = -1;	// 초기화
	m_wrecker   = 0;	// 견인모드
	m_loopcnt   = 0;	// 루프홀딩 카운트

	memset(&m_ProcInfo, 0, sizeof(MtProcInfo));
	m_ImgErrCnt = 0;	// 영상이 남는 경우의 카운트

	m_prevCarClass = CAR_TYPE_NO;
	m_tkClosed = FALSE;
	m_ImgMatchAlg = 0;

	return;
}

/**
* 큐에 있는 데이터를 확인한다. 
*/
MtImg* CTcsLink::NewImgQ()
{
	int bFind = FALSE;
	int id = g_ImgQ.pend;
	MtPk *pk = NULL;
	LPARAM lparam;

	while (!bFind)
	{
		if(g_ImgQ.Img[id].bUsed == FALSE)
		{
			//기타 정보 초기화
			g_ImgQ.pend = id;
			g_ImgQ.Img[id].bTrfd  = FALSE;	// 처리 완료 여부
			g_ImgQ.Img[id].bUsed  = TRUE;
			g_ImgQ.Img[id].bSndFTP= FALSE;
			g_ImgQ.Img[id].DoubleCode = 0;	//2013.4.25

			bFind = TRUE;
		}
		else
		{
			id = g_ImgQ.Img[id].Next;
			//버퍼가 꽉찬 경우
			if(id == g_ImgQ.pbgn)	
			{
				m_delFilename.Format("%s", g_ImgQ.Img[g_ImgQ.pbgn].ImgInfo.srcFileName); 
				m_delpltFilename.Format("%s",g_ImgQ.Img[g_ImgQ.pbgn].ImgInfo.pltFileName); 
				lparam = g_ImgQ.Img[g_ImgQ.pbgn].bSndFTP;
				SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_DELFILE, lparam);
				FreeImgQ(&g_ImgQ.Img[g_ImgQ.pbgn]);
				g_ImgQ.pbgn = g_ImgQ.Img[g_ImgQ.pbgn].Next;
			}
		}
	} 
	m_bNextPrevKey = FALSE;

	SYSTEMTIME st;
	GetLocalTime(&st);

	ZeroMemory(g_ImgQ.Img[id].imgDateTime, 15);
	sprintf_s(g_ImgQ.Img[id].imgDateTime, 15, "%04d%02d%02d%02d%02d%02d",
		st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);

	AddLog("[ImgQ ID] %d, %s\n", id, g_ImgQ.Img[id].imgDateTime);

	return &g_ImgQ.Img[id];
}

/**
* 이미지 큐를 해제한다. 
*
*@param		img		
*/
void CTcsLink::FreeImgQ(MtImg *img)
{
	img->bUsed = FALSE;
	img->bTrfd = FALSE;
	img->bSndFTP = FALSE;
	img->DoubleCode = 0;
	
	memset(&img->ImgInfo, 0, SIZE_IMGINFO); 
}

/**
* 영상 큐 위치를 이후로 이동한다. 
*
*@param		queueID		
*/
int CTcsLink::MoveQfwd(int queueID)
{
	int id;
	//오류검사
	if( (queueID > 15) || (queueID < 0) )
	{	
		id = g_ImgQ.pscr;
	}
	else
	{
		id = queueID;
	}

	if(g_ImgQ.pbgn < 0)
	{
		g_ImgQ.pbgn = 0;
		return g_ImgQ.pbgn;
	}

	if(id == g_ImgQ.pend)
	{ 
		return g_ImgQ.pend;
	}
	
	id = g_ImgQ.Img[g_ImgQ.pscr].Next;

	g_ImgQ.pscr = id;
	return id;
}

/**
* 영상 큐 위치를 전으로 이동한다. 
*
*@param		queueID		
*/
int CTcsLink::MoveQrev(int queueID)
{
	int id;
	//오류검사
	if( (queueID > 15) || (queueID < 0) )
	{	
		id = g_ImgQ.pscr;
	}
	else
	{
		id = queueID;
	}

	if(g_ImgQ.pbgn < 0)
	{
		g_ImgQ.pbgn = 0;
		return g_ImgQ.pbgn;
	}

	if(id == g_ImgQ.pbgn)
	{ 
		return g_ImgQ.pbgn;
	}
	
	id = g_ImgQ.Img[g_ImgQ.pscr].Prev;

	g_ImgQ.pscr = id;
	return id;
}

/**
* 디렉토리 경로를 생성한다. --- 삭제 가능
*/
BOOL myUtil_CheckDir(CString sDirName)
{
	CFileFind file;
	CString strFile = "\\*.*";
	BOOL bResult = file.FindFile(sDirName + strFile);

	if(!bResult){
		bResult = CreateDirectory(sDirName+"\\",NULL);
		if(!bResult){
			/*Error*/
			return FALSE;
		}
		return TRUE;
	}  
	return FALSE;
}

/**
* TCS로부터 받은 데이터를 처리하고 응답데이터를 생성한다.  
*
*@param		buf		받은 데이터
*@param		size	buf 길이
*@return 	응답할 데이터
*/
MtPk* CTcsLink::OnTcpRx(unsigned char *buf, int size)
{
	int   lenCmd;
	int   cmd;
	int   id;
	DWORD serial;
	MtPk* pk = NULL;
	MtImg* img = NULL;
	TkData tk;
	WPARAM wParam;
	LPARAM lParam;
	int bProc = TRUE;

	memcpy(&lenCmd, buf, 4);
	if(lenCmd != (size-4))
	{
		return NULL;
	}
	memcpy(&serial, &buf[5], 4);

	cmd = buf[4];
	switch(cmd)
	{
	case INIT_PARAM : 
		pk = mkFrame(MT_CMD_INITACK, serial);
		memcpy(&m_ImgW, &buf[ 9], 4);
		if( (m_ImgW < 160) || (m_ImgW > 700)) 
		{
			m_ImgW = 484;
		}
		memcpy(&m_ImgH, &buf[13], 4);
		if( (m_ImgH < 120) || (m_ImgH > 500) ) 
		{
			m_ImgH = 364;
		}
		memcpy(&m_plW,  &buf[17], 4);
		if( (m_plW < 40) || (m_plW > 700) )
		{
			m_plW = 144;
		}
		memcpy(&m_plH,  &buf[21], 4);
		if( (m_plH < 20) || (m_plH > 500) )
		{
			m_plH = 84;
		}
		SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_INIT, 0);
		break;
		
	case WORK_START :
		pk = mkFrame(MT_CMD_ACK);
		break;
		
	case WORK_FINISH :
		//영상Q를 맨끝으로 이동 - 2개정도의 영상은 측면이라고 생각
		LPARAM lparam;
		for(int i=0; i < MT_MAX_IMGQ; i++)
		{
			g_ImgQ.pbgn = i;

			m_delFilename.Format("%s", g_ImgQ.Img[i].ImgInfo.srcFileName);
			m_delpltFilename.Format("%s",g_ImgQ.Img[i].ImgInfo.pltFileName);

			if(m_delFilename.IsEmpty() && m_delpltFilename.IsEmpty())
				continue;

			lparam = g_ImgQ.Img[i].bSndFTP;
			SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_DELFILE, lparam);
			FreeImgQ(&g_ImgQ.Img[i]);
			Sleep(10);
		}
		InitImgQ();

		AddLog("[근무 종료후 TCS 미처리영상 처리 완료]\n");

		pk = mkFrame(MT_CMD_ACK);
		break;
		
	case OFFICE_DATA :
		pk = mkFrame(MT_CMD_ACK);
		putTkData(&buf[9], &tk, cmd);

		//2012.11.07 - 로그로 기록을 남김
		wParam = (0x3600 | tk.extCode) | (tk.extSerial << 16);
		lParam = (tk.entICNum << 16) | (MAKEWORD(tk.extFtype, tk.extTtype));
		PostMessage(m_hWnd, WM_LOG_TKDATA, wParam, lParam);
		Sleep(1);

		if(g_ImgQ.pscr >= 0)
		{
			img = &g_ImgQ.Img[g_ImgQ.pscr];
		}
		
		if(g_ImgQ.pscr >= 0)
		{
			memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
		}

		ProcImg(img, &m_tkSnd);
		m_bNextPrevKey = FALSE;
		break;
		
	case EXIT_DATA :	// @@cjw - 출구특별처리데이터
		pk = mkFrame(MT_CMD_ACK);
		putTkData(&buf[9], &tk, cmd);

		//2012.11.07 - 로그로 기록을 남김
		wParam = (0x3700 | tk.extCode) | (tk.extSerial << 16);
		lParam = (tk.entICNum << 16) | (MAKEWORD(tk.extFtype, tk.extTtype));
		PostMessage(m_hWnd, WM_LOG_TKDATA, wParam, lParam);
		Sleep(1);

		if(g_ImgQ.pscr >= 0)
		{
			img = &g_ImgQ.Img[g_ImgQ.pscr];
		}
		
		// @@cjw - 20160502 서울영업소 차종변경 데이터 처리
		// 특처리의 확장코드에 대한 처리
		// bProc : ftp 전송 여부에 대한 변수
		// bProc가 TRUE : ProcImg 함수로 ftp 전송을 실행
		// 차변 후 면제, 긴급일 경우에는 면제와 긴급이 우선 (ex. 차변 후 면제는 면제로 처리)
		// 차변 후 기타 명령은 차변이 우선 (ex. 차변후 사업용 화물 처리는 차변으로 처리)
		switch(tk.extCode)
		{
		case CD_FREE :
		case OVERLAB_DATA_USE :
		case CD_URGENT :
			bProc = TRUE;
			if(img != NULL){
				 //2013.4.15 SIJ, 향후 다른 중복코드의 발생가능성에 대한 대비가 필요함
				if(m_SendDoubleCode){
					//차종변경과 중복되었을 경우, 차종변경을 먼저 전송함
					if(img->DoubleCode == CD_CAR_CHANGE)
					{	
						ProcImg(img, &m_tkSnd);
					}
					else
					{
						bProc = FALSE;
					}
					img->DoubleCode = tk.extCode;
				}
				memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
			}
			
			if(bProc){
				ProcImg(img, &m_tkSnd);
			}
			break;
		case CD_TOW :
			m_wrecker = TRUE;
		case CD_NEXT_PROC :
			if(img != NULL)
			{
				if(m_tkSnd.cmd == 0)
				{
					memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
				}
				img->DoubleCode = tk.extCode;	//2013.5.30 
			}
			break;
			
		case CD_CAR_CHANGE :	//2013.4.15 SIJ 차종변경은 따로 처리
			bProc = FALSE;
			if(img != NULL){
				//2013.4.15 SIJ, 향후 다른 중복코드의 발생가능성에 대한 대비가 필요함
				if(m_SendDoubleCode)
				{
					if(img->DoubleCode != 0)
					{
						ProcImg(img, &m_tkSnd);
						bProc = TRUE;
					}
					img->DoubleCode = tk.extCode;	//2013.5.30 SIJ
					memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
					if(bProc)
					{
						ProcImg(img, &m_tkSnd);
					}
				}
				else{
					memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
				}
			}
			break;
		////////////////////////////////
		case CD_COMPACT_TRUCK : 
		case CD_BUSINESS_TRUCK :
		case CD_EMPLOYEE_FREE_CARD :
			bProc = TRUE;
			if(img != NULL)
			{
				 //2013.4.15 SIJ, 향후 다른 중복코드의 발생가능성에 대한 대비가 필요함
				if(m_SendDoubleCode)
				{
					//차종변경과 중복되었을 경우, 차종변경을 먼저 전송함
					if(img->DoubleCode == CD_CAR_CHANGE)
					{	
						ProcImg(img, &m_tkSnd);
					}
					else
					{
						bProc = FALSE;
					}
					img->DoubleCode = tk.extCode;
					memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
				}
				else
				{
					if(m_tkSnd.cmd == 0)
						memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
					else
						bProc = FALSE;
				}
			}
			break;
		////////////////////////////////
		}	//switch(tk.extCode) end

		m_bNextPrevKey = FALSE;
		break;
		
	case STOP_LANE_DATA :
		pk = mkFrame(MT_CMD_ACK);
		putTkData(&buf[9], &tk, cmd);
		memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
		m_tkClosed = TRUE;		// 차로 운영하지 않음
		m_bNextPrevKey = FALSE;
		break;
		
	case TICKET_CONFIRM_DATA :
		bProc = TRUE;
		pk = mkFrame(MT_CMD_ACK);
		putTkData(&buf[9], &tk, cmd);

		//2012.11.07 - 로그로 기록을 남김
		wParam = (0x5700 | tk.extCode) | (tk.extSerial << 16);
		lParam = (tk.entICNum << 16) | (MAKEWORD(tk.extFtype, tk.extTtype));
		PostMessage(m_hWnd, WM_LOG_TKDATA, wParam, lParam);
		Sleep(0);

		if(g_ImgQ.pscr >= 0)
		{
			img = &g_ImgQ.Img[g_ImgQ.pscr];
		}
		tk.extExemp[15] = 2;
		if(m_tkSnd.cmd == 0)
		{
			memcpy(&m_tkSnd, &tk, SIZE_TKDATA);
		}
		
		if(img != NULL)
		{
			if(img->DoubleCode != 0)
			{
				img->DoubleCode = 0;
				if(img->bTrfd)
				{
					bProc = FALSE;
				}
			}
		}

		//2019.9.2
		AddLog("[TICKET_CONFIRM_DATA] scr id=%d, QID=%d, DoubleCode =%d, bTrf=%d\n", g_ImgQ.pscr, img->QNum, img->DoubleCode, img->bTrfd);

		if(bProc)
		{
			ProcImg(img, &m_tkSnd);
		}
		m_bNextPrevKey = FALSE;
		break;
		
	case STATUS_REQUEST :
		pk = mkFrame(MT_CMD_STS);
		SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_STSRES, 0);
		break;
		
	case PREVIOUS_DATA :
		id = MoveQrev(-1);
		if( (id >= 0) && (id < 16) ){
			pk = mkFrame(MT_CMD_SYNC, id);
		}
		m_bNextPrevKey = TRUE;
		break;
		
	case NEXT_DATA :
		id = MoveQfwd(-1);
		if( (id >= 0) && (id < 16) ){
			pk = mkFrame(MT_CMD_SYNC, id);
		}
		m_bNextPrevKey = TRUE;
		break;		

	case CAR_NUM_REQUEST:

		//CLinkTCSDlg m_tcsDlg;

		int car_cnt = 0;
		
		_finddata_t fd;
		long handle;
		int result = 1;
	#ifdef	PASS_TEST
		m_tcsDlg.m_passImgPath = "";
		m_tcsDlg.m_passBakImgPath = "";	
		m_tcsDlg.m_passImgPath = "D:\\TcsWork";
		m_tcsDlg.m_passBakImgPath = "D:\\TcsWorkBak";		
	#endif

		handle = _findfirst(g_pDlg->m_passImgPath + "\\*.jpg", &fd);
		// 파일이 존재하지 않음
		//2020.1.11 SIJ 파일이 존재할 때에만 실행
		if(handle != -1)
		{
			TRACE("g_pDlg->m_passImgPath:%s", g_pDlg->m_passImgPath); 

			//2020.1.11 더이상 필요없는 기능
			//myUtil_CheckDir(g_pDlg->m_passBakImgPath +"\\"+ createDate);

			while (result != -1)
			{ 
				//2019.08.26 CopyFile(g_pDlg->m_passImgPath + "\\" + fd.name, g_pDlg->m_passBakImgPath + "\\" + createDate + "\\" + fd.name, FALSE);
				try	//2020.1.11 삭제시에 에러가 발생할 경우를 대비
				{
					DeleteFile(g_pDlg->m_passImgPath + "\\" + fd.name); 
				}
				catch(...)
				{
					AddLog("[TCS통과대수] %s파일 삭제 에러\n", fd.name);
				}
				result = _findnext(handle, &fd);

				car_cnt++;
			}
		}
		_findclose(handle);

		pk = mkFrame(MT_CMD_CAR_NUM, 0, car_cnt);
		TRACE("car_cnt : %d", car_cnt);
		AddLog("[TCS로 통과대수 전송] %d\n", car_cnt);
		
		break;
	}

	if( cmd == OFFICE_DATA || cmd == EXIT_DATA || cmd == TICKET_CONFIRM_DATA)
	{
		if(++m_tcsSockCnt == 10)
		{
			m_tcsSockCnt = 11;
			Sleep(20);
			SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_CAMERA_ERR, 0);
		}
	}

	if(m_tcsSockErrCnt >= 10)
	{
		Sleep(20);
		SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_TCSSOCK, 0);
	}
	m_tcsSockErrCnt = 0;
	
	return pk;
}

/**
* 초기화를 수행한다 
*/
void CTcsLink::Init(HWND hWnd)
{
	m_hWnd = hWnd;
	InitImgQ();
}

/**
* 받은 TCS 데이터를 파싱하여 tk에 저장한다. 
*
*@param		buf			TCS에서 받은 데이터부
*@param		tk			파싱한 데이터
*@param		ctrlcode		명령어	
*/
void CTcsLink::putTkData(unsigned char *buf, TkData *tk, byte ctrlcode)
{
	int i;

	i = 0;
	tk->cmd			= ctrlcode;
	tk->entMon		= (char)BCDtoINT(&buf[i++], 2);		// 입구-월(2D)
	tk->entDay		= (char)BCDtoINT(&buf[i++], 2);		// 입구-일(2D)
	tk->entHour		= (char)BCDtoINT(&buf[i++], 2);		// 입구-시(2D)
	tk->entMin		= (char)BCDtoINT(&buf[i++], 2);		// 입구-분(2D)
	tk->entSerial	= (WORD)BCDtoINT(&buf[i], 4);		// 입구일련번호(4D)
	i+=2;
	tk->entICNum	= (WORD)BCDtoINT(&buf[i++], 3);		// 입구IC번호(3D)
	tk->entProper	= (char)buf[i++] & 0x0F;			// 입구-적격차구분(1D)
	tk->entLane		= (char)BCDtoINT(&buf[i++], 2);		// 입구-차로번호(2D)
	tk->entType		= (char)BCDtoINT(&buf[i], 1);		// 입구-예비권구분(1D)
	tk->entCntAxle	= (char)buf[i++] & 0x0F;			// 입구-축수코드(1D)

	tk->extYear		= (char)BCDtoINT(&buf[i++], 2);		// 출구-년(2D)
	tk->extMon		= (char)BCDtoINT(&buf[i++], 2);		// 출구-월(2D)
	tk->extDay		= (char)BCDtoINT(&buf[i++], 2);		// 출구-일(2D)
	tk->extHour		= (char)BCDtoINT(&buf[i++], 2);		// 출구-시(2D)
	tk->extMin		= (char)BCDtoINT(&buf[i++], 2);		// 출구-분(2D)
	tk->extWorkNum	= (WORD)BCDtoINT(&buf[i], 4);		// 출구-근무번호(4D)
	i+=2;
	m_workNum = tk->extWorkNum;		// cjw

	//BCD가 아닌 HEX로
	tk->extCode		= buf[i++];							// 출구-처리코드(2D)
	tk->extSerial	= (WORD)BCDtoINT(&buf[i], 4);		// 출구-일련번호(4D)
	i+=2;
	tk->extFare		= BCDtoINT(&buf[i], 6);				// 출구-통행요금(6D)
	i+=3;
	tk->extCash		= BCDtoINT(&buf[i], 6);				// 출구-수수할현금(6D)
	i+=3;
	tk->extICNum	= (WORD)BCDtoINT(&buf[i++], 3);		// 출구-출구IC번호(3D)
	tk->extCntAxle	= (char)buf[i++] & 0x0F;			// 출구-축수코드(1D)
	tk->extTtype	= (char)BCDtoINT(&buf[i], 1);		// 출구-통행권 차종(1D)
	if(tk->extTtype > 6)
	{
		tk->extFtype = 0;
	}
	tk->extFtype	= (char)buf[i++] & 0x0F;			// 출구-확정   차종(1D)
	if(tk->extFtype > 6)
	{
		tk->extFtype = 0;
	}
	//출구-할인카드번호(16D)
	tk->extDisc[ 0]	= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extDisc[ 1]	= 0x30 + (buf[i++] & 0x0F);
	tk->extDisc[ 2]	= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extDisc[ 3]	= 0x30 + (buf[i++] & 0x0F);
	tk->extDisc[ 4]	= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extDisc[ 5]	= 0x30 + (buf[i++] & 0x0F);
	tk->extDisc[ 6]	= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extDisc[ 7]	= 0x30 + (buf[i++] & 0x0F);
	tk->extDisc[ 8]	= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extDisc[ 9]	= 0x30 + (buf[i++] & 0x0F);
	if( (buf[i] == 0) && (buf[i+1] == 0) && (buf[i+2] == 0) )
	{
		tk->extDisc[10] = 0x30;
		tk->extDisc[11] = 0x30;
		tk->extDisc[12] = 0x30;
		tk->extDisc[13] = 0x30;
		tk->extDisc[14] = 0x30;
		tk->extDisc[15] = 0x30;
		i += 3;
	}
	else
	{
		tk->extDisc[10]	= 0x30 + ((buf[i] & 0xF0) >> 4);
		tk->extDisc[11]	= 0x30 + (buf[i++] & 0x0F);
		tk->extDisc[12]	= 0x30 + ((buf[i] & 0xF0) >> 4);
		tk->extDisc[13]	= 0x30 + (buf[i++] & 0x0F);
		tk->extDisc[14]	= 0x30 + ((buf[i] & 0xF0) >> 4);
		tk->extDisc[15]	= 0x30 + (buf[i++] & 0x0F);
	}

	//차종-면제카드번호(16D)
	tk->extExemp[ 0]= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extExemp[ 1]= 0x30 + (buf[i++] & 0x0F);
	tk->extExemp[ 2]= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extExemp[ 3]= 0x30 + (buf[i++] & 0x0F);
	tk->extExemp[ 4]= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extExemp[ 5]= 0x30 + (buf[i++] & 0x0F);
	tk->extExemp[ 6]= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extExemp[ 7]= 0x30 + (buf[i++] & 0x0F);
	tk->extExemp[ 8]= 0x30 + ((buf[i] & 0xF0) >> 4);
	tk->extExemp[ 9]= 0x30 + (buf[i++] & 0x0F);
	if( (buf[i] == 0) && (buf[i+1] == 0) && (buf[i+2] == 0) )
	{
		tk->extExemp[10]= 0x30;
		tk->extExemp[11]= 0x30;
		tk->extExemp[12]= 0x30;
		tk->extExemp[13]= 0x30;
		tk->extExemp[14]= 0x30;
		tk->extExemp[15]= 0x30;
		i += 3;
	}
	else
	{
		tk->extExemp[10]= 0x30 + ((buf[i] & 0xF0) >> 4);
		tk->extExemp[11]= 0x30 + (buf[i++] & 0x0F);
		tk->extExemp[12]= 0x30 + ((buf[i] & 0xF0) >> 4);
		tk->extExemp[13]= 0x30 + (buf[i++] & 0x0F);
		tk->extExemp[14]= 0x30 + ((buf[i] & 0xF0) >> 4);
		tk->extExemp[15]= 0x30 + (buf[i++] & 0x0F);
	}

	m_prevCarClass = tk->extFtype;
	return;
}

//2016.8.17 SIJ – 별도의 화면제어 알고리즘 없이, 번호판 인식된 영상아니면 맨끝으로 이동
MtImg * CTcsLink::selectImg(int bNew, int bNotAlg)
{
	int id;
	int bSelected = false;
	id = g_ImgQ.pscr;          // 2012.3.29
	int bLoop;

	// 필터링 알고리즘 추가필요
	if(g_ImgQ.pscr == -1)      // 처음시작시
	{
		id = 0;
		bSelected = true;
	}
	else
	{
		if(m_bNextPrevKey == TRUE)		// 이전영상/다음영상을 눌렀을 때
		{
			id = g_ImgQ.pscr;
			bSelected = true;
		}
		else
		{
			if(bNotAlg == TRUE)			// 알고리즘을 적용하지 않는 모드, 혼용차로 모드
			{
				id = g_ImgQ.pend;
				bSelected = true;
			}
		}
	}
	
	if(bSelected == false)
	{
		id = g_ImgQ.pscr;
		bLoop = TRUE;
		while( bLoop == TRUE )
		{
			if(id != g_ImgQ.pend)
			{
				if(g_ImgQ.Img[id].bTrfd == FALSE)
				{
					if( (g_ImgQ.Img[id].ImgInfo.bRecog == 2) || (g_ImgQ.Img[id].ImgInfo.bRecog == 1) )
					{
						bLoop = FALSE;
						break;
					}
					else
					{
						id = MoveQfwd(id);
					}
				}
				else
					id = MoveQfwd(id);
			}
			else
			{
				bLoop = FALSE;
				break;
			}

			if(id == -1)
				id = g_ImgQ.pend;

			if(id == g_ImgQ.pend)
			{
				bLoop = FALSE;
				break;
			}
		}          //while( (id != g_ImgQ.pend) && (id != -1) )
	}          //if(bSelected == false)

	//선택된 영상리턴
	if( (id >= 0) && (id <= 15) )
	{
		g_ImgQ.pscr = id;
		return &g_ImgQ.Img[id];
	}
	else
	{
		return &g_ImgQ.Img[g_ImgQ.pscr];
	}
}

LONG CTcsLink::BCDtoINT(unsigned char *buf, int size)
{
	int i,j;
	int d[8];
	int cnt;
	WORD valHi, valLo;
	LONG value;
	for(i=0; i < 8; i++)
	{
		d[i] = 0;
	}
	
	if(size > 8) cnt = 8;
	else cnt = size;
	for(i=(8-size),j=0; i < 8; i+=2,j++)
	{
		d[ i ] = (buf[j] & 0xF0) >> 4;
		if( (i+1) < 8)
		{
			d[i+1] =  buf[j] & 0x0F;
		}
	}
	
	valHi = (d[0]*1000) + (d[1]*100) + (d[2]*10) + d[3];
	valLo = (d[4]*1000) + (d[5]*100) + (d[6]*10) + d[7];
	value = valHi*10000 + valLo;

	return value;
}

/**
* 이미지 프레임을 생성한다. 
*
*@param		img		
*/
MtPk* CTcsLink::mkImgFrame(MtImg *img)
{
	unsigned char *buf;
	long lencmd;
	int i;
	CString strlog;

	buf = g_mtpk.buffer;

	if(img == NULL){
		return NULL;
	}

	if(img->ImgInfo.pImgSize > 0)
	{
		lencmd = 79 + img->ImgInfo.pImgSize + img->ImgInfo.ImgSize; 
	}
	else
	{
		lencmd = 79 + img->ImgInfo.ImgSize; 
	}
	g_mtpk.size = lencmd + 4;

	//Initialize
	memset(g_mtpk.buffer, 0, MAX_PACKET_SIZE);

	//Length
	memcpy(&buf[0], &lencmd, 4);

	//opcode
	buf[4] = 0x73;
	//serial
	memcpy(&buf[5], &img->QNum, 4);
	i = 9;
	//인식성공여부
	buf[i] = img->ImgInfo.bRecog;
	i++;
	//번호판 인식 내용 - Dummy
	memcpy(&buf[i], img->ImgInfo.strPlate, 20);
	i += 20;
	//원본영상크기 - width
	memcpy(&buf[i], &img->ImgInfo.ImgW, 4);
	i += 4;
	//원본영상크기 - Height
	memcpy(&buf[i], &img->ImgInfo.ImgH, 4);
	i += 4;
	//번호판인식의 좌표 - x1, y1, x2, y2
	i += 16;
	//번호판 영상데이터 - 크기
	memcpy(&buf[i], &img->ImgInfo.pImgSize, 4);
	i += 4;
	//번호판영상데이터 - 영상(NULL)
	if(img->ImgInfo.pImgSize > 0)
	{
		memcpy(&buf[i], img->pBuf, img->ImgInfo.pImgSize);
		i += img->ImgInfo.pImgSize;
	}

	//차량영상데이터 - 크기
	memcpy(&buf[i], &img->ImgInfo.ImgSize, 4);
	i += 4;
	//차량영상데이터
	memcpy(&buf[i], img->cBuf, img->ImgInfo.ImgSize);
	i += img->ImgInfo.ImgSize;
	// B/L존재여부
	buf[i] = img->ImgInfo.BLtype;
	i++;
	// B/L 스트링
	memcpy(&buf[i], img->ImgInfo.strBL, 20);
	i += 20;

/*	strlog.Format("[면탈->LCL] 영상데이터 BL(PL) 인덱스=%d, %02X %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
				i-21, buf[i-21], buf[i-20],buf[i-19],buf[i-18],buf[i-17],buf[i-16],buf[i-15],buf[i-14],buf[i-13],buf[i-12],buf[i-11],
				buf[i-10],buf[i-9],buf[i-8],buf[i-7],buf[i-6],buf[i-5],buf[i-4],buf[i-3],buf[i-2],buf[i-1]);
	AddLog(strlog); */

	return &g_mtpk;
}

/**
* 패킷 프레임을 생성한다.
*
*@param		cmd			명령어
*@param		ImgNum		선택 이미지 번호, 기본값 -1
*/

MtPk* CTcsLink::mkFrame(ULONG cmd, int ImgNum, int Data)
{
	ULONG lenCmd;
	ULONG serial;
	unsigned char *buf;

	if( (cmd == MT_CMD_SYNC) && (m_ImgMatchAlg != 0) )
	{
		return NULL;
	}

	lenCmd = 5;
	if(cmd == MT_CMD_STS)
	{
		lenCmd = 6;
	}
	else if(cmd == MT_CMD_CAR_NUM)
	{
		lenCmd = 9;
	}

	if(cmd == MT_CMD_INITACK)
	{
		m_ImgMatchAlg = 0;
		serial = 0x01010101;
	}
	else if(ImgNum > 15) 
	{
		serial = 0;
	}
	else
	{
		serial = ImgNum;
	}

	g_mtpk.size = lenCmd + 4;
	buf = g_mtpk.buffer;

	memcpy(buf, &lenCmd, 4);
	buf[4] = (unsigned char)(cmd & 0x00FF);
	memcpy(&buf[5], &serial, 4);

	if(cmd == MT_CMD_STS) 
	{
		buf[9] = GetStatus();
	}
	else if(cmd == MT_CMD_CAR_NUM)
	{
		memcpy(&buf[9], &Data, 4);
	}
	
	return &g_mtpk;
}

/**
* 기기 상태를 확인한다. 지금은 무조건 정상(0) 반환
*/
unsigned char CTcsLink::GetStatus()
{
	//기기상태를 체크할 수 있는 코드를 추가
	return 0;
}

/**
* @@cjw - FTP 파일명을 설정한다. 
*
*@param		Img		FTP로 전송할 이미지 데이터
*@param		tk		TCS로부터 받은 데이터 구조체
*/

void CTcsLink::setFTPFileName(MtImg *Img, TkData *tk)
{
	char *buf;
	char ofsNum[5]   = {0};
	char tkNum[5]    = {0};
	char wkNum[5]    = {0};
	char strCode[3]  = {0};
	char bufdate[20] = {0};
	int i;

	memset(bufdate, 0, sizeof(bufdate));
	sprintf_s(bufdate, sizeof(bufdate), "%02d%02d%02d%02d%02d", tk->extYear, tk->extMon, tk->extDay, tk->extHour, tk->extMin);

	if(Img == NULL)
	{
		//빈 영상파일일 경우에는 어떻게?
		return;
	}
	else
	{
		memcpy(Img->ImgInfo.dstFileName, Img->ImgInfo.srcFileName, MT_MAXLEN_FILENAME);
		buf = Img->ImgInfo.dstFileName;

		sprintf_s(ofsNum, sizeof(ofsNum),  "%03d", tk->extICNum);
		sprintf_s(tkNum,  sizeof(tkNum),   "%04d", tk->extWorkNum);
		sprintf_s(wkNum,  sizeof(wkNum),   "%04d", tk->extSerial);
		sprintf_s(strCode,sizeof(strCode), "%02x", tk->cmd);
		
		//BL차량은 촬영되자마자 전송하고, TCS관련정보만 전송한다.
		memcpy( buf,     ofsNum, 3);
		memcpy(&buf[3],  tkNum, 4);
		memcpy(&buf[7],  bufdate, 10);
		memcpy(&buf[17], wkNum, 4);
		memcpy(&buf[21], strCode, 2);
		i = 23;

		//2013.4.15 , 차변과 면제등 특별처리코드가 중복될때, 모두 보내기 위함
		if(Img->DoubleCode == CD_CAR_CHANGE)
		{
			buf[i++] = 't';
			buf[i++] = 'm';
			buf[i++] = 'p';
		}

		buf[i++] = '.';
		buf[i++] = 'J';
		buf[i++] = 'P';
		buf[i++] = 'G';
		buf[i++] = '\0';
	}
}

/**
* 이미지를 처리한다.
*
*@param		Img		
*@param		tk		
*/

void CTcsLink::ProcImg(MtImg *Img, TkData *tk)
{
	int bSend = FALSE;
	MtImg *tmpImg;
	int tkSerial;
	int bLog = TRUE;
	int tkCode;

	tmpImg   = Img;
	tkSerial = m_ProcInfo.TkSerial;
	tkCode   = m_ProcInfo.code;

	m_ProcInfo.TkSerial = tk->extSerial;
	m_ProcInfo.code     = tk->extCode;
	//전송조건에 맞는지 조사
	//전송해야할 영상이 NULL일 때,
	if(tmpImg == NULL)
	{
		if(++m_loopcnt >= LOOP_RESET_COUNT)
		{
			m_loopcnt = 0;
			SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_UNMATCHED_TK, 0x0201);
			Sleep(50);
		}
		SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_EMPTY_TK, 0x0101);		// 로그파일에 기록하여 추적할 수 있도록 함
	}
	// 권의 일련번호가 맞지 않으면 무조건 전송
	else if(tk->extSerial != tkSerial)
	{
		//영상이 전송된 경우, 정상적이지 않은 경우
		if( tmpImg->bTrfd == TRUE )
		{
			//견인모드일때, 견인으로 인해 트리거가 발생되지 않은 경우
			if(m_wrecker == TRUE)
			{
				m_wrecker = FALSE;
				SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_EMPTY_TK, 0x0102);
			}
			else
			{
				//루프홀딩을 의심해 볼 수 있는 경우
				if((int)tmpImg->QNum == g_ImgQ.pend)
				{
					if(++m_loopcnt >= LOOP_RESET_COUNT)
					{
						m_loopcnt = 0;
						SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_UNMATCHED_TK, 0x0202);
						Sleep(50);
					}
					// @@cjw 20160823 - 광주 영업소 요청
					// 영상이 맞지 않아도 마지막 영상을 전송하게 함.
					// 영상 정보 초기화를 막기위해 SendMessage는 주석처리
					//SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_EMPTY_TK, 0x0103);
					bSend = TRUE;
				}
				else	//영상이 뒤에 더 남아 있는데도 굳이 이전영상키 등으로 처리된 영상을 재처리 하는 경우
				{
					bSend = TRUE;
				}
			}
		}
		//영상이 아직 전송되지 않은 정상적인 경우
		else	
		{
			//버퍼에 영상이 실제로 담겨있는 경우
			if(tmpImg->bUsed)	
			{
				m_loopcnt = 0;
				m_ProcInfo.bSended = FALSE;
				bSend = TRUE;
			}
			// 할당되지 않은 버퍼로 포인터가 넘어간 경우,
			// 즉, 큐의 이동에 오류가 발생한 경우
			else
			{		
				SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_EMPTY_TK, 0x0106);
			}
		}
	}
	//일련번호가 같아도 전송하지 않았으면 전송
	else
	{
		//전송되지 않은 정상영상
		if((m_ProcInfo.bSended == FALSE) && (tmpImg->bTrfd == FALSE))
		{	
			bSend = TRUE;
		}
		//처리코드가 다르면 전송 - 2013.5.10
		else
		{
			if(m_SendDoubleCode)
			{
				//일반통행권확인이 아닌 특별처리코드 일때
				if(tkCode != tk->extCode)
				{	
					bSend = TRUE;
				}
				else
				{
					bLog = FALSE;
				}
			}
			else
			{
				bLog = FALSE;
			}
		}
	}

	// @@cjw - 영상의 전송
	if (bSend == TRUE)
	{
		setFTPFileName(tmpImg, tk);

		m_ProcInfo.bSended = TRUE;
		tmpImg->bTrfd      = TRUE;
		tmpImg->bSndFTP    = TRUE;

		SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_FTPSEND, tmpImg->QNum);
	}
	else
	{
		SendMessage(m_hWnd, WM_MT_MSG, MT_WPARAM_FAIL_SELIMG, bLog);
	}
}

/**
* 파일이름에서 정보를 읽는다. 
*
*@param		info		
*/

void CTcsLink::ReadInfoFromFileName(MtImgInfo *info)
{
	char tmpstr[256], strMnt[10], strCnt[10];
	memset(tmpstr, 0, 256);
	memset(strMnt, 0, 10);
	memset(strCnt, 0, 10);
	int i;
	char *pdest;
	CString str;
	char *strbuf;
	int lenPlate;	//2013.9.1 

	pdest = strchr(info->srcFileName, '.');
	i = (int)(pdest - info->srcFileName + 1);

	if(i < 30) return;
	memcpy(tmpstr, &info->srcFileName[23], i-24);

	//2013.9.1 'O' 추가
	if( (tmpstr[5] == 'X') || (tmpstr[5] == 'x') || (tmpstr[5] == ' ') ||
		(tmpstr[5] == 'O') || (tmpstr[5] == 'o'))
	{
		info->bRecog = RECOG_FAIL;
	}
	else
	{
		info->bRecog = RECOG_OK;
		//memset(info->strPlate, 0x20, sizeof(info->strPlate));
		memset(info->strPlate, 0x00, sizeof(info->strPlate));
		memcpy(info->strPlate, &info->srcFileName[23], i-24);
		str.Format("%s", info->strPlate);
		str.Trim();
		strbuf = str.GetBuffer(256);
		//memset(info->strPlate, 0x20, sizeof(info->strPlate));
		memset(info->strPlate, 0x00, sizeof(info->strPlate));
		lenPlate = str.GetLength();
		memcpy(info->strPlate, strbuf, lenPlate);
		str.ReleaseBuffer();

		i = lenPlate - 8;	//2013.9.1 용도부 숫자 위치
		info->bTow = 0;		//2013.9.1 견인차량인지에 대한 여부

		//용도부숫자가 98,99이면 견인
		if(info->strPlate[i] == '9')
		{
			if( (info->strPlate[i+1] == '8') || (info->strPlate[i+1] == '9') )
				info->bTow = 1;
		}
	}
}

/**
* 주제어기에 전송하는 버퍼를 초기화한다. 
*/
void CTcsLink::FreeTkSndData()
{
	memset(&m_tkSnd, 0, SIZE_TKDATA);
}

/**
* 근무번호를 리턴한다.
*/
WORD CTcsLink::GetTcsWorkNum()
{
	if(m_workNum == 0)
		m_workNum = m_NumLane * 100;

	return m_workNum;
}

void CTcsLink::AddLog(LPCTSTR format, ...)
{
	if(g_pDlg == NULL)
		return;

	va_list args;
	va_start(args, format);
	
	CString log;

	int len = _vsnprintf_s(log.GetBuffer(1000), 1000, 1000, (const char *)format, args);
	g_pDlg->AddLog(log);
	log.ReleaseBuffer(len);
}