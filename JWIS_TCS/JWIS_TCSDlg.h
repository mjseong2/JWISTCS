
// JWIS_TCSDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "ServerAgentSocket.h"
#include "TcsLink.h"
#include "MtSvrConSock.h"
#include "SetupDlg.h"

#define WM_TIMER_INIT				1
#define WM_TIMER_STS				2
#define WM_TIMER_SYNCIMG			3
#define WM_TIMER_MTSVR_PING			4
#define WM_TIMER_MTSVR_CONNECT		5
#define WM_SVR_SOCK_CHECK			6			// 서버소켓접속 여부 체크
#define WM_TIMER_INIT_LCL			7			// 차선제어기 초기화
#define WM_TIMER_DBCONNECT			8
#define WM_TIMER_SEND_STATUS		9
#define WM_TIMER_WDT				11

// CJWIS_TCSDlg 대화 상자
class CJWIS_TCSDlg : public CDialogEx
{
// 생성입니다.
public:
	CJWIS_TCSDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

	//enum { IDD = IDD_JWIS_TCS_DIALOG };
	//----------- 추가 부분 ---------------------------------
	CString		m_TowCarName;			// 견인차량일 경우, 영상파일 이름
	int			m_bTowCar;				// 견인차량인지의 여부
	int			m_MixedHipass;			// 하이패스 혼용차로 여부
	int			m_stsODBC;				// ODBC의 연결상태
	int			m_bShowLogTime;

	CSetupDlg		m_dlgSetup;
	CMtSvrConSock	m_mtSvrSock;		// m_MtSvrCSock
	
	void SetFtpBuffer(MtImg *mtimg, TkData *tk);
	void SetFTPGUIStatus(int status);
	void SetODBCGUIStatus(int status);
	void SetSVRGUIStatus(int status);
	void SetTCSCGUIStatus(int status);
	void SendStatus();

	CServerSock*	m_SvrSock;
	int				m_SvrPort;
	unsigned short	m_lclPort;
	int			m_svrStsCnt;			// 서버접속여부 이상 카운터
	int			m_LightErrorCnt;		// 조명이상을 밝혀낼 변수
	char		m_rcvbuf[40960];
	int			m_total;
	FILE*		m_ftpFile;
	int			m_ftpfileSize;
	HANDLE 		m_ftpEvent;
	HANDLE		m_svrConnEvent;
	HANDLE		m_jpgEvent;

	CString		m_ftpAddr;
	CString		m_ftpUser;
	CString		m_ftpPass;
	CString		m_DBUser;
	CString		m_DBPass;

	CString		m_MyEtherName;			// 제어기 자체 IP
	CString		m_MyIPAddr;				// 제어기 자체 IP
	CString		m_MySubnet;				// 제어기 자체 서브넷마스크
	
	int			m_gateType;
	int			m_AllTkData;			// 2013.3.21 개방식에서 일반통행권확인데이터(0x57)을 전송
	int			m_SendDoubleCode;		// 2013.4.15 
	int			m_BLtype;
	int			m_BLSearchTime;
	CString		m_BLData[2];
	CString		m_BLFileName;
	CMtFtpSock	m_ftp;
	CString		m_emptyImageName;
	char		m_TitleRecogSW[256];
	DWORD		m_LoopRstTickCnt;

	void SendToRecogSW();
	void SendToTowTrigger();
	CxImage::CXTEXTINFO		m_BLtext;
	CxImage::CXTEXTINFO		m_RLtext;
	CxImage::CXTEXTINFO		m_PLtext;
	CxImage::CXTEXTINFO		m_RFtext;		// 2012.11.20 SIJ -환불메세지
	CxImage::CXTEXTINFO		m_NightDCtext;	// 2015.05.16 @@cjw - 심야할인

	int			m_AckMode;
	void AddLog(LPCTSTR str, int showtime = LOG_TIME);
	int			m_cnt_sts;
	void WriteINI();
	CTcsLink	m_tcsLink;
	int			m_cImgW;
	int			m_cImgH;
	int			m_pImgW;
	int			m_pImgH;

	CString		m_tagPath;
	CString		m_pltImgPath;			// 영상처리기 차량 번호판 생성 경로
	CString		m_carImgPath;			// 영상처리기 차량 이미지 생성 경로
	CString		m_svrImgPath;			// 서버전송용 파일 경로
	CString		m_tmpImgPath;			// 임시경로(TCS 전송용)
	CString		m_bakImgPath;			// 백업경로
	CString		m_svrImgPathCopy;		// YJW 서버전송용 파일 복사
	CString		m_LogPath;				// 로그경로
	CString		m_passImgPath;			// 면탈촬영통과대수 저장 경로
	CString		m_passBakImgPath;		// 면탈촬영통과대수 백업 경로

	int			m_NumCamera;
	int			m_NumLane;
	int			m_NumOffice;

	CxImage		m_cxCimg;				// 차량영상 이미지
	CxImage		m_cxPimg;				// 번호판영상 이미지
	CString		m_strCimg;				// 차량영상 이미지파일명
	CString		m_strPimg;				// 번호판영상 이미지파일명

	CWinThread* m_ImgThread;
	CWinThread* m_ftpThread;
	CWinThread* m_DBThread;

	HANDLE		m_logEvent;
	int			m_BackupFileErasing;

	unsigned short m_ftpPort;
	//----------- 여기까지 추가 부분 ------------------------

// 대화 상자 데이터입니다.
	enum { IDD = IDD_JWIS_TCS_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

public:
	void Read_Ini(void);		// INI 파일 내용 읽기
	//void CreateDir(TCHAR* Path);
// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit_tcsConn;
	CEdit m_edit_svrConn;
	CEdit m_edit_ftpStatus;
	CEdit m_edit_odbcConn;
	CStatic m_static_Version;
	CButton m_chk_prtLog;
	CButton m_chk_prtTCS;
	CButton m_chk_saveLog;
	CStatic m_static_env1;
	CStatic m_static_env2;
	CRichEditCtrl m_redit_Log;

	int		m_odbc_conn;
	int		m_tcsconn;
	int		m_svrconn;
	int		m_FTP_Status;

	int     m_TakeBackImg;			// 후면촬영테스트 기능
	CString m_BackImgName;			// 후면촬영 파일 이름
	int     m_extWMV;				// 동영상 파일인지 지정

	void BackupFiles(CString srcPath, CString dstPath, int EraseSec);
	void InitLCLPort();
	// 제어기 자체의 IP를 설정 (netsh명령 이용)
	void SetTCPIPAddress(CString EtherName, CString IPAddr, CString SubnetMask);
	
	afx_msg void OnClose();
protected:
	afx_msg LRESULT OnNewImage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMtMsg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSockAccept(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMtSvrSock(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFtpAccept(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFTPDataSock(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLogTkData(WPARAM wParam, LPARAM lParam);
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBtnWriteIni();
	afx_msg void OnBnClickedBtnInitq();
	afx_msg void OnBnClickedBtnSaveLog();
	afx_msg void OnBnClickedBtnEraseBackup();
	afx_msg void OnBnClickedBtnTowTest();
	afx_msg void OnBnClickedBtnBacktrigger();
	afx_msg void OnBnClickedBtnExit();
};

extern CJWIS_TCSDlg *g_pDlg;
