// ServerAgentSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JWIS_TCS.h"
#include "ServerAgentSocket.h"

extern HANDLE g_TCSEvent;

// CServerAgentSocket

CServerAgentSocket::CServerAgentSocket()
{
	m_bConnected = FALSE;
	m_bufLen = 0;
	MAX_MT_RX_LEN = (SIZE_TKDATA + 9) * 2;		// 2012.3.2 
	m_sndbuf = NULL;
	m_sndsize = 0;
	CAsyncSocket::CAsyncSocket();
}

CServerAgentSocket::~CServerAgentSocket()
{
	
}

// CServerAgentSocket 멤버 함수

void CServerAgentSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_bufLen = 0;
	m_bConnected = FALSE;
	SendMessage(m_Hwnd, WM_STS_REQ, WM_WPARAM_SOCKCL, 0);
	//CAsyncSocket::OnClose(nErrorCode);
}

void CServerAgentSocket::Init(int Connected)
{
	m_bufLen	= 0;
	m_bConnected = Connected;
	m_sndsize	= 0;
	m_sndbuf	= NULL;
}

/**
* TCS로부터 응답데이터를 받는 이벤트이다. 
*
*@param		nErrorCode		오류 코드
*/
void CServerAgentSocket::OnReceive(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	int		rxLen = 0;
	MtPk*	mtpk = NULL;
	ULONG	cmdLen = 0;
	int		pkLen = 0;
	BYTE	opcode = 0xFF;
	LPARAM	lparam;
	int		sndsize = 0;
	int		serial = 0xFF;			//2012.06.21 

	rxLen = Receive(&m_pBuf[m_bufLen], MTSOCK_MAX_BUFFER-m_bufLen);
	m_bufLen += rxLen;

	while(m_bufLen >= 4)
	{
		memcpy(&cmdLen, m_pBuf, 4);
		pkLen = cmdLen + 4;

		//2013.5.29 
		//최소 길이는 5이상 (ACK인 경우)
		if( (cmdLen < 5) || (cmdLen > (SIZE_TKDATA+1) ) )
		{
			pkLen = -1;
		}
		else
		{
			//패킷처음에 있는 데이터가 이상하면 클리어
			if( (pkLen > MAX_MT_RX_LEN) || (pkLen <= 0) )
			{		
				pkLen = -1;
			}
		}

		if(pkLen < 0)
		{
			// 2013.5.30 
			lparam = rxLen | ((serial << 20) & 0x00F00000) | ((opcode << 24) & 0xFF000000);	//2012.06.21 영상번호 추가
			PostMessage(m_Hwnd, WM_STS_REQ, WM_WPARAM_DATRCV, lparam);

			m_bufLen = 0;
			while(rxLen > 0)
			{
				rxLen = Receive(&m_pBuf[0], MTSOCK_MAX_BUFFER);
				Sleep(10);		// 2013.5.29 
			}
			return;
		}

		// 받아야할 양보다 더 온 경우
		if(pkLen < m_bufLen)
		{
			m_bufLen = m_bufLen - pkLen;
			memcpy(m_packet, m_pBuf, pkLen);
			memcpy(m_pBuf,  &m_pBuf[pkLen], m_bufLen);
		}
		// 딱맞게 온 경우
		else if(pkLen == m_bufLen)	
		{
			memcpy(m_packet, m_pBuf, pkLen);
			m_bufLen = 0;
		}
		// 부족한 경우
		else	
		{
			//2013.5.30 
			lparam = rxLen | ((serial << 20) & 0x00F00000) | ((opcode << 24) & 0xFF000000);	//2012.06.21 영상번호 추가
			PostMessage(m_Hwnd, WM_STS_REQ, WM_WPARAM_DATRCV, lparam);
			return;
		}

		mtpk = m_ptcsLnk->OnTcpRx(m_packet, pkLen);
		Sleep(1);
		if(mtpk != NULL)
		{
			sndsize = SendData(mtpk->buffer, mtpk->size); //2012.1.14 build.51 수정, 리턴값 받도록
			//2012.1.14 build.51 수정
			if(sndsize < mtpk->size)
			{	
				pkLen = 0x000FFFFF;
			}
			else
			{
				serial = mtpk->buffer[5];
			}
		}
		opcode = m_packet[4];
		lparam = pkLen | ((serial << 20) & 0x00F00000) | ((opcode << 24) & 0xFF000000);	//2012.06.21 SIJ 영상번호 추가
		PostMessage(m_Hwnd, WM_STS_REQ, WM_WPARAM_DATRCV, lparam);

		Sleep(0);
	}

	//CAsyncSocket::OnReceive(nErrorCode);
}

/**
* TCS 에 데이터를 전송한다. 
*
*@param		buf			전송할 데이터
*@param		bufsize		buf 크기
*@return	전송한 데이터 수
*/
int CServerAgentSocket::SendData(unsigned char *buf, int bufsize)
{
	int		sndsize;
	int		total = 0;
	int		i = 0;
	int		rest = bufsize;
	int		sndErr = 0;
	DWORD	wResult;

	//2012.1.14 build.51 추가
	if(m_bConnected == FALSE) {
		return 0;
	}

	wResult = WAIT_FAILED;
	i = 0;
	while( (wResult != WAIT_OBJECT_0) && (i < 50) )
	{
		wResult = WaitForSingleObject(g_TCSEvent, 10);
		if(wResult == WAIT_OBJECT_0){
			break;
		}
		i++;
	}
	ResetEvent(g_TCSEvent);
	Sleep(50);		//2013.5.10 SIJ

	total = 0;
	sndErr = 0;
	i = 0;
	rest = bufsize;
	while( (total < bufsize) && (i < 500) )
	{
		i++;

		sndsize = Send(&buf[total], rest);
		if(sndsize > 0) {
			total += sndsize;
			rest -= sndsize;
			sndErr = 0;
		}
		else{
			sndErr++;
			//2012.1.14 build.51  1->4
			if(sndErr > 4) {
				i = 500;
				break;
			}
			else	{
				Sleep(10);
			}
		}
	}

	Sleep(50);

	m_sndbuf  = buf;
	m_sndsize = bufsize;

#ifdef PASS_TEST
	TRACE("시작");
	for(int i = 0; i < bufsize; i++){
		TRACE("%02x", buf[i]);
	}

#endif

	SetEvent(g_TCSEvent);
	return total;
}

/**
* 데이터를 재전송한다. 
*/
void CServerAgentSocket::reSend()
{
	if((m_sndbuf != NULL) && (m_sndsize > 0))
	{
		SendData(m_sndbuf, m_sndsize);
	}
}


//=============================== CServerSock =================================
//
//

CServerSock::CServerSock()
{
	CAsyncSocket::CAsyncSocket();
}

CServerSock::~CServerSock()
{
	delete m_pAgentSock;
}

void CServerSock::Init(HWND hwnd)
{
	m_Hwnd = hwnd;

	m_pAgentSock = new CServerAgentSocket;
	m_pAgentSock->m_Hwnd = hwnd;

}

void CServerSock::OnAccept(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	UINT port;

	if( nErrorCode > 0 ){
		AfxMessageBox("연결 Accept 오류",MB_OK,NULL);
		return;
	}
	
	if(m_pAgentSock->m_hSocket != INVALID_SOCKET)
		m_pAgentSock->Close();
	Accept(*m_pAgentSock);
	m_pAgentSock->GetPeerName( m_pAgentSock->m_youAddress, port );
	m_pAgentSock->Init(TRUE);
	SendMessage(m_Hwnd, WM_STS_REQ, WM_WPARAM_ACCEPT, 0);

	CAsyncSocket::OnAccept(nErrorCode);
}


void CServerSock::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnClose(nErrorCode);
}

BOOL CServerSock::Listen( int port, int nConnectionBacklog )
{
	m_port = port;
	Create( m_port );
	return CAsyncSocket::Listen(nConnectionBacklog);
}
