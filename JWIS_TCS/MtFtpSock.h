#pragma once

#define RX_MAX_BUFFER 512

class CMtFtpSock
{
public:
	CMtFtpSock(void);
	virtual ~CMtFtpSock(void);

	int		m_connected;
	SOCKET	m_sock;
	SOCKET	m_TransListenSock;
	SOCKET	m_TransmitSock;

	HANDLE	m_hListenEvent;
	HANDLE	m_hAcceptEvent;
	HANDLE	m_hRcvDoneEvent;
	
	unsigned short m_listenPort;

	WORD GetFile(LPCTSTR filename, LPCTSTR path);
	void FtpDisconnect();
	WORD UploadBuffer(LPCTSTR filename, char *buf, int bufsize, int gateType);
	int rcvData(int timeout);
	int GetRcvCode(char *buf, int *subcode=NULL);
	BOOL FtpConnect(LPCTSTR svrip, LPCTSTR user, LPCTSTR pass, void *Dlg);
	SOCKET CreateListenSocket(HWND hWnd, u_int msg);
	void SetListenPort(unsigned short port);
	virtual void OnClose( int nErrorCode );

protected:
	char m_rxbuf[RX_MAX_BUFFER];
	int  m_rxLen;
};

