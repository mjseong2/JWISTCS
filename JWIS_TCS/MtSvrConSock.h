#pragma once
#include "MtFtpSock.h"

class CMtSvrConSock
{
public:
	CMtSvrConSock(void);
	virtual ~CMtSvrConSock(void);

	int		m_connected;
	SOCKET	m_sock;
	HWND	m_Hwnd;

	BOOL Connect(LPCTSTR svrip, int port);
	//BOOL ConnectCopy(LPCTSTR svrip, int port);
	void Disconnect();
	bool sendString(LPCTSTR str);
	void SetWnd(HWND hwnd);

protected:
	char m_rxbuf[RX_MAX_BUFFER];
	int  m_rxLen;
};

