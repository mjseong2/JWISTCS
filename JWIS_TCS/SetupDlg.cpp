// SetupDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "JWIS_TCS.h"
#include "SetupDlg.h"
#include "afxdialogex.h"

// CSetupDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSetupDlg, CDialog)

CSetupDlg::CSetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSetupDlg::IDD, pParent)
{

}

CSetupDlg::~CSetupDlg()
{
}

void CSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHK_MIXED_HIPASS, m_chk_mixedHipass);
	DDX_Control(pDX, IDC_CHK_ALL_TKDATA, m_chk_allTkData);
	DDX_Control(pDX, IDC_EDIT_OFFICE_NUM, m_edit_officeNum);
	DDX_Control(pDX, IDC_EDIT_CAMERA_NUM, m_edit_cameraNum);
	DDX_Control(pDX, IDC_EDIT_LANE_NUM, m_edit_laneNum);
	DDX_Control(pDX, IDC_EDIT_MYIPADDR, m_edit_myIPAddr);
	DDX_Control(pDX, IDC_EDIT_MYSUBNETMASK, m_edit_mySubnetMask);
	DDX_Control(pDX, IDC_EDIT_FTPADDR, m_edit_ftpAddr);
	DDX_Control(pDX, IDC_EDIT_DBUSER, m_edit_odbcUser);
	DDX_Control(pDX, IDC_EDIT_DBPASS, m_edit_odbcPass);
	DDX_Control(pDX, IDC_CHK_MIXED_SEND_CHANGE_CARTYPE, m_chk_sendDoubleCode);
	DDX_Control(pDX, IDC_EDIT_IMGPATH, m_edit_carPath);
	DDX_Control(pDX, IDC_EDIT_IMGPATH3, m_edit_pltPath);
	DDX_Control(pDX, IDC_EDIT_TAGPATH, m_edit_tagPath);
	DDX_Control(pDX, IDC_EDIT_IMGPATH4, m_edit_tmpPath);
	DDX_Control(pDX, IDC_EDIT_IMGPATH2, m_edit_svrPath);
	DDX_Control(pDX, IDC_EDIT_IMGPATH5, m_edit_bakPath);
	DDX_Control(pDX, IDC_EDIT_IMGPATH6, m_edit_logPath);
	DDX_Control(pDX, IDC_EDIT_SVRCONPORT, m_edit_svrPort);
	DDX_Control(pDX, IDC_EDIT_LCL_PORT2, m_edit_lclPort);
	DDX_Control(pDX, IDC_EDIT_FTPUSER, m_edit_ftpUser);
	DDX_Control(pDX, IDC_EDIT_FTPPASS, m_edit_ftpPass);
	DDX_Control(pDX, IDC_EDIT_FTPPORT, m_edit_ftpPort);
	DDX_Control(pDX, IDC_RADIO_TYPE_OUT, m_radio_gateType_Out);
	DDX_Control(pDX, IDC_RADIO_TYPE_UP, m_radio_gateType_Up);
	DDX_Control(pDX, IDC_RADIO_TYPE_DOWN, m_radio_gateType_Down);
}


BEGIN_MESSAGE_MAP(CSetupDlg, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDOK, &CSetupDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSetupDlg 메시지 처리기입니다.


void CSetupDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CString str;
	m_edit_carPath.SetWindowText(m_carImgPath);
	m_edit_pltPath.SetWindowText(m_pltImgPath);
	m_edit_tagPath.SetWindowText(m_tagPath);
	m_edit_tmpPath.SetWindowText(m_tmpImgPath);	// TmpImg
	m_edit_svrPath.SetWindowText(m_svrImgPath);
	m_edit_bakPath.SetWindowText(m_bakImgPath);
	m_edit_logPath.SetWindowText(m_LogPath);
	//2012.7.12 SIJ---------------------------
	m_edit_myIPAddr.SetWindowText(m_MyIPAddr);
	m_edit_mySubnetMask.SetWindowText(m_MySubnet);
	//----------------------------------------
	m_edit_ftpAddr.SetWindowText(m_ftpAddr);
	m_edit_ftpUser.SetWindowText(m_ftpUser);
	m_edit_ftpPass.SetWindowText(m_ftpPass);
	str.Format("%d", m_ftpPort);
	m_edit_ftpPort.SetWindowText(str);
	str.Format("%d", m_SvrPort);
	m_edit_svrPort.SetWindowText(str);
	str.Format("%d", m_lclPort);
	m_edit_lclPort.SetWindowText(str);
	str.Format("%d", m_NumCamera);
	m_edit_cameraNum.SetWindowText(str);
	str.Format("%d", m_NumLane);
	m_edit_laneNum.SetWindowText(str);
	str.Format("%d", m_NumOffice);
	m_edit_officeNum.SetWindowText(str);
	m_edit_odbcUser.SetWindowText(m_DBUser);
	m_edit_odbcPass.SetWindowText(m_DBPass);

	// 차선의 운영형태 (0=폐쇄식출구, 1=개방식상행, 2=개방식하행)
	switch(m_gateType)
	{
	case 0 : 
		m_radio_gateType_Out.SetCheck(1);
		break;
	case 1 : 
		m_radio_gateType_Up.SetCheck(1);
		break;
	case 2 : 
		m_radio_gateType_Down.SetCheck(1);
		break;
	}
	m_chk_mixedHipass.SetCheck(m_MixedHipass);

	//2013.3.21 SIJ 개방식차로에서 일반통행권데이터(0x57)의 전송여부
	m_chk_allTkData.SetCheck(m_AllTkData);

	m_chk_sendDoubleCode.SetCheck(m_SendDoubleCode);
}


void CSetupDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;

	m_edit_carPath.GetWindowText(m_carImgPath);
	m_edit_pltPath.GetWindowText(m_pltImgPath);
	m_edit_tagPath.GetWindowText(m_tagPath);
	m_edit_svrPath.GetWindowText(m_svrImgPath);
	m_edit_tmpPath.GetWindowText(m_tmpImgPath);
	m_edit_bakPath.GetWindowText(m_bakImgPath);
	m_edit_logPath.GetWindowText(m_LogPath);
	m_edit_ftpAddr.GetWindowText(m_ftpAddr);
	m_edit_ftpUser.GetWindowText(m_ftpUser);
	m_edit_ftpPass.GetWindowText(m_ftpPass);
	//2012.7.12 SIJ -----------------------------
	m_edit_myIPAddr.GetWindowText(m_MyIPAddr);
	m_edit_mySubnetMask.GetWindowText(m_MySubnet);
	//-------------------------------------------

	m_edit_svrPort.GetWindowText(str);
	m_SvrPort = atoi(str);

	m_edit_lclPort.GetWindowText(str);
	m_lclPort = atoi(str);

	m_edit_ftpPort.GetWindowText(str);
	m_ftpPort = atoi(str);	

	m_edit_cameraNum.GetWindowText(str);
	m_NumCamera = atoi(str);

	m_edit_laneNum.GetWindowText(str);
	m_NumLane = atoi(str);

	m_edit_officeNum.GetWindowText(str);
	m_NumOffice = atoi(str);

	m_edit_odbcUser.GetWindowText(m_DBUser);
	m_edit_odbcPass.GetWindowText(m_DBPass);

	// 차선의 운영형태 (0=폐쇄식출구, 1=개방식상행, 2=개방식하행)
	if(m_radio_gateType_Out.GetCheck() == 1)
		m_gateType = 0;
	else if(m_radio_gateType_Up.GetCheck() == 1)
		m_gateType = 1;
	else if(m_radio_gateType_Down.GetCheck() == 1)
		m_gateType = 2;

	m_MixedHipass = m_chk_mixedHipass.GetCheck();

	m_AllTkData   = m_chk_allTkData.GetCheck();

	m_SendDoubleCode = m_chk_sendDoubleCode.GetCheck();

	CDialog::OnOK();
}
