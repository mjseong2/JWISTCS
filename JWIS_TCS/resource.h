//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by JWIS_TCS.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDS_ABOUTBOX                    101
#define IDD_JWIS_TCS_DIALOG             102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_DLG_SETUP                   129
#define IDC_EDIT_TCS_CONN               1000
#define IDC_EDIT_SVR_CONN               1001
#define IDC_EDIT_FTP_CONN               1002
#define IDC_EDIT_ODBC_CONN              1003
#define IDC_CHK_PRINT_LOG               1004
#define IDC_CHK_PRINT_TCS               1005
#define IDC_CHK_SAVE_LOG                1006
#define IDC_RICHEDIT                    1007
#define IDC_BTN_TOW_TEST                1008
#define IDC_BTN_BACKTRIGGER             1009
#define IDC_BTN_INITQ                   1010
#define IDC_BTN_SAVE_LOG                1011
#define IDC_BTN_ERASE_BACKUP            1012
#define IDC_BTN_EXIT                    1013
#define IDC_BTN_WRITE_INI               1014
#define IDC_STATIC_VERSION              1015
#define IDC_STATIC_ENVIRONMENT1         1016
#define IDC_STATIC_ENVIRONMENT2         1017
#define IDC_RADIO_TYPE_OUT              1018
#define IDC_RADIO_TYPE_UP               1019
#define IDC_RADIO_TYPE_DOWN             1020
#define IDC_CHK_MIXED_HIPASS            1021
#define IDC_CHK_ALL_TKDATA              1022
#define IDC_EDIT_DBUSER                 1023
#define IDC_EDIT_DBPASS                 1024
#define IDC_CHK_MIXED_SEND_CHANGE_CARTYPE 1025
#define IDC_EDIT_IMGPATH                1026
#define IDC_EDIT_IMGPATH3               1027
#define IDC_EDIT_TAGPATH                1028
#define IDC_EDIT_IMGPATH4               1029
#define IDC_EDIT_IMGPATH2               1030
#define IDC_EDIT_IMGPATH5               1031
#define IDC_EDIT_IMGPATH6               1032
#define IDC_EDIT_SVRCONPORT             1033
#define IDC_EDIT_LCL_PORT2              1034
#define IDC_EDIT_FTPUSER                1035
#define IDC_EDIT_FTPPASS                1036
#define IDC_EDIT_FTPPORT                1037
#define IDC_EDIT_OFFICE_NUM             1038
#define IDC_EDIT_CAMERA_NUM             1039
#define IDC_EDIT_LANE_NUM               1040
#define IDC_EDIT_MYIPADDR               1041
#define IDC_EDIT_MYSUBNETMASK           1042
#define IDC_EDIT_FTPADDR                1043

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1039
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
