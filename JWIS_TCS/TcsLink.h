#pragma once

#define WM_MT_MSG					WM_USER + 0x1001		// 사용자 정의 메시지 
#define WM_LOG_TKDATA				WM_USER + 0x1002		// TCS데이터 로그출력메세지


#define MT_WPARAM_FTPSEND			0x101					// FTP 전송
#define MT_WPARAM_STSRES			0x102					// 시간을 체크하고 조건에 따라 백업파일 삭제
#define MT_WPARAM_DELFILE			0x103					// 파일 삭제
#define MT_WPARAM_INIT				0x104					// 초기화
#define MT_WPARAM_UNMATCHED_TK		0x105
#define MT_WPARAM_EMPTY_TK			0x106
#define MT_WPARAM_FAIL_SELIMG		0x107					// 이미지 선택 실패
#define MT_WPARAM_CAMERA_ERR		0x108					// 카메라 오류
#define MT_WPARAM_TCSSOCK			0x109

#define MT_STATUS_INTERVAL			5500					// 10% 시간오차 반영

#define MT_EMPTY_IMAGE_FILENAME		"00000000000000000000000XXXXXXXXXXXX.jpg"

#define MT_MAX_IMGQ					16
#define MT_MAXLEN_FILENAME			256
#define MT_MAXLEN_FILEPATH			128
#define MT_SIZE_MTPACKET 			4096000

//-------------------- IPU-LCL -------------------------------------------
#define MT_CMD_ACK					0x0006		// ACK
#define MT_CMD_NAK					0x0015		// NAK	
#define MT_CMD_STS					0x0072
#define MT_IMAGE					0x0073
#define MT_CMD_SYNC					0x0074		// 영상맞춤 전송
#define MT_CMD_INITACK				0x0106
#define MT_CMD_CAR_NUM				0x0078		// 차량 촬영대수 전송

#define SIZE_TKDATA					73
#define SIZE_IMGINFO				1099

//TCS 명령코드
#define ACK							0x06		// ACK
#define	NAK							0x15		// NAK	
#define	INIT_PARAM					0x30		// 초기화 parametera
#define	WORK_START					0x31		// 근무개시
#define	WORK_FINISH					0x32		// 근무종료
#define	OFFICE_DATA					0x36		// 사무실처리데이터
#define	EXIT_DATA					0x37		// 출구특별처리데이터
#define	STOP_LANE_DATA				0x38		// 폐쇄차선데이터
#define	TICKET_CONFIRM_DATA			0x57		// 통행권 확인 데이터
#define	OVERLAB_DATA_USE			0x58		// 중복영상 사용
#define	STATUS_REQUEST				0x71		// 상태확인요청
#define	STATUS_RESPONSE				0x72		// 상태확인종료
#define	PREVIOUS_DATA				0x75		// 전영상
#define	NEXT_DATA					0x76		// 후영상
#define	CAR_NUM_REQUEST				0x77		// 촬영대수 요청

// 출구특별 처리코드
#define	CD_FREE						0x30		// 면제
#define	CD_UTURN					0x31		// 유턴
#define	CD_URGENT					0x32		// 긴급
#define	CD_TOW						0x33		// 견인
#define	CD_CAR_CHANGE				0x34		// 차종변경
#define	CD_TIME						0x35		// 시간주의
#define	CD_NEXT_PROC				0x84		// 후처리
#define	CD_COMPACT_TRUCK			0x89		// 소형화물
#define	CD_BUSINESS_TRUCK			0x92		// 사업용화물
#define	CD_EMPLOYEE_FREE_CARD		0x94		// 직원면제카드

#define	RECOG_FAIL		0
#define	RECOG_AREA		1
#define	RECOG_OK		2

// BL 타입
#define	NOT_BL			0
#define	BL				1		// BL
#define	PL				2		// PL
#define	BL_PL			3		// BL + PL
#define	RL				4		// RL
#define	BL_RL			5		// BL + RL
#define	REFUND			6		// 환불차량

#define	CAR_TYPE_NO		0		// 차종 지정 안함
#define	CAR_TYPE_4		4		// 4종 차량
#define	CAR_TYPE_5		5		// 5종 차량

// 로그 표시 옵션
#define	LOG_TIME		0		// 시간 표시
#define	LOG_DAY			1		// 날짜 표시
#define	LOG_FILE		2		// 파일 저장
#define	LOG_STRING		3		// 문자만 표시


//2019.9.5 SIJ
#define MAX_PACKET_SIZE 2 * 1024 * 1024		// 2M Pixel


// TCS로부터 받은 데이터 구조체
typedef struct _TkData
{
	byte		cmd;				// 제어코드
	byte		entMon;				// 입구-월(2D)
	byte		entDay;				// 입구-일(2D)
	byte		entHour;			// 입구-시(2D)
	BYTE		entMin;				// 입구-분(2D)
	WORD		entSerial;			// 입구일련번호(4D)
	WORD		entICNum;			// 입구IC번호(3D)
	byte		entProper;			// 입구-적격차구분(1D)
	byte		entLane;			// 입구-차로번호(2D)
	byte		entType;			// 입구-예비권구분(1D)
	byte		entCntAxle;			// 입구-축수코드(1D)
	byte		extYear;			// 출구-년(2D)
	byte		extMon;				// 출구-월(2D)
	byte		extDay;				// 출구-일(2D)
	byte		extHour;			// 출구-시(2D)
	byte		extMin;				// 출구-분(2D)
	WORD		extWorkNum;			// 출구-근무번호(4D)
	byte		extCode;			// 출구-처리코드(2D)
	WORD		extSerial;			// 출구-일련번호(4D)
	DWORD		extFare;			// 출구-통행요금(6D)
	DWORD		extCash;			// 출구-수수할현금(6D)
	WORD		extICNum;			// 출구-출구IC번호(3D)
	byte		extCntAxle;			// 출구-특수코드(1D)
	byte		extTtype;			// 출구-통행권 차종(1D)
	byte		extFtype;			// 출구-확정   차종(1D)
	byte		extDisc[16];		// 출구-할인카드번호(16D)
	byte		extExemp[16];		// 차종-면제카드번호(16D)
} TkData;

typedef struct _MtProcInfo
{
	int			TkSerial;
	int			bSended;
	int			code;
} MtProcInfo;

typedef struct _MtImgInfo
{
	DWORD		ImgW;
	DWORD		ImgH;
	DWORD		pImgW;
	DWORD		pImgH;
	DWORD		pLeft;
	DWORD		pTop;
	char		srcFileName[MT_MAXLEN_FILENAME];	// 파일명
	char		dstFileName[MT_MAXLEN_FILENAME];	// 새로운 파일명
	char		BLFileName[MT_MAXLEN_FILENAME];		// BL일 경우 생성할 새로운 이름
	char		BLFileName2[MT_MAXLEN_FILENAME];	// YJW추가_SvrImgCopy BL 파일 이름
	char		pltFileName[MT_MAXLEN_FILENAME];
	int			ImgSize;
	int 		pImgSize;
	BYTE		bRecog;								// 0 = 미인식, 1 = 번호판 영역을 찾음, 2 = 인식
	char		strPlate[20];						// 번호판
	BYTE		BLtype;								// BL 타입
	char		strBL[20];
	char		bTow;								//2013.9.1 SIJ - 견인차량인지에 대한 여부	
} MtImgInfo;

//이미지 구조체
typedef struct _MtImg
{
	MtImgInfo			ImgInfo;
	unsigned char		cBuf[1024 * 1024];			// 차량영상
	unsigned char		pBuf[1024 * 1024];			// 번호판영상
	DWORD				QNum;
	int			Next;						// 다음영상
	int			Prev;						// 이전 영상
	int			bUsed;						// 영상이 들어있는지 여부
	int			bTrfd;						// 처리완료 되었는지 여부
	int			bSndFTP;					// FTP로 전송되었는지, 백업용파일의 지표로 사용 2011.05.08
	int			DoubleCode;					// 차종변경여부, 2013.4.15
	char		imgDateTime[15];			// 이미지 생성 시간(년월일시분초) cjw
} MtImg;

//Queue운영을 위한 구조체
typedef struct _MtImgQ
{
	MtImg		Img[16];		
	int			pbgn;			// 버퍼의 시작점
	int			pend;			// 버퍼의 끝점
	int			pscr;			// 화면에 표시되는 지점
} MtImgQ;

typedef struct _MtPacket
{
	unsigned char	buffer[MAX_PACKET_SIZE];
	int				size;
} MtPk;

extern	MtImgQ	g_ImgQ;
extern	MtPk	g_mtpk;

class CTcsLink
{
public:
	CTcsLink(void);
	virtual ~CTcsLink(void);

public:
	int			m_prevCarClass;		// 이전에 통과한 차량의 차종
	int			m_tcsSockCnt;		// TCS와 통신하는지에 대한 데이터
	int			m_tcsSockErrCnt;	// TCS 접속 에러 카운터
	TkData		m_tkSnd;			// 전송할 TCS처리 데이터
	int			m_ImgW;				// 영상의 크기 - Width
	int			m_ImgH;				// 영상의 크기 - Height
	int			m_plW;				// 번호판의 크기 - Width
	int			m_plH;				// 번호판의 크기 - Height
	CString		m_delFilename;
	CString		m_delpltFilename;
	CString		m_EmptyFileName;
	int			m_FlagEmptyImg;		// 영상없이 TCS데이터만 처리되었다는 플래그
	int			m_wrecker;			// 견인모드
	int			m_ImgErrCnt;		// 영상이 남는 경우의 카운트

	int			m_bNextPrevKey;		// 영상 이전이나 다음키 제어 여부
	int			m_tkClosed;			// 폐쇄차로 데이터가 발생되었는지의 여부
	int			m_NumLane;			// 차선번호
	HANDLE		m_closedEvent;		// 측면영상을 폐쇄차로 데이터로 전송
	int			m_ImgMatchAlg;		// 영상맞춤알고리즘
	int			m_SendDoubleCode;	// 2013.5.30

	void ProcImg(MtImg *Img, TkData *tk);
	void setFTPFileName(MtImg *Img, TkData *tk);
	BYTE GetStatus();
	LONG BCDtoINT(unsigned char *buf, int size);
	void putTkData(unsigned char *buf, TkData *tk, byte ctrlcode);
	void Init(HWND hWnd);
	int	MoveQfwd(int queueID);
	int	MoveQrev(int queueID);
	void FreeImgQ(MtImg *img);
	
	void InitImgQ();
	
	MtPk* mkFrame(ULONG cmd, int ImgNum=-1, int data = 0);
	MtImg* selectImg(int bNew = TRUE, int bNotAlg = FALSE);
	MtPk* OnTcpRx(unsigned char *buf, int size);
	MtPk* mkImgFrame(MtImg *img);
	MtImg* NewImgQ();
	//CLinkTCSDlg m_tcsDlg;

	void FreeTkSndData();
	void ReadInfoFromFileName(MtImgInfo *info);
	WORD GetTcsWorkNum();
	void AddLog(LPCTSTR format, ...);

protected:
	HWND		m_hWnd;
	MtProcInfo	m_ProcInfo;
	int			m_loopcnt;		// 루프홀딩 카운트
	WORD		m_workNum;		// 
};

