#include "StdAfx.h"
#include "MtSvrConSock.h"
#include "JWIS_TCS.h"
#include "JWIS_TCSDlg.h"
#include <string.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


CMtSvrConSock::CMtSvrConSock(void)
{
	m_rxLen = 0;
	m_connected = FALSE;
}


CMtSvrConSock::~CMtSvrConSock(void)
{
}

void CMtSvrConSock::SetWnd(HWND hwnd)
{
	m_Hwnd = hwnd;
}

/**
* 면탈서버에 접속한다. 
*
*@param		svrip		서버 IP
*@param		port		포트
*/
BOOL CMtSvrConSock::Connect(LPCTSTR svrip, int port)
{
	SOCKADDR_IN servAddr;

	m_sock = socket(AF_INET, SOCK_STREAM, 0);
	
	memset(&servAddr, 0, sizeof(SOCKADDR_IN));
	servAddr.sin_family=AF_INET;
	servAddr.sin_addr.s_addr=inet_addr(svrip);
	servAddr.sin_port=htons(port);
 
	if( connect(m_sock, (SOCKADDR*)&servAddr, sizeof(servAddr)) == SOCKET_ERROR ){
		return FALSE;
	}

	BOOL optval = TRUE;
	// 소켓 옵션을 변경한다. 
	if(setsockopt(m_sock, SOL_SOCKET, SO_KEEPALIVE, (char*)&optval, sizeof(optval)) == SOCKET_ERROR){
		return FALSE;
	}

	// 비동기적으로 소켓 데이터를 처리한다. 
	if ( WSAAsyncSelect(m_sock, m_Hwnd, WM_MTSVR_SOCK,  FD_CLOSE|FD_READ) == SOCKET_ERROR ) {
		closesocket(m_sock);
		m_sock = INVALID_SOCKET;
		return FALSE;
	}

	//m_connected = TRUE;
	return TRUE;
}

void CMtSvrConSock::Disconnect()
{
	m_connected = FALSE;
	if(m_sock != INVALID_SOCKET)
	{
		closesocket(m_sock);
	}
}

/**
*  문자를 전송한다. 
*
*@param		str		
*/
bool CMtSvrConSock::sendString(LPCTSTR str)
{
	char szSndBuf[256];
	
	if(m_connected == FALSE) 
	{
		return FALSE;
	}

	memset(szSndBuf, 0, sizeof(szSndBuf));
	sprintf_s(szSndBuf, sizeof(szSndBuf), "%s ", str);
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);

	return TRUE;
}
