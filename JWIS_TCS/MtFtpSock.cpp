#include "StdAfx.h"
#include "MtFtpSock.h"
#include "JWIS_TCS.h"
#include "JWIS_TCSDlg.h"
#include <MMSystem.h>
#include <string.h>
#include <MSWSock.h>
#include <WinSock2.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CJWIS_TCSDlg*	pDlg;

CMtFtpSock::CMtFtpSock(void)
{
	m_rxLen = 0;
	m_connected = FALSE;
	m_TransListenSock	= INVALID_SOCKET;
	m_TransmitSock		= INVALID_SOCKET;
	m_hListenEvent		= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hAcceptEvent		= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hRcvDoneEvent		= CreateEvent(NULL, TRUE, FALSE, NULL);
}


CMtFtpSock::~CMtFtpSock(void)
{
	CloseHandle(m_hListenEvent);
	CloseHandle(m_hAcceptEvent);
	CloseHandle(m_hRcvDoneEvent);
}

void CMtFtpSock::SetListenPort(unsigned short port)
{
	m_listenPort = port;
}

SOCKET CMtFtpSock::CreateListenSocket(HWND hWnd, u_int msg)
{
	SOCKADDR_IN servAddr;
	m_TransListenSock = socket(AF_INET, SOCK_STREAM, 0);
	if(m_TransListenSock == INVALID_SOCKET)
	{
		return INVALID_SOCKET;
	}
	
	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family=AF_INET;
	servAddr.sin_port=htons(0);
	servAddr.sin_addr.s_addr=htonl(INADDR_ANY);

	int optVal = 1;
	if(setsockopt(m_TransListenSock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal, sizeof(optVal)) == SOCKET_ERROR) {
		closesocket(m_TransListenSock);
		return INVALID_SOCKET;
	}

	if( bind(m_TransListenSock, (LPSOCKADDR)&servAddr, sizeof(servAddr)) == SOCKET_ERROR) {
		closesocket(m_TransListenSock);
		return INVALID_SOCKET;
	}
	if (listen(m_TransListenSock, SOMAXCONN) == SOCKET_ERROR) {
		closesocket(m_TransListenSock);
		return INVALID_SOCKET;
	}

	//연결요청 이벤트의 준비를 한다.
	if ( WSAAsyncSelect(m_TransListenSock, hWnd, msg, FD_ACCEPT|FD_CLOSE) == SOCKET_ERROR ) {
		closesocket(m_TransListenSock);
		return INVALID_SOCKET;
	}

	return m_TransListenSock;
}

BOOL CMtFtpSock::FtpConnect(LPCTSTR svrip, LPCTSTR user, LPCTSTR pass, void *Dlg)
{
	SOCKADDR_IN servAddr;
	char szSndBuf[256];
	int i;
	char tmp[512];
	int timeout = 1000;

	pDlg = (CJWIS_TCSDlg *)Dlg;

	m_sock = socket(AF_INET, SOCK_STREAM, 0);
	
	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family=AF_INET;
	servAddr.sin_addr.s_addr=inet_addr(svrip);
	servAddr.sin_port=htons(21);
	
	if( connect(m_sock, (SOCKADDR*)&servAddr, sizeof(servAddr))==SOCKET_ERROR )
	{
		closesocket(m_sock);
		return FALSE;
	}

	BOOL optval = TRUE;
	if(setsockopt(m_sock, SOL_SOCKET, SO_KEEPALIVE, (char*)&optval, sizeof(optval)) == SOCKET_ERROR)
	{
		closesocket(m_sock);
		return FALSE;
	}

	if(rcvData(500) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		memset(tmp, 0, sizeof(tmp));
		sprintf_s(tmp, sizeof(tmp), "'%d' - %s\0", i, m_rxbuf);
		pDlg->AddLog(tmp);
		if(i != 220)
		{
			closesocket(m_sock);
			return FALSE;
		}
	}
	else
	{
		closesocket(m_sock);
		return FALSE;
	}

	//2013.3.21 - 220으로 시작되는 문자열이 여러번 전송될 경우, 이를 처리
	//FileZila등에서는 확실하게 동작되나, 다른 FTP서버에서는 어떤지 테스트가 필요함
	while(i == 220)
	{
		if(rcvData(timeout) > 0)
		{
			i = GetRcvCode(m_rxbuf);
			memset(tmp, 0, sizeof(tmp));
			sprintf_s(tmp, sizeof(tmp), "'%d' - %s\0", i, m_rxbuf);
			pDlg->AddLog(tmp);
		}
		else
			i = 0;
	}


	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "USER %s\r\n", user);
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	pDlg->AddLog(szSndBuf);

	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		memset(tmp, 0, sizeof(tmp));
		sprintf_s(tmp, sizeof(tmp), "'%d' - %s\0", i, m_rxbuf);
		pDlg->AddLog(tmp);
		if(i != 331) 
		{
			closesocket(m_sock);
			return FALSE;
		}
	}
	else
	{
		closesocket(m_sock);
		return FALSE;
	}

	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "PASS %s\r\n", pass);
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	pDlg->AddLog(szSndBuf);

	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		memset(tmp, 0, sizeof(tmp));
		sprintf_s(tmp, sizeof(tmp), "'%d' - %s\0", i, m_rxbuf);
		pDlg->AddLog(tmp);
		if(i != 230)
		{
			closesocket(m_sock);
			return FALSE;
		}
	}
	else
	{
		closesocket(m_sock);
		return FALSE;
	}
	m_connected = TRUE;

	if(m_TransmitSock != INVALID_SOCKET)
	{
		closesocket(m_TransmitSock);
		m_TransmitSock = INVALID_SOCKET;
	}
	return TRUE;
}

/**
* 코드를 받는다.
*
*@param		buf		
*@param		subcode		
*/
int CMtFtpSock::GetRcvCode(char *buf, int *subcode)
{
	int code = -1;
	if(subcode != NULL)
	{
		sscanf_s(buf, "%d %d", &code, subcode);
	}
	else
	{
		sscanf_s(buf, "%d %d", &code);
	}
	return code;
}

int CMtFtpSock::rcvData(int timeout)
{
	ULONG ioLength;
	int r;
	int cnt;
	if(timeout == 0)
		cnt = 1;
	else if(timeout > 0)
		cnt = timeout;

	m_rxLen = 0;
	memset(m_rxbuf, 0, RX_MAX_BUFFER);
	for(int i=0; i<cnt; i++)
	{
		if(	ioctlsocket(m_sock, FIONREAD, &ioLength) == SOCKET_ERROR ) return -1;
		if(ioLength > 0)
		{
			r = recv(m_sock, &m_rxbuf[m_rxLen], ioLength, 0);
			m_rxLen += r;
			if( (m_rxbuf[m_rxLen-2] == '\r') && (m_rxbuf[m_rxLen-1] == '\n') ) break;
		}
		Sleep(1);
	}

	return m_rxLen;
}

/**
* 버퍼를 업로드한다.
*
*@param		filename		
*@param		buf		
*@param		bufsize		
*@param		gateType		
*/
WORD CMtFtpSock::UploadBuffer(LPCTSTR filename, char *buf, int bufsize, int gateType)
{
	char szSndBuf[256];
	int i;
	CString str_name;
	int timeout;

	timeout = 500;

	//PWD
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "PWD \r\n");
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 257) return (0x1000 | (i & 0x0FFF));
	}
	else
		return 0x1000;
	Sleep(0);

	//CWD
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "CWD /\r\n");
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 250) return (0x9000 | (i & 0x0FFF));
	}
	else
		return 0x9000;
	Sleep(0);

	//TYPE A
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "TYPE I\r\n");
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 200) return (0x2000 | (i & 0x0FFF));
	}
	else
		return 0x2000;
	Sleep(1000);

	//PORT command를 위한 parameter
	struct sockaddr_in sockData;
	int nDataLen;
	u_short uDataPort;
	BYTE ip1,ip2,ip3,ip4,p1, p2;

	nDataLen = (int)sizeof(sockData);
	getsockname(m_TransListenSock, (sockaddr*)&sockData, &nDataLen);
	uDataPort = ntohs(sockData.sin_port);
	p1 = HIBYTE(uDataPort);
	p2 = LOBYTE(uDataPort);

	struct sockaddr_in serv_addr;
	int addr_len = sizeof(serv_addr);
	getsockname(m_sock, (struct sockaddr*)&serv_addr, &addr_len);

	ip1 = serv_addr.sin_addr.S_un.S_un_b.s_b1;
	ip2 = serv_addr.sin_addr.S_un.S_un_b.s_b2;
	ip3 = serv_addr.sin_addr.S_un.S_un_b.s_b3;
	ip4 = serv_addr.sin_addr.S_un.S_un_b.s_b4;

	//연결되어 Accept가 이루어지면 JWIS_TCSDlg에서 시그널을 Set함
	//데이터 전송용 소켓의 연결을 기다림 --> DLG폼에서 SetEvent를 할 것임
	m_TransmitSock = INVALID_SOCKET;
	SetEvent(m_hListenEvent);
	ResetEvent(m_hAcceptEvent);

	//PORT command
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "PORT %d,%d,%d,%d,%d,%d\r\n", ip1,ip2,ip3,ip4, p1,p2);
	i = strlen(szSndBuf);
	send(m_sock, szSndBuf, i, 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 200) return (0x4000 | (i & 0x0FFF));
	}
	else
	{
		return 0x4000;
	}
	Sleep(100);

	//STOR command
	if( (filename[0] == 'B') || (filename[0] == 'b') )
		str_name.Format("%s", &filename[1]);
	else if( (filename[0] == 'R') || (filename[0] == 'r') )	//2012.5.7 SIJ
		str_name.Format("%s", &filename[1]);
	else if( (filename[0] == 'T') || (filename[0] == 't') )	//2013.8.30 SIJ
		str_name.Format("%s", &filename[1]);
	else if( (filename[0] == 'C') || (filename[0] == 'c') )	// 2016.03.16 cjw
		str_name.Format("%s", &filename[1]);
	else
		str_name.Format("%s", filename);
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "STOR %s\r\n", str_name);
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);

		if(i != 150 && i != 125)
			return (0x5000 | (i & 0x0FFF));
	}
	else
	{
		return 0x5000;
	}


	DWORD ret_wait;
	ret_wait = WaitForSingleObject(m_hAcceptEvent, 10000);

	ResetEvent(m_hListenEvent);
	if(ret_wait != WAIT_OBJECT_0)
		return 0x3006;

	if(m_TransmitSock == INVALID_SOCKET)
		return 0x3004;


	//데이터 전송
	int total=0;
	int ZeroCnt = 0;
	while(total < bufsize)
	{
		i = send(m_TransmitSock, &buf[total], bufsize-total, 0);

		//2012.11.06 - SOCKET_ERROR뿐만 아니라, 모든 에러에 대한 처리
		if(i < 0)
		{
			shutdown(m_TransmitSock, SD_BOTH);
			closesocket(m_TransmitSock);
			m_TransmitSock = INVALID_SOCKET;
			return 0x3005;
		}
		else if(i == 0)//2012.11.06
		{
			if(ZeroCnt++ > 10)	//전송결과가 0byte인 경우가 연속으로 %d회 이상인 경우
			{
				shutdown(m_TransmitSock, SD_BOTH);
				closesocket(m_TransmitSock);
				m_TransmitSock = INVALID_SOCKET;
				return 0x3007;
			}
			Sleep(10);	//... 그냥 좀 쉬었다가 재전송하는게 어떨까해서...
		}
		else
			ZeroCnt = 0;

		total += i;
	}
	Sleep(100);

	//마무리
	closesocket(m_TransmitSock);
	m_TransmitSock = INVALID_SOCKET;

	//완료 신호 접수
	if(rcvData(1000) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 226) return (0x6000 | (i & 0x0FFF));
	}
	else
		return 0x6000;
	Sleep(0);

	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "RNFR %s\r\n", str_name);		// rename from
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 350)return (0x7000 | (i & 0x0FFF));
	}
	else
		return 0x7000;
	Sleep(0);

	// rename to
	memset(szSndBuf, 0, 256);
	if( (filename[0] == 'B') || (filename[0] == 'b') )
		sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO %s\r\n", filename);	//2012.5.7 SIJ
	else if( (filename[0] == 'R') || (filename[0] == 'r') )
		sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO %s\r\n", filename);
	else if( (filename[0] == 'T') || (filename[0] == 't') )	//2013.8.30 SIJ
		sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO %s\r\n", filename);
	else if( (filename[0] == 'C') || (filename[0] == 'c') )	//2016.03.16 cjw
		sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO %s\r\n", filename);
	else
	{
		// 차선의 운영형태 (0=폐쇄식출구, 1=개방식상행, 2=개방식하행)
		switch(gateType)
		{
			case 0 : sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO o%s\r\n", filename);
				break;
			case 1 : sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO u%s\r\n", filename);
				break;
			case 2 : sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO d%s\r\n", filename);
				break;
			default : sprintf_s(szSndBuf, sizeof(szSndBuf), "RNTO o%s\r\n", filename);	//2012.12.04 SIJ - 기본형을 'o'(폐쇄식출구)로 지정
				break;
		}
	}
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		
		if(i == 550) //파일이 이미 존재함, 또는 그런 파일을 생성할 수 없음
		{	//전송한 파일을 지우고 전송이 완료되었다고 리턴 2011.05.08 SIJ
			memset(szSndBuf, 0, sizeof(szSndBuf));
			sprintf_s(szSndBuf, sizeof(szSndBuf), "DELE /%s\r\n", filename);
			send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
			rcvData(timeout);	//삭제되었는지 확인안함, 확인을 꼭 해야 하나...
		}
		else if(i != 250) return (0x8000 | (i & 0x0FFF));
	}
	else
		return 0x8000;

	return 0;
}

void CMtFtpSock::FtpDisconnect()
{
	char szSndBuf[256];
	int timeout = 500;

	if(m_connected == TRUE)
	{
		m_connected = FALSE;

		memset(szSndBuf, 0, 256);
		sprintf_s(szSndBuf, sizeof(szSndBuf), "quit\r\n");
		try
		{
			if(send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0) != SOCKET_ERROR)
				rcvData(timeout);
		}
		catch(...)
		{
		}
	}

	closesocket(m_sock);
}

void CMtFtpSock::OnClose( int nErrorCode )
{
	closesocket(m_sock);
	m_connected = FALSE;
}

char FTP_RCV_BUF[4096];
WORD CMtFtpSock::GetFile(LPCTSTR filename, LPCTSTR path)
{
	char szSndBuf[256];
	int i=0, filesize;
	char tmpstr[128];
	char *buf = FTP_RCV_BUF;
	int  rcvMax = sizeof(FTP_RCV_BUF);
	int total=0;
	int bufsize=0;
	CString str, substr;
	int timeout = 500;

	//PWD
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "PWD \r\n");
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);	//2019.08.27 SIJ
		if(i != 257) return (0x1000 | (i & 0x0FFF));
	}
	else
	{
		return 0x1000;
	}
	
	Sleep(0);

	//CWD
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "CWD /\r\n");
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 250) return (0x9000 | (i & 0x0FFF));
	}
	else
		return 0x9000;
	Sleep(0);


	//SIZE
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "SIZE %s\r\n", filename);
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		filesize = -1;
		i = GetRcvCode(m_rxbuf, &filesize);
		pDlg->m_ftpfileSize = filesize;
		if(i != 213) return (0x3000 | (i & 0x0FFF));
		if(filesize == -1) return 0x3FFF;
	}
	else
		return 0x3000;
	Sleep(0);


	//TYPE I
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "TYPE I\r\n");
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 200) return (0x2000 | (i & 0x0FFF));
	}
	else
		return 0x2000;
	Sleep(0);


	//PORT command를 위한 parameter
	SOCKADDR_IN sockData;
	int nDataLen;
	u_short uDataPort;
	BYTE ip1,ip2,ip3,ip4,p1, p2;

	nDataLen = (int)sizeof(sockData);
	getsockname(m_TransListenSock, (sockaddr*)&sockData, &nDataLen);
	uDataPort = ntohs(sockData.sin_port);
	p1 = HIBYTE(uDataPort);
	p2 = LOBYTE(uDataPort);

	HOSTENT *lphostnet;
	unsigned int *local_ip;
	struct in_addr src_ip;
	memset(tmpstr, 0, 128);
	gethostname(tmpstr, 128);
	lphostnet = gethostbyname(tmpstr);
	local_ip = (unsigned int *)lphostnet->h_addr_list[0];
	src_ip.s_addr = *local_ip;
	ip1 = src_ip.S_un.S_un_b.s_b1;
	ip2 = src_ip.S_un.S_un_b.s_b2;
	ip3 = src_ip.S_un.S_un_b.s_b3;
	ip4 = src_ip.S_un.S_un_b.s_b4;

	ip1 = 127;
	ip2 = 0;
	ip3 = 0;
	ip4 = 1;

	//연결되어 Accept가 이루어지면 JWIS_TCSDlg에서 시그널을 Set함
	//데이터 전송용 소켓의 연결을 기다림 --> DLG폼에서 SetEvent를 할 것임
	m_TransmitSock = INVALID_SOCKET;
	SetEvent(m_hListenEvent);
	ResetEvent(m_hRcvDoneEvent);

	//PORT command
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "PORT %d,%d,%d,%d,%d,%d\r\n", ip1,ip2,ip3,ip4, p1,p2);
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 200) return (0x4000 | (i & 0x0FFF));
	}
	else
	{
		return 0x4000;
	}
	Sleep(100);

	//RETR command
	memset(szSndBuf, 0, 256);
	sprintf_s(szSndBuf, sizeof(szSndBuf), "RETR %s\r\n", filename);
	send(m_sock, szSndBuf, (int)strlen(szSndBuf), 0);
	if(rcvData(timeout) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 150) return (0x5000 | (i & 0x0FFF));
	}
	else
	{
		return 0x5000;
	}

	DWORD ret_wait;
	ret_wait = WaitForSingleObject(m_hRcvDoneEvent, 300 * 1000);	//time out 3분

	ResetEvent(m_hListenEvent);
	if(ret_wait != WAIT_OBJECT_0)
		return 0x3006;

	if(m_TransmitSock == INVALID_SOCKET)
		return 0x3004;

	//완료 신호 접수
	if(rcvData(1000) > 0)
	{
		i = GetRcvCode(m_rxbuf);
		if(i != 226) return (0x6000 | (i & 0x0FFF));
	}
	else
	{
		return 0x6000;
	}

	//마무리
	closesocket(m_TransmitSock);

	if(pDlg->m_total < filesize)
	{
		return 0x7000;
	}

	return 0;
}