#pragma once
#include "afxwin.h"

// CSetupDlg 대화 상자입니다.

class CSetupDlg : public CDialog
{
	DECLARE_DYNAMIC(CSetupDlg)

public:
	CSetupDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSetupDlg();

	//GUI에 대응되는 변수
	CString		m_carImgPath;
	CString		m_pltImgPath;
	CString		m_tagPath;
	CString		m_svrImgPath;
	CString		m_svrImgPathCopy;
	CString		m_tmpImgPath;
	CString		m_bakImgPath;
	CString		m_LogPath;
	CString		m_ftpAddr;
	CString		m_ftpUser;
	CString		m_ftpPass;
	int		m_ftpPort;
	int		m_SvrPort;
	int		m_lclPort;
	int		m_NumCamera;
	int		m_NumLane;
	int		m_NumOffice;
	CString		m_DBUser;
	CString		m_DBPass;
	int		m_gateType;			// Radio 차선운영형태
	int		m_AllTkData;		// 2013.3.21 SIJ 개방식차로에서 일반통행권 데이터를 전송할 것인지의 여부
	int		m_SendDoubleCode;	// 2013.4.15 SIJ, 차종변경과 면제등이 동시에 발생할때, 차종변경데이터를 전송할 것인가의 여부
	int		m_MixedHipass;		// 혼용차로 여부
	//2012.7.12 SIJ - 스스로 제어기의 IP를 설정
	CString		m_MyIPAddr;
	CString		m_MySubnet;
	int m_ExtenededScreeen; //상세옵션설정으로 화면확장이 되었는지의 여부

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SETUP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CButton m_chk_mixedHipass;
	CButton m_chk_allTkData;
	CEdit m_edit_officeNum;
	CEdit m_edit_cameraNum;
	CEdit m_edit_laneNum;
	CEdit m_edit_myIPAddr;
	CEdit m_edit_mySubnetMask;
	CEdit m_edit_ftpAddr;
	CEdit m_edit_odbcUser;
	CEdit m_edit_odbcPass;
	CButton m_chk_sendDoubleCode;
	CEdit m_edit_carPath;
	CEdit m_edit_pltPath;
	CEdit m_edit_tagPath;
	CEdit m_edit_tmpPath;
	CEdit m_edit_svrPath;
	CEdit m_edit_bakPath;
	CEdit m_edit_logPath;
	CEdit m_edit_svrPort;
	CEdit m_edit_lclPort;
	CEdit m_edit_ftpUser;
	CEdit m_edit_ftpPass;
	CEdit m_edit_ftpPort;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	CButton m_radio_gateType_Out;	// 차선의 운영형태 (0=폐쇄식출구, 1=개방식상행, 2=개방식하행)
	CButton m_radio_gateType_Up;
	CButton m_radio_gateType_Down;
	afx_msg void OnBnClickedOk();
};
